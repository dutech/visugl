# VisuGL

Une bibliothèque C++ légère, avec seulement des fichiers header (```xxx.hpp```) pour faire de la visualisation rapide et sans fioriture de données sous forme de courbes.

## Utilisation basique

Explicitée dans ```test/010-markplot.cpp```.

- créer une [Window](#class_W) (ou ```WindowStatic```)
- créer une [Figure](#class_F) attachée à cette [Window](#class_W) par ```Window::add_plotter( )```.
- créer une [Data](#class_D) à laquelle on ajoute des [Sample](#class_S) avec ```add( &sample )```
- cette [Data](#class_D) peut-être affichée avec un [LinePlotter](#class_LP) ou [MarkPlotter](#class_MP). Cette instance de [Plotter](#class_P) doit être liée à la [Figure](#class_F) avec ```Figure::add_plotter()```.

## Concepts de la bibliothèque

La bibliothèques est organisée en une hiérarchie de [Plotter](#class_P), chaque ```Plotter``` a des fils. Chacun a un [BoundinBox](#class_B) qui indique quelle portion de l'espace est affiché dans et par ce Plotter. On peut "fixer" ces différentes [BoundinBox](#class_B) ou les recalculer (```update_bbox()```) avant chaque affichage.

Il y a deux grandes sortes de fenêtres ([WinHolder](#class_WH)) possible : avec double buffer ([Window](#class_W)) ou sans ([WinStatic](#class_WS)). Ce dernier cas est plus rapide et plus adapté aux affichages où on ne fait que ajouter des éléments, ce qui permet d'optimiser la méthode ```render_last()``` des [Plotter](#class_P). Une [WinStatic](#class_WS) est entièrement réaffichée (plus lent) quand la fenêtre change de taille, qu'on ajoute un nouveau [Plotter](#class_P) ou qu'on le demande explicitement avant un ```WinHolder::render()``` en modifiant ```WinHolder::_should_redraw```.

Les [WinHolder](#class_WH) offrent un mode ```offscreen```, encore plus rapide mais sans affichage. Dans tous les cas, on peut sauvergarder ce qui est affiché dans une [WinHolder](#class_WH) avec la méthode ```save()```.

Chaque [Plotter](#class_P) a aussi un mode ```debug``` qui affiche son ```name``` quand il affiche (```render()``` ou ```render_last()```) ou (**TODO quand cela touche à sa [BoundinBox](#class_B)**) ou pour certains événenent (souris, clavier, resize, etc).

### <a name="class_P">Plotter</a> : tout ce qui affiche quelque chose

voir ```plotter.hpp```.

- Tout les objets qui affichent quelque chose héritent de ```Plotter```.

- Un ```Plotter``` s'affiche, ainsi que tous ses fils (```Plotter::_plotters```).

- Un ```Plotter``` est délimité par une ```BoundingBox```.

  - ```update_bbox()``` est a appeler pour recalculer cette ```BoundingBox```.

- Afficher se faire par le ```render()``` ou ```render_last()```

  - ```render()``` est destiné aux supports avec swap du buffer
  
  - ```render_last()``` n'affiche que ce qui est "nouveau" par rapport au dernier ```render_last()```. Adapté aux support avec un seul buffer.
  
  - les arguments ```screen_ratio_x/y``` permettent d'afficher du texte (voir par exemple [GraphicText](#class_GT) )
  
Tous les objets qui héritent de ```Plotter``` sont sensé redéfinir ```update_bbox()```, ```render()``` et, éventuellement, ```render_last()```.

### <a name="class_WH">WindHolder</a> : template de Fenêtre

Décliné en version avec 1 ou 2 buffers d'affichage.

voir ```window.hpp```.

#### <a name="class_W">Window</a> : fenêtre avec swapping buffer

voir ```window.hpp```.

- par défaut, une ```BoundingBox``` de [0,1] x[0,1]. Ce sont les coordonnées propres de la ```Window```.

- ```update_bbox()``` : recalcule une ```BoundingBox``` un peu plus grande que celle de ses fils.

- ```save( filename )``` : sauvegarde buffer au format PNG.

- ```render()``` : 

  - tous les fils ```for( const auto& plotter: _plotters)```
  
     - ```auto& bb = plotter->get_bbox();```
     
     - le ```BoundingBox``` du fils indique où le fils va s'afficher, dans les coordonnées de la ```Window``` 
     
     - set le ```Viewport``` pour ce fils
     
     - ```plotter->render( ratio_x, ratio_y );```
     
- ```render_last()``` : **pas implémenté**

### <a name="class_F">Figure</a> : Axes et tracés

voir ```figure.hpp```.

- par défaut, une ```BoundingBox``` de [-1,1] x[-1,1]. Ce sont les coordonnées propres de la ```Figure```.

- ```get_bbox()``` renvoie en fait ```_bbox_winspace``` qui est les coordonnées de la [Figure](#class_F) dans l'espace de la ```WinHolder``` ([Window](#class_W) ou [WinStatic](#class_WS)).

- ```update_bbox()``` met à jour la ```_bbox``` qui est en fait la ```innerbbox``` de la Figure. Englobant tous les fils.

- ```set_bbox()``` permet de "fixer" la ```_bbox``` et donc de déterminer ce qui est affiché des différents [Plotter](#class_P) fils. Jusqu'au prochain ```update_bbox()```...

- **TODO** : calcul redondants de _bbox et innerbox **??**

- ```render()``` :

  - màj des ```Axis``` ? alors recalcule ```_bbox = innerbox``` et change les ```Axis```
  
  - calcul matrices de projection, efface le fond.
  
  - pour tous les fils : ```plotter->render( screen_ratio_x, screen_ratio_y )```.
  
  - affiche les ```Axis```
  
  - affiche tous les ```GraphicText```
  
  - affiche le ```_title```

### <a name="class_D">Data</a> : les données à afficher

- ```add( Sample& )``` ajoute une nouvelle donnée et met à jour la [BoundinBox](#class_B) du ```Data```.

- ```get_samples()``` permet d'accéder à la collection des [Sample](#class_S).

### <a name="class_LP">LinePlotter</a> : afficher données avec des courbes

- ```update_bbox()``` recalcule la ```BoudingBox``` en fonction de ```_data```.

- quand changement dans ```_data``` : appel de ```children_changed()```. Surtout pour effacer la courbe. (**TODO à vérifier**).

- ```render()``` : 

  - relie chaque ```Sample``` de ```_data``` avec ```GL_LINE_STRIP```.
  
  - màj de ```_last_sample```.
  
- ```render_last()``` :

  - utilise ```GL_LINE_STRIP``` à partir de ```_last_sample```.
  
  - màj de ```_last_sample```.



## Autres Classes / Fonctionnalités

### <a name="class_GT">```GraphicText```</a> dans ```visugl.hpp```
Les données pour faciliter l'affichage de texte.

Un exemple d'affichage de texte dans ```Figure::render()``` (line 205 de ```figure.hpp```)

``` c++
for( auto& txt: _text_list) {
  glColor3d( txt.col.r, txt.col.g, txt.col.b );
  glPushMatrix(); {
    glTranslated( txt.x, txt.y, 0.0);
    glScaled( screen_ratio_x, screen_ratio_y, 1.0 );
    _font->Render( txt.msg.c_str() );
  } glPopMatrix();
}
```

### Logging

Pour logger les opérations sur les BoundingBox, une macro ```LOGBOX( string expression )``` est définie dans ```visugl.hpp```. Il suffit de compiler ou d'utiliser ```waf``` avec l'option ```-D LOG_BOX``` pour l'activer.

