# VisuGL : OpenGL (legacy) / very fast C++ live data vizualisation tool

Header only ?

## Documentation

A new version of VisuGL is slowly comming where `Data` + `Plotters` replace `Curve`. As a `Curve` was a mix of data, ploting function and parameters for plotting, I try to separate things.

### Basic Classes

- `Plotter` : Basic class for all plotting element

- `WinHolder [Plotter]` : window with Viewport, listener and _font
  - `Window [WinHolder]` : double buffer, compatible with AntTweakbar
  - `WindowStatic [WinHolder]` : single buffer (faster)

- `Figure [Plotter]` : draw axes

- `Figure3D [Plotter]` : draw axes, uses Trackball

- `Data [Plottable]` : has List<Sample>, notify `Plotters` when cleared.

- `XXXPlotters` : to rended `Data` as line, marks, etc...
  - `LinePloter [Plotter]` : render as line (color, width)
  - `MarkPlotter [Plotter]` : render as lozange or triangle (color, size)

- `Curve [Plotter]` : has List<Sample>, render as GL_LINES

  - `CurveMean [Curve]` : apply 'mean' filter on Samples
  - `CurveDyn<T> [Curve]` : need Container T and Functor from T to Sample
  - `ScatterPlotter [Curve]` : render Samples as small diamonds

- `ImgPlotter<TData> [Plotter]` : display 2D image using "veridis" Colormap

- `ColorMap` : define and apply Colormap to values

- `ColorMapPlotter<TData> [Plotter]` : plot a Colormap vertically

### Helpers functions and Classes

- `void to_png() [gl_utils.hpp]` : save Window to file

- `check_error() [gl_utils.hpp]` : check and display OpenGL errors

- `utils::FixedQueue<T> [std::List<T>]` : FIFO queue with a maximum size

- `Trackball []` : use GML to maintain a Transform for the camera position, used in Figure3D.

## Existing Demo (in test/)
### 000-debug [named]
Deux Curve dans Figure dans Window, avec debug de `render`.
Avec l'option 'named', chaque Plotter à un nom qui est affiché lors du debug.
Un exemple typique de sortie textuelle donne:
__RENDER WIN
__RENDER FIG
__RENDER CURVE1
__end render CURVE1
__RENDER CURVE2
__end render CURVE2
__end render FIG
__end render WIN


### 001-curve
Quatre Curve dans Figure dans Window

- `_curve1` : add point avec progression dans l'axe X. function classique
- `_curve2` : copie inversée de _curve1, plus différence entre add_sample et add_data
- `_curve3` : add_sample avec une Collection
- `_curve4` : ajout d'une série temporelle

### 001-curveoffscreen
Crée une Curve qui n'est pas affichée mais sauvegardée dans `001-curveoffscreen.png`.

### 001-scatterplot
Crée un scatter plot, tout simple, avec les données des _curve1 et _curve2 de 001-curve

### 002-dyncurve
Deux courbes dynamiques, avec adaptation uniquement selon l'axe des X.
Courbe est le 'Mean' de l'autre
Actuellement (v0.2), utiliser WinHolder::update() pour adapter les axes (range et position)

### 003-staticwin swap|static
Montre la différence entre Double et Single Buffer quand on ne fait des courbes qui se contentent d'ajouter de l'info. Affiche une trajectoire aléatoire dans monde avec rebonds

**ATTENTION** : il faut avoir des Plotters avec render_last() implémenté !

### 003-statiscatter swap|static
Montre la différence entre Double et Single Buffer avec ScatterPlot. Sur le même principe que `003-staticwin`

### 003-smartcurve
Un exemple qui utilise les `CurveDyn` avec une Collection (`_data`) et qui s'adapte aux changements dans cette Collection.

### 004-img_cmap
Exemple de Colormap utilisée pour afficher une Image (tableau de valeurs).

### 005-curve3D
Exemple de visualisation d'une courbe en 3D. Figure3D utilise le Trackball pour gérer la vue sur la courbe (rotation, zoom, déplacement).

### 010-markplot
Exemple de visualisation de `Data` avec un `LinePlotter` et un `MarkPlotter` dans une `Figure`, elle-mêm dans `Window`.

### 013-staticplotters
Montre la différence entre Double et Single Buffer quand on ne fait des courbes qui se contentent d'ajouter de l'info. Affiche une trajectoire aléatoire dans monde avec rebonds. Utilise `Data`, `LinePlotter` et `MarkPlotter`.

### 101-curve_ant
Exemple d'utilisation de AntTweakBar pour modifier les paramètres des courbes affichées.

La touche SPACE bascule le mode d'affichage scatter.

**ATTENTION** : nécessite AntTweakBar et, pour une raison complexe, affiche de nombreux message d'erreur XLib pendant l'exécution (on peut les ignorer en redirigeant les erreurs : `101-curve_ant 2> /dev/null`).

## Build
### configure
### build
### install

## Dependencies
- **waf** [https://waf.io/], a meta builder (altenative to make, CMake)
  download from the web site

- **FTGL** [],
  sudo apt install 

- **OpenGL** + **GLEW** [],

- **GLFW3** [], 

- **libpng** [],

- **libpngwriter** [],

- optionnaly **AntTweakBar** [],

- optionnaly, if using clang, **clang_compilation_database**, which allow my emacs+company to analyze code and suggest better auto-completion
  
- **Consolas** fonts
  `cp ressources/Consolas.ttf ~/.fonts `
  
