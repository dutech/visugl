#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

#include <string>

namespace visugl {
// ********************************************************************* Color
struct Color {
  double r,g,b;
};
// ******************************************************************** Sample
struct Sample {
  double x,y,z;
};
// *************************************************************** GraphicText
struct GraphicText {
  GraphicText(double x, double y, const std::string msg, Color col) :
    x(x), y(y), msg(msg), col(col) {}
  double x, y;
  const std::string msg;
  Color col = {0.0, 0.0, 0.0};
};
}; // namespace visugl

#endif // BASE_TYPES_HPP
