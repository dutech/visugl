/* -*- coding: utf-8 -*- */

#ifndef WINHOLDER_HPP
#define WINHOLDER_HPP

/**
 * A WinHolder hold a GLFWwindow, and is a Plotter
 * Has its own font.
 * Can be rendered and saved OFFSCREEN.
 * Can have DOUBLE or SINGLE buffer
 * BUT has no render function !!
 *
 * The Plotter::bbox of a window is always (0,0)@(1,1)
 */

#include "grafik.hpp"

#ifdef USE_ANTTWEAKBAR
#include <AntTweakBar.h>
#endif

#define LOG_TITLE
#include "logging.hpp"
#include "base_types.hpp"
#include "bounding_box.hpp"
#include "plotter.hpp"

// Default BBox for Window => not really useful
#define WINMINX (0.0)
#define WINMAXX (1.0)
#define WINMINY (0.0)
#define WINMAXY (1.0)

namespace visugl {

// ***************************************************************************
// ***************************************************************** WinHolder
// ***************************************************************************
class WinHolder : public Plotter
{
public:
  // ***************************************************** WinHolder::creation
  WinHolder( std::string title = "WinHolder",
             const int width=640, const int height=400,
             const bool offscreen=false, const bool double_buffer=true,
             const int posx=-1, const int posy = -1 ) :
    Plotter( WINMINX, WINMAXX, WINMINY, WINMAXY ), // default _bbox
    _title( title ), _width(width), _height(height),
    _offscreen(offscreen), _double_buffer(double_buffer),
    _window(nullptr), _should_redraw(false),
    _font(nullptr),
    _key_cbk_ext(nullptr)
  {
    // Create window _________________________________________________
    glfwSetErrorCallback(grafik::error_callback);

    if( _offscreen) {
      glfwWindowHint(GLFW_VISIBLE, false );
    }
    if (not _double_buffer) {
      glfwWindowHint( GLFW_DOUBLEBUFFER, GL_FALSE );
    }
    _window = glfwCreateWindow(_width, _height, _title.c_str(), NULL, NULL);
    if (! _window ) {
      glfwTerminate();
      exit(EXIT_FAILURE);
    }
    glfwSetWindowPos( _window, posx, posy );
    glfwMakeContextCurrent( _window );
    // TODO can also be set to another DataStructure
    glfwSetWindowUserPointer( _window, this);
    glfwSetKeyCallback( _window, key_callback);
    glfwSetWindowSizeCallback( _window, resize_callback );
    glfwSetWindowPosCallback( _window, move_callback);
        
    /** Init Fonts */
    _font = grafik::font();

    /** offscreen => need RenderBuffer in FrameBufferObject */
    if( _offscreen ) {
      GLenum error = glewInit();
      if (error != GLEW_OK) {
        std::cout << "error with glew init() : " << glewGetErrorString(error) << std::endl;
      } else {
        std::cout << "glew is ok\n\n";
      }
      // std::cout << "__CREATE RenderBuffer" << std::endl;
      glGenRenderbuffers( 1 /* nb buffer */, &_render_buf);
      grafik::check_error();
      glBindRenderbuffer( GL_RENDERBUFFER, _render_buf );
      grafik::check_error();

      // format compatible with 'save' method
      glRenderbufferStorage(GL_RENDERBUFFER, GL_RGB, width, height);
      //glRenderbufferStorage(GL_RENDERBUFFER, GL_RGB32F, width, height);
      grafik::check_error();
      
      glBindRenderbuffer( GL_RENDERBUFFER, 0 );
      grafik::check_error();

      // std::cout << "__CREATE FrameBufferObject"  << std::endl;
      glGenFramebuffers(1 /* nb objects*/, &_fbo);
      grafik::check_error();
      glBindFramebuffer( GL_FRAMEBUFFER, _fbo );
      grafik::check_error();
      glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, /* attach point */
				 GL_RENDERBUFFER, _render_buf );
      grafik::check_error();
      
      // switch back to window-system-provided framebuffer
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
      grafik::check_error();
    }

    LOGBBOX( "__CREATE WinHolder " << _name << " BB=" << get_bbox() );
  }
  // *************************************************** WinHolder::destructor
  virtual ~WinHolder()
  {
    destroy_grafik();
  }
  void destroy_grafik()
  {
    if (_window) {
      glfwDestroyWindow( _window);
      _window = nullptr;
    }

    if( _offscreen ) {
      glDeleteRenderbuffers( 1, &_render_buf );
      grafik::check_error();
      glDeleteFramebuffers( 1, &_fbo );
      grafik::check_error();
      _offscreen = false;
    }
  }

  // ******************************************************* WinHolder::update
  /** window can not update its BBox but ask subPlotters to do it */
  virtual void update_bbox()
  {
    LOGBBOX( "__UPDWinH " << _name << " BB=" << get_bbox() );

    for( const auto& plotter: _plotters ) {
      // std::cout << "  calling "<< plotter->_name << std::endl;
      LOGBBOX( "  calling "<< plotter->_name );
      plotter->update_bbox();
    }

    LOGBBOX( "  UPDWinH " << _name << " BB=" << get_bbox() );
  }
  // ******************************************************* WinHolder::render
  virtual void render( float screen_ratio_x = 1.0, float screen_ratio_y = 1.0 )
  {
    std::cout << "__WinHolder::render() NOT IMPLEMENTED" << std::endl;
  }

  // ********************************************************* WinHolder::save
  void save( const std::string& filename )
  {
    std::cout << "__SAVE in " << filename << std::endl;
    // Make sure using current window
    glfwMakeContextCurrent( _window );
    // TODO can also be set to another DataStructure
    glfwSetWindowUserPointer( _window, this);

    if( _offscreen ) {
      // set rendering destination to FBO
      glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
      grafik::check_error();
    }
    // debug
    // std::cout << "  before" << std::endl;
    grafik::to_png( filename, _width, _height );
    // std::cout << "  after" << std::endl;
  }

  // **************************************************** WinHolder::Listeners
  void add_mouse_btn_listener( PlotterPtr plotter )
  {
    _mouse_btn_listeners.push_back( plotter );
  }
  void add_keyboard_listener( PlotterPtr plotter )
  {
    _keyboard_listeners.push_back( plotter );
  }
  void add_keycallback( void (*key_callback) (int key, int scancode,
                                              int action, int mods) )
  {
    _key_cbk_ext = key_callback;
  }
  // **************************************************** WinHolder::attributs
  std::string _title;
  int _width, _height;
  bool _offscreen;
  bool _double_buffer;
  GLFWwindow* _window;
  bool _should_redraw;
  /** Fonts to write text */
  FTGLTextureFont* _font;
  /** GLew variables for FrameBufferObject, RenderBuffer */
  GLuint _fbo, _render_buf;
protected:
  /** Listeners */
  PlotterList _mouse_btn_listeners;
  PlotterList _keyboard_listeners;

  /** Optionnal Callbacks */
  void (*_key_cbk_ext) (int, int, int, int);

protected:
  // ************************************************ WinHolder::GLFW callback
  // ************************************ WindowStatic::window move and resize
  static void resize_callback( GLFWwindow* window, int width, int height )
  {
    LOGBBOX( "__resize_cbk" );
    // call Class method
    ((WinHolder *)glfwGetWindowUserPointer(window))->on_resized();
  }
  static void move_callback( GLFWwindow *window, int xpos, int ypos )
  {
    LOGBBOX( "__move_cbk" );

    // call Class method
    ((WinHolder *)glfwGetWindowUserPointer(window))->on_resized();
  }
  virtual void on_resized()
  {
    if (_debug_fg) std::cout << "__on_resized " << _name << std::endl;

    _should_redraw = true;
    
    // get window size
    if( _offscreen ) {
      glBindRenderbuffer( GL_RENDERBUFFER, _render_buf );
      grafik::check_error();
      glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &_width);
      grafik::check_error();
      glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &_height);
      grafik::check_error();
      glBindRenderbuffer( GL_RENDERBUFFER, 0 );
      grafik::check_error();
    }
    else {
      glfwGetFramebufferSize( _window, &_width, &_height);
    }

    LOGBBOX( "__on_resized " << _name << " w=" << _width << " h=" << _height );
  }
  // ******************************************** WinHolder::keyboard_callback
  static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
  {
    // call Class method
    ((WinHolder *)glfwGetWindowUserPointer(window))->on_key( key, scancode, action, mods );
  }
  virtual void on_key( int key, int scancode, int action, int mods)
  {
    for( auto& listener: _keyboard_listeners) {
      listener->on_key( key, scancode, action, mods );
    }

    // optionnal extern callback
    if (_key_cbk_ext) {
      (*_key_cbk_ext)( key, scancode, action, mods );
    }
  }
  
}; // WinHolder
// *********************************************************** WinHolder - END

// ***************************************************************************
// ******************************************************************** Window
// ***************************************************************************
class Window : public WinHolder
{
public:
  // ******************************************************* Window::constructor
  
  Window( std::string title = "Window",
          const int width=640, const int height=400,
          const bool offscreen=false,
          const int posx=-1, const int posy = -1 ) :
    WinHolder( title, width, height, offscreen, true /*dbl_buffer*/,
               posx, posy )
  {
    // Additionnal Callback
    glfwSetMouseButtonCallback( _window, mouse_button_callback );
    glfwSetCursorPosCallback( _window, mouse_move_callback );
    glfwSetScrollCallback( _window, mouse_scroll_callback);

#ifdef USE_ANTTWEAKBAR
    // Initialize AntTweakBar
    TwInit(TW_OPENGL, NULL);
    TwWindowSize( _width, _height);

    // Create AntTweakBar
    _bar = TwNewBar( "TweakBar" );
    TwDefine(" GLOBAL help='Parameters of the Application' ");

    // - Directly redirect GLFW mouse button events to AntTweakBar
    glfwSetMouseButtonCallback( _window,
                                (GLFWmousebuttonfun) TwEventMouseButtonGLFW3 );
    
    // - Directly redirect GLFW mouse position events to AntTweakBar
    glfwSetCursorPosCallback( _window,
                              (GLFWcursorposfun) TwEventCursorPosGLFW3 );
    // - Directly redirect GLFW mouse wheel events to AntTweakBar
    glfwSetScrollCallback( _window,
                           (GLFWscrollfun) TwEventScrollGLFW3 );
    // - Directly redirect GLFW key events to AntTweakBar */
    glfwSetCharModsCallback( _window,
                             (GLFWcharmodsfun) TwEventCharModsGLFW3 );
    // TODO: What about key event ?
    // glfwSetKeyCallback( _window,
    //                     (GLFWkeyfun) TwEventKeyGLFW3 );
#endif
    
    LOGBBOX( "__CREATE Win " << _name << " BB=" << get_bbox() );
  }
  // ****************************************************** Window::destructor
  virtual ~Window()
  {
#ifdef USE_ANTTWEAKBAR
    // Terminate AntTweakBar
    TwDeleteBar( _bar );
    TwTerminate();
#endif
  }

  // ********************************************************** Window::render
  virtual void render( float screen_ratio_x = 1.0, float screen_ratio_y = 1.0 )
  {
    if (_debug_fg) std::cout << "__RENDER " << _name << std::endl;

    grafik::check_error();

    // Make sure using current window
    glfwMakeContextCurrent( _window );
    // TODO can also be set to another DataStructure
    glfwSetWindowUserPointer( _window, this);

    glfwPollEvents();

    if (not _should_render) {
      return;
    }
    
    if( _offscreen ) {
      // set rendering destination to FBO
      glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
      grafik::check_error();
    }
    
    //std::cout << "__WINDOW::render loop _plotters" << std::endl;
    for( const auto& plotter: _plotters) {

      // Set glViewport according to Figure BBox
      auto& bb = plotter->get_bbox();
      // std::cout << "  bb=" << bb << std::endl;
      // std::cout << "  _width=" << _width << " _height=" << _height << std::endl;
      
      // glViewport bottomleft x,y, width, height
      glViewport( 0 + std::lround(bb.x_min * _width),
                  0 + std::lround(bb.y_min * _height),
                  std::lround((bb.x_max - bb.x_min) * _width),
                  std::lround((bb.y_max - bb.y_min) * _height) );
      grafik::check_error();

      // "screen" ratio for that Figure
      auto ratio_x = 1.0 / ((double) _width * (bb.x_max - bb.x_min));
      auto ratio_y = 1.0 / ((double) _height * (bb.y_max - bb.y_min));

      plotter->render( ratio_x, ratio_y );
      grafik::check_error();
    }

    if( not _offscreen ) {

#ifdef USE_ANTTWEAKBAR
      // Draw tweak bars
      TwDraw();
#endif

      glfwSwapBuffers( _window );
      //glfwPollEvents();

    }
    if (_debug_fg) std::cout << "__end render " << _name << std::endl;
  }
  // ******************************************************* Window::attributs
#ifdef USE_ANTTWEAKBAR
  TwBar* _bar;
#endif

private:
  // *************************************************** Window::GLFW Callback
  virtual void on_resized()
  {
    WinHolder::on_resized();

    if (not _offscreen) {
#ifdef USE_ANTTWEAKBAR
      // Send the new window size to AntTweakBar
      TwWindowSize(_width, _height);
#endif
    }
  }
  // *********************************************** Window::keyboard callback
  // TODO: call AntTweakBar and, if not dealt with, registered Listeners
  // ************************************************** Window::mouse callback
  static void mouse_button_callback( GLFWwindow* window, int button, int action, int mods )
  {
    // call Class method
    ((Window *)glfwGetWindowUserPointer(window))->on_mouse_button( button, action, mods );
  }
  virtual void on_mouse_button( int button, int action, int mods )
  {
    //std::cout << "Window::on_mouse_button" << std::endl;

    // call AntTweakBar and, if not dealt with, registered Listeners
    for( auto& listener: _mouse_btn_listeners) {
      listener->on_mouse_button( button, action, mods );
    }
  }

  static void mouse_move_callback( GLFWwindow* window, 
				   double xpos, double ypos) 
  {
    // callback of Classe
    ((Window *)glfwGetWindowUserPointer(window))->on_mouse_move( xpos, ypos);
  }
  virtual void on_mouse_move( double xpos, double ypos )
  {
    //std::cout << "Window::on_mouse_move" << std::endl;

    // call AntTweakBar and, if not dealt with, registered Listeners
    for( auto& listener: _mouse_btn_listeners) {
      listener->on_mouse_move( xpos, ypos );
    }
  }

  static void mouse_scroll_callback(GLFWwindow* window,
                                    double xoffset, double yoffset)
  {
    // callback of Classe
    ((Window *)glfwGetWindowUserPointer(window))->on_mouse_scroll( xoffset, yoffset );
  }
  virtual void on_mouse_scroll( double xoffset, double yoffset )
  {
    //std::cout << "Window::on_mouse_scroll" << std::endl;

    // call AntTweakBar and, if not dealt with, registered Listeners
    for( auto& listener: _mouse_btn_listeners) {
      listener->on_mouse_scroll( xoffset, yoffset );
    }
  }
}; // Window
// ************************************************************** Window - END

// ***************************************************************************
// ************************************************************** WindowStatic
// ***************************************************************************
class WindowStatic : public WinHolder
{
public:
  // ************************************************** WindowStatic::creation
  WindowStatic( std::string title = "Window",
          const int width=640, const int height=400,
          const bool offscreen=false,
          const int posx=-1, const int posy = -1 ) :
    WinHolder( title, width, height, offscreen, false /*dbl_buffer*/,
               posx, posy )
  {
    LOGBBOX( "__CREATE WinStatic " << _name << " BB=" << get_bbox() );
  }
  // *********************************************** WindowStatic::add_plotter
  virtual bool add_plotter( const PlotterPtr plotter )
  {
    bool res = WinHolder::add_plotter( plotter );
    _should_redraw = true;
    LOGBBOX( "__ADD_PLOTTER WinStatic " << _name << " BB=" << get_bbox() );
    return res;
  }
  // **************************************************** WindowStatic::render
  virtual void render( float screen_ratio_x = 1.0, float screen_ratio_y = 1.0 )
  {
    if (_debug_fg) {
      std::cout << "__RENDER " << _name;
      std::cout << " _redraw=" << _should_redraw << std::endl;
    }
    grafik::check_error();

    // Make sure using current window
    glfwMakeContextCurrent( _window );
    // TODO can also be set to another DataStructure
    glfwSetWindowUserPointer( _window, this);

    if (not _should_render) {
      glfwPollEvents();
      return;
    }
    
    if( _offscreen ) {
      // set rendering destination to FBO
      glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
      grafik::check_error();
    }

    //std::cout << "__WINDOW::render loop _plotters" << std::endl;
    for( const auto& plotter: _plotters) {

      grafik::check_error();
      auto& bb = plotter->get_bbox();

      // glViewport bottomleft x,y, width, height
      glViewport( 0 + std::lround(bb.x_min * _width),
                  0 + std::lround(bb.y_min * _height),
                  std::lround((bb.x_max - bb.x_min) * _width),
                  std::lround((bb.y_max - bb.y_min) * _height) );
      grafik::check_error();

      // TODO not sure this ratio is good
      auto ratio_x = 1.0 / ((double) _width * (bb.x_max - bb.x_min));
      auto ratio_y = 1.0 / ((double) _height * (bb.y_max - bb.y_min));
      if (_should_redraw) 
        plotter->render( ratio_x, ratio_y );
      else
        plotter->render_last( ratio_x, ratio_y );
      // next render is incremental
      // must be set here (before glfwPollEvents() that triggers resize cbk)
      _should_redraw = false;

      grafik::check_error();
    }
    // // do not clear unless called by a reset_drawing
    // if (_should_redraw) {
    //     _reset_drawing = true;
    //     _should_redraw = false;
    //   }
    // if (_reset_drawing) {
    //   glClearColor( 1.0, 1.0, 1.0, 1.0);
    //   while ((err = glGetError()) != GL_NO_ERROR) {
    //     std::cerr << "WIN glClearColor glViewport OpenGL error: " << err << std::endl;
    //   }
    //   glClear(GL_COLOR_BUFFER_BIT);
    //   while ((err = glGetError()) != GL_NO_ERROR) {
    //     std::cerr << "WIN glClear OpenGL error: " << err << std::endl;
    //   }
    // }
    
    if( not _offscreen ) {
      glFinish(); // wait for all glCommand are finished (vs Double Buffer)
      grafik::check_error();
      glfwPollEvents();
    }
    
    if (_debug_fg) {
      std::cout << "__end render " << _name;
      std::cout << " _redraw=" << _should_redraw << std::endl;
    }
  }
}; // WindowStatic
// ******************************************************** WindowStatic - END

}; //namespace visugl

#endif // WINHOLDER_HPP
