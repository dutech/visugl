/* -*- coding: utf-8 -*- */

#ifndef FIGURE3D_HPP
#define FIGURE3D_HPP

/** 
 * A Figure3D has Axes (rendered if _draw_axes is set) ans is Plotter
 * The View of the Plot is controlled through a Trackball.
 * View can be reseted using 'R/r'.
 *
 * BEWARE : OpenGL is right-handed "internally" but converted to Left-Handed by
 *          glOrtho => thus scale by 1,1,-1 at the end !
 */

#include <visugl.hpp>
#include <gl_utils.hpp>
#include <axis.hpp>

#include <GLFW/glfw3.h>
#include <sstream>

#include <trackball.hpp>

namespace visugl {
// ***************************************************************************
// ****************************************************************** Figure3D
// ***************************************************************************
class Figure3D : public Plotter
{
public:
  // ****************************************************** Figure3D::creation
  Figure3D( Window& window,
            std::string title = "",
            const Range& x_range = {-1.0, 1.0, 10, 2},
            const Range& y_range = {-1.0, 1.0, 10, 2},
            const Range& z_range = {-1.0, 1.0, 10, 2}
            ) :
    //Plotter( -1.0, 1.0, -1.0, 1.0 ),
    //Plotter( 0.0, 1.0, 0.0, 1.0 ),
    Plotter( x_range._min, x_range._max, y_range._min, y_range._max ),
    _win(window),
    _title( title ),
    _should_render(true),
    _bbox_winspace( {0.1, 0.9, 0.1, 0.9} ),
    _draw_axes( true ),
    _text_list(), _font( window._font ),
    _trackball(),
    _x_axis( "X", x_range ), _y_axis( "Y", y_range ),
    _z_axis( "Z", z_range )
  {
    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);

    // glEnable(GL_BLEND);
    // glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    // register as mouse_listener
    _win.add_mouse_btn_listener( this );
    // register also as keyboard listener
    _win.add_keyboard_listener( this );
  }
  ~Figure3D()
  {
  }

  // *************************************************** Figure::set_draw_axes
  void set_draw_axes( bool draw_axes )
  {
    _draw_axes = draw_axes;
  }
  // *************************************************** Figure3D::GraphicText
  void clear_text()
  {
    _text_list.clear();
  }
  void add_text( const std::string msg, double x=0.0, double y=0.0,
                 Color col={0.0,0.0,0.0} )
  {
    _text_list.push_back( GraphicText{ x, y, msg, col } );
  }
  // ****************************************************** Figure3d::set_view
  void set_view_xup()
  {
    TrackBall::Transform ident( 1.0f );
    TrackBall::Transform view = glm::rotate( ident,
                                             - (float)M_PI/2.f, /* angle rad */
                                             glm::vec3( 0.f, 1.f, 0.f ));
    _trackball.set_transform( view );
                                  
  }
  void set_view_yup()
  {
    TrackBall::Transform ident( 1.0f );
    // translate along Oy
    TrackBall::Transform trans = glm::translate( ident,
                                                glm::vec3( 0.f, 1.f, 0.f ));
    // then rotation around Ox
    TrackBall::Transform view = glm::rotate( trans,
                                             -(float)M_PI/2.f, /* angle rad */
                                             glm::vec3( 1.f, 0.f, 0.f ));
    _trackball.set_transform( view );
                                  
  }
  void set_view_zup()
  {
    TrackBall::Transform ident( 1.0f );
    _trackball.set_transform( ident );
                                  
  }
  // ******************************************************** Figure3D::render
  virtual void render( float screen_ratio_x = 1.0, float screen_ratio_y = 1.0 )
  {
    if (_debug_fg) std::cout << "__RENDER " << _name << std::endl;
    if (not _should_render) return;

    // setup GL_PROJECTION to _bbox
    GLenum err;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    while ((err = glGetError()) != GL_NO_ERROR) {
      std::cerr << "FIG PROJECTION OpenGL error: " << err << std::endl;
    }
    glOrtho( _bbox.x_min, _bbox.x_max, _bbox.y_min, _bbox.y_max, 1.f, -1.f);
    while ((err = glGetError()) != GL_NO_ERROR) {
      std::cerr << _bbox << std::endl;
      std::cerr << "FIG ORTHO OpenGL error: " << err << std::endl;
    }
    glMatrixMode(GL_MODELVIEW);
    while ((err = glGetError()) != GL_NO_ERROR) {
      std::cerr << "FIG MODELVIEW OpenGL error: " << err << std::endl;
    }
    glLoadIdentity();

    // clear
    glClearColor( 1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    while ((err = glGetError()) != GL_NO_ERROR) {
        std::cerr << "STARTOpenGL error: " << err << std::endl;
    }
    
    // preserve GL state
    glPushMatrix();

    // the ratio receieved where in WinSpace, update to FigureSpace
    screen_ratio_x *= (_bbox.x_max - _bbox.x_min);
    screen_ratio_y *= (_bbox.y_max - _bbox.y_min);
    
    // apply Model Transform

    // apply Trackball View Transform
    glMultMatrixf( glm::value_ptr( _trackball.get_transform() ));
    glScalef( 1.f, 1.f, -1.f );
    

    // reference frame
    if (_draw_axes ) {
      draw_frame( screen_ratio_x, screen_ratio_y );
    }
    
    unsigned int nb_plot = 0;
    for( const auto& plotter: _plotters) {
      plotter->render( screen_ratio_x, screen_ratio_y );

      utils::gl::check_error();
      nb_plot++;
    }
   
    // restore GL state
    glPopMatrix();

    if (_debug_fg) {
      auto bb = get_bbox();
      std::cout << "  Fig BBox= {" << bb.x_min << "; " << bb.x_max << "; " << bb.y_min << "; " << bb.y_max << "}" << std::endl;
      std::cout << "  Fig BBox= " << get_bbox() << std::endl;
    }
    
    // GraphicText
    for( auto& txt: _text_list) {
      glColor3d( txt.col.r, txt.col.g, txt.col.b );
      glPushMatrix(); {
        glTranslated( txt.x, txt.y, -1.0);
        glScaled( screen_ratio_x, screen_ratio_y, 1.0 );
        _font->Render( txt.msg.c_str() );
      }
      glPopMatrix();
    }

    // Title
    if (_title != "") {
      glColor3d( 0.0, 0.0, 0.0 );
      glPushMatrix(); {
        // glTranslated( get_bbox().x_min + (get_bbox().x_max - get_bbox().x_min) / 2.0 - (_title_offset*screen_ratio_x),
        //               (get_bbox().y_max - 0.05 * (get_bbox().y_max - get_bbox().y_min)),
        //               0.0 );
        glTranslated( _bbox.x_min + (_bbox.x_max - _bbox.x_min) / 2.0 - (_title_offset*screen_ratio_x),
          (_bbox.y_max - 0.05 * (_bbox.y_max - _bbox.y_min)),
          0.0 );
        glScaled( screen_ratio_x, screen_ratio_y, 1.0 );
        _font->Render( _title.c_str() );
      } glPopMatrix();
    }
    if (_debug_fg) std::cout << "__end render " << _name << std::endl;
  }
  virtual void render_last( float screen_ratio_x = 1.0,
                            float screen_ratio_y = 1.0 )
  {
    if (_debug_fg) std::cout << "__RENDER_LAST " << _name << std::endl;
    
    std::cout << "__Figure3D::render_last: NOT COMPATIBLE WITH StaticWin" << std::endl;
    exit(1);

    if (_debug_fg) std::cout << "__end render_last " << _name << std::endl;
  }
  // ********************************************************** Figure3D::draw
  void draw_frame( float screen_ratio_x = 1.0, float screen_ratio_y = 1.0 )
  {
    _x_axis.render( screen_ratio_x, screen_ratio_y );
    glPushMatrix();
    glRotatef( 90.0, 0.0, 0.0, 1.0 ); // ang, x,y,z
    //glRotatef( 90.0, 0.0, 1.0, 0.0 ); // ang, x,y,z
    _y_axis.render( screen_ratio_x, screen_ratio_y );
    glPopMatrix();
    glPushMatrix();
    glRotatef( -90.0, 0.0, 1.0, 0.0 ); // ang, x,y,z because left-hand syst
    //glRotatef( 90.0, 0.0, 1.0, 0.0 ); // ang, x,y,z
    _z_axis.render( screen_ratio_x, screen_ratio_y );
    glPopMatrix();
  }
  // *********************************************** Figure3D::on_mouse_button
  /** should be called by appropriate Window */
  virtual void on_mouse_button( int button, int action, int mods )
  {
    double x, y;
    glfwGetCursorPos( _win._window, &x, &y);
    //std::cout <<"Figure3D::Mouse Button at (" << x <<  ", " << y << ")\n";

    // clear_text();
    // std::stringstream msg;
    // msg << "Mouse : (" << x <<  ", " << y << ")";
    // add_text( msg.str(), 0.02, 0.90 );

    _trackball.on_mouse_btn( button, action, mods,
                             x/(_win._width), y/(_win._height));
  }
  virtual void on_mouse_move( double xpos, double ypos )
  {
    double x, y;
    glfwGetCursorPos( _win._window, &x, &y);
    //std::cout <<"Figure3D::Mouse move at (" << x <<  ", " << y << ")\n";

    // clear_text();
    // std::stringstream msg;
    // msg << "Mouse move : (" << x <<  ", " << y << ")";
    // add_text( msg.str(), 0.02, 0.90 );

    _trackball.on_mouse_move( x/(_win._width), y/(_win._height) );
  }
  virtual void on_mouse_scroll( double xoffset, double yoffset )
  {
    _trackball.on_mouse_scroll( xoffset, yoffset );
  }
  // ********************************************************** Figure3D::on_key
  virtual void on_key( int key, int scancode, int action, int mods)
  {
    if (key == GLFW_KEY_R && action == GLFW_PRESS) {
      _trackball.reset();
    }
    else if (key == GLFW_KEY_D && action == GLFW_PRESS) {
      auto vtrans = _trackball.get_transform();
      std::cout << "DEBUG TRANS **************************" << std::endl;
      std::cout << _trackball.pretty_4x4( vtrans ) << std::endl;
    }
    else if (key == GLFW_KEY_KP_2 && action == GLFW_PRESS) {
      set_view_xup();
    }
    else if (key == GLFW_KEY_KP_6 && action == GLFW_PRESS) {
      set_view_yup();
    }
    else if (key == GLFW_KEY_KP_8 && action == GLFW_PRESS) {
      set_view_zup();
    }
  }
  
  // *********************************************** Figure3D::BBox for Window
  /** TODO constant value in WindowSpace */
  virtual const BoundingBox& get_bbox() const
  {
    return _bbox_winspace;
  }
  
  // ***************************************************** Figure3D::attributs
public:
  TrackBall::Transform get_transform() const
  {
    return _trackball.get_transform();
  }
public:
  Window& _win;
  std::string _title;
  bool _should_render;
  BoundingBox _bbox_winspace;
  
  bool _draw_axes;
  
  /** List of Graphictext */
  std::list<GraphicText> _text_list;
  /** Fonts to write text */
  FTFont* _font;
  double _title_offset;
  
  /** Trackball */
  TrackBall _trackball;

  Axis _x_axis, _y_axis, _z_axis;
}; // Figure3D
}; // namespace visugl

#endif // FIGURE3D_HPP
