/* -*- coding: utf-8 -*- */

#ifndef SHAPE2D_HPP
#define SHAPE2D_HPP

/**
 * Shape2D has
 * - Sample _origin
 * - double orient
 * - Color _fg_col;
 *
 * render( ratio_x, ratio_y );
 * */

#include <array>

#include "visugl.hpp"
#include "gl_utils.hpp"

#define GLM_FORCE_RADIANS
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>

namespace visugl {
struct Shape2D {
  Shape2D() : _origin{0.0, 0.0, 0.0}, _orient{0.0},
              _fg_col{0.0, 0.0, 0.0} {
  }
  virtual void update_bbox() {}
  virtual void render( float screen_ratio_x = 1.0, float screen_ratio_y = 1.0 ) {}

  Sample _origin;
  double _orient;
  Color  _fg_col;
}; // struct Shape2D

const glm::vec3 vec_Oz = glm::vec3( 0.0, 0.0, 1.0 );
// ***************************************************************** transform
void begin_transform2D( const Sample& translation,
                        double rotation_angle)
{
  // rotation and translation of Shape2D
  glm::mat4 identity_m( 1.0 ); // identity
  glm::mat4 rotate_m = glm::rotate( identity_m,
                                    static_cast<float>(rotation_angle),
                                    vec_Oz );
  // then translation
  glm::mat4 transform_m = glm::translate( rotate_m,
                                          glm::vec3( translation.x,
                                                     translation.y,
                                                     translation.z ));

  glPushMatrix();
  utils::gl::check_error();
  glMultMatrixf(  glm::value_ptr( transform_m ));
  utils::gl::check_error();
}
void end_transform2D()
{
  glPopMatrix();
  utils::gl::check_error();
}

# define CIRCLE_NB_ELEMENTS 64
// Compute constexpr table of cosine and sine
// TODO only for c++14 and above
// template<size_t SZ>
// constexpr std::array<double, SZ> compute_sine_table()
// {
//     std::array<double, SZ> table{};
//     const auto pi = std::acos(-1); // pi is not a constant in c++11

//     for (size_t n = 0; n < SZ; ++n) {
//         table[n] = std::sin(2 * pi * static_cast<double>(n) / static_cast<double>(SZ-1));
//     }

//     return table;
// }
// template<size_t SZ>
// constexpr std::array<double, SZ> compute_cosine_table()
// {
//     std::array<double, SZ> table{};
//     const auto pi = std::acos(-1); // pi is not a constant in c++11

//     for (size_t n = 0; n < SZ; ++n) {
//         table[n] = std::cos(2 * pi * static_cast<double>(n) / static_cast<double>(SZ-1));
//     }

//     return table;
// }
// ***************************************************************************

class Circle2D : public Shape2D {
public:
  Circle2D( double radius) : Shape2D(), _radius(radius), _width(1.0) {
    const auto pi = std::acos(-1); // pi is not a constant in c++11
    for (size_t n = 0; n < CIRCLE_NB_ELEMENTS; ++n) {
        cos_tab[n] = std::cos(2 * pi * static_cast<double>(n) / static_cast<double>(CIRCLE_NB_ELEMENTS-1));
        sin_tab[n] = std::sin(2 * pi * static_cast<double>(n) / static_cast<double>(CIRCLE_NB_ELEMENTS-1));
    }
  }
  virtual ~Circle2D() {}

  virtual void render( float screen_ratio_x = 1.0, float screen_ratio_y = 1.0 ) {

    // Color
    glColor4d( _fg_col.r, _fg_col.g, _fg_col.b, 1.0);
    utils::gl::check_error();

    // -------------------------------------------------------------------------
    //  Rendering using GL_LINE_STRIP
    // -------------------------------------------------------------------------
    glEnable (GL_BLEND);
    utils::gl::check_error();
    glEnable (GL_LINE_SMOOTH);
    utils::gl::check_error();
    glLineWidth( _width );
    utils::gl::check_error();

    glBegin(GL_LINE_STRIP);
    for (int id = 0; id < CIRCLE_NB_ELEMENTS; ++id) {
      glVertex3d( _radius * cos_tab[id],
                  _radius * sin_tab[id],
                  0.0 );
    }
    glEnd();
    utils::gl::check_error();

  }

  // ******************************************************* Circle2D::attributs
  void set_color( const Color& col )
  {
    _fg_col = col;
  }
  void set_width( const GLfloat& width )
  {
    _width = width;
  }

private:
  double _radius;
  double _width;
  // TODO not constexpr
  std::array<double,CIRCLE_NB_ELEMENTS> cos_tab;
  std::array<double,CIRCLE_NB_ELEMENTS> sin_tab;
  // auto cos_tab = compute_cosine_table<CIRCLE_NB_ELEMENTS>();
  // auto sin_tab = compute_sine_table<CIRCLE_NB_ELEMENTS>();
}; // class Circle2D


class Rect2D : public Shape2D {
public:
  Rect2D( double width, double height) : Shape2D(), _size_x(width), _size_y(height)
  {}
  virtual ~Rect2D() {}

  virtual void render( float screen_ratio_x = 1.0, float screen_ratio_y = 1.0 ) {

    // Color
    glColor4d( _fg_col.r, _fg_col.g, _fg_col.b, 1.0);
    utils::gl::check_error();

    // -------------------------------------------------------------------------
    //  Rendering using GL_LINE_STRIP
    // -------------------------------------------------------------------------
    glEnable (GL_BLEND);
    utils::gl::check_error();

    glBegin(GL_TRIANGLE_STRIP); {
      glVertex3d( -_size_x/2.0, _size_y/2.0, 0.0 );
      glVertex3d( -_size_x/2.0, -_size_y/2.0, 0.0 );
      glVertex3d( _size_x/2.0, _size_y/2.0, 0.0 );
      glVertex3d( _size_x/2.0, -_size_y/2.0, 0.0 );
    }
    glEnd();
    utils::gl::check_error();

  }
  // ******************************************************* Circle2D::attributs
  void set_color( const Color& col )
  {
    _fg_col = col;
  }

private:
  double _size_x, _size_y;
};
}; // namespace visugl
#endif // SHAPE2D_HPP
