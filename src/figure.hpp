/* -*- coding: utf-8 -*- */

#ifndef FIGURE_HPP
#define FIGURE_HPP

/** 
 * A Figure has Axes and is a Plotter (which can containes Plotter).
 * Can have a title.
 *
 * Reminder: Winholder will use get_bbox() to position the Figure. So, Figure
 * changes the semantics of get_bbox() to return its _bbox_winspace instead.
 *
 * get_bbox() : return the layout of the Figure in Winspace (should not vary much)
 * get_limits() : return the BBox actually used to render Plotters.
 */

#include <iostream>                  // std::cout
#include <string>                    // std::string

#include "logging.hpp"
#include "base_types.hpp"
#include "bounding_box.hpp"
#include "plotter.hpp"
//#include "window.hpp"
#include "axis.hpp"


// Minimum size for Figure Bounding Box
#define FIGMINSIZE (0.01)

namespace visugl {
// ***************************************************************************
// ******************************************************************** Figure
// ***************************************************************************
class Figure : public Plotter
{
public:
  // ******************************************************** Figure::creation
  Figure( std::string title = "",
	  const Range& x_range = {-1.0, 1.0, 10, 2},
	  const Range& y_range = {-1.0, 1.0, 10, 2} ) :
    Plotter( x_range._min, x_range._max, y_range._min, y_range._max ),
    _title( title ), _should_render(true),
    _bbox_winspace( {0.02, 0.98, 0.02, 0.98} ),
    _update_axes_x( false ), _update_axes_y( false ),
    _draw_axes( true ),
    _axis_x( "X", x_range),
    _axis_y( "Y", y_range),
    _axispos_x( 0.0), _axispos_y( 0.0 ),
    _text_list(),
    _font( grafik::font() ), _title_offset( 0.0 )
  {
    // make sure figure various bbox and title_size are computed
    set_bbox(_bbox);
    _axis_x.set_hpos( _hpos );
  }
  // ***************************************************** Figure::destruction
  ~Figure()
  {
  }

  // *************************************************** Figure::set_draw_axes
  void set_draw_axes( bool draw_axes )
  {
    _draw_axes = draw_axes;
  }
  // ***************************************************** Figure::GraphicText
  void clear_text()
  {
    _text_list.clear();
  }
  void add_text( const std::string& msg, double x=0.0, double y=0.0, Color col={0.0,0.0,0.0} )
  {
    _text_list.push_back( GraphicText{ x, y, msg, col } );
  }
  void set_title( const std::string& title )
  {
    _title = title;
  }
  // ********************************************************** Figure::update
  BoundingBox get_innerbbox()
  {
    // Find innerbox of all children
    BoundingBox bbox; // empty by construction
    bbox.set_empty();    // but make sure
    
    for( const auto& plotter: _plotters ) {
      LOGBBOX( "  calling "<< plotter->_name );
      plotter->update_bbox();
      bbox.update( plotter->get_bbox() );
    }

    // minimum size
    bbox.min_size( FIGMINSIZE );
    
    return bbox;
  }
  virtual void update_bbox()
  {
    LOGBBOX( "__UPDF " << _name << " start limits=" << get_limits() );

    auto innerbox = get_innerbbox();
    set_bbox( innerbox.x_min, innerbox.x_max, innerbox.y_min, innerbox.y_max );
    
    LOGBBOX( "  UPDF " << _name << " end limits= " << get_limits() );
    LOGBBOX( "       " << _name << "     outerBB= " << get_bbox() );
  }
  void set_bbox( const BoundingBox& bb )
  {
    set_bbox( bb.x_min, bb.x_max, bb.y_min, bb.y_max );
  }
  virtual void set_bbox( double x_min, double x_max,
                         double y_min, double y_max )
  {
    LOGBBOX( "__SETF " << _name << " start limits=" << get_limits() );

    _bbox.set( x_min, x_max, y_min, y_max );
    // minimum size
    _bbox.min_size( FIGMINSIZE );

    _axis_x = Axis( "X", {_bbox.x_min, _bbox.x_max, 10, 2});
    _axis_y = Axis( "Y", {_bbox.y_min, _bbox.y_max, 10, 2});
    // Some room for title
    if (_title != "" ) {
      LOGTITLE( "__set_bbox " << _name << " BB=" << get_limits() );
      //_bbox.y_max += 0.1* (_bbox.y_max - _bbox.y_min);
      auto titlebox = _font->BBox( _title.c_str() );
      _title_offset = (titlebox.Upper().X() - titlebox.Lower().X()) / 2.0;

      LOGTITLE( "  changed to BB=" << get_limits() );
      LOGTITLE( "  titleBox lowx=" << titlebox.Lower().X() << " lowy=" << titlebox.Lower().Y() << " higx=" << titlebox.Upper().X() << " higy=" << titlebox.Upper().Y() );
      LOGTITLE( "  offset=" << _title_offset );
    }

    // position of center for Axis
    _axispos_x = 0.0;
    _axispos_y = 0.0;
    if (_bbox.x_min > 0.0 or _bbox.x_max < 0.0)
      _axispos_x = _bbox.x_min;// + 0.05 * (_bbox.x_max - _bbox.x_min);
    if (_bbox.y_min > 0.0 or _bbox.y_max < 0.0)
      _axispos_y = _bbox.y_min;// + 0.05 * (_bbox.y_max - _bbox.y_min);

    LOGBBOX( "  SETF " << _name << " end limits= " << get_limits() );
    LOGBBOX( "       " << _name << "     outerBB= " << get_bbox() );
  }
  // ********************************************************** Figure::render
  virtual void render( float screen_ratio_x = 1.0, float screen_ratio_y = 1.0 )
  {
    // if needed, update _bbox
    if (_debug_fg) std::cout << "__RENDER " << _name << std::endl;
    LOGTITLE( "__RENDER " << _name << "ratio_x=" << 1./screen_ratio_x << " ratio_y=" << 1./screen_ratio_y );
    if (not _should_render) return;

    if( _update_axes_x || _update_axes_y ) {
      auto innerbox = get_innerbbox();
 
      // TODO: only update, NOT recreate !!!
      if( _update_axes_x) {
        _axis_x = Axis( "X", {innerbox.x_min, innerbox.x_max, 10, 2});
        _axis_x.set_hpos( _hpos );
      }
      if( _update_axes_y )
        _axis_y = Axis( "Y", {innerbox.y_min, innerbox.y_max, 10, 2});
      // Some room for title
      if (_title != "" ) {
        innerbox.y_max += 0.1* (innerbox.y_max - innerbox.y_min);
        // TODO redundant with _bbox.set( innerbox  );
        auto titlebox = _font->BBox( _title.c_str() );
        _title_offset = (titlebox.Upper().X() - titlebox.Lower().X()) / 2.0;
      }

      _bbox.set( innerbox );
      // position of center for Axis
      _axispos_x = 0.0;
      _axispos_y = 0.0;
      if (_bbox.x_min > 0.0 or _bbox.x_max < 0.0)
        _axispos_x = _bbox.x_min;// + 0.05 * (_bbox.x_max - _bbox.x_min);
      if (_bbox.y_min > 0.0 or _bbox.y_max < 0.0)
        _axispos_y = _bbox.y_min;// + 0.05 * (_bbox.y_max - _bbox.y_min);
    }

    // clear
    glClearColor( 1.0, 1.0, 1.0, 1.0);
    grafik::check_error();
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    grafik::check_error();

    // setup GL_PROJECTION to _bbox
    glMatrixMode(GL_PROJECTION);
    grafik::check_error();
    glLoadIdentity();
    grafik::check_error();
    glOrtho( get_limits().x_min, get_limits().x_max, get_limits().y_min, get_limits().y_max, 1.f, -1.f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    grafik::check_error();

    // the ratio recieved where in WinSpace, update to FigureSpace
    screen_ratio_x *= (_bbox.x_max - _bbox.x_min);
    screen_ratio_y *= (_bbox.y_max - _bbox.y_min);
    LOGTITLE( "  UPD ratio ratio_x=" << 1./screen_ratio_x << " ratio_y=" << 1./screen_ratio_y );

    // Plotters
    unsigned int nb_plot = 0;
    for( const auto& plotter: _plotters) {
      plotter->render( screen_ratio_x, screen_ratio_y );
      grafik::check_error();
    
      nb_plot++;
    }

    if (_debug_fg) {
      auto bb = get_limits();
      std::cout << "  Fig limitsBBox= {" << bb.x_min << "; " << bb.x_max << "; " << bb.y_min << "; " << bb.y_max << "}" << std::endl;
      std::cout << "  Fig outerBBox= " << get_bbox() << std::endl;
      std::cout << "  Axes at " << _axispos_x << ", " << _axispos_y << std::endl;
    }
    LOGBBOX( "  limits= " << get_limits() );
    LOGBBOX( "  outerBB= " << get_bbox() );
    
    // Basic axes
    if( _draw_axes ) {
      glPushMatrix(); // AXE_Y
      grafik::check_error();
      glTranslated( 0.0, _axispos_y, 0.0 );
      grafik::check_error();
      _axis_x.render( screen_ratio_x, screen_ratio_y );
      grafik::check_error();
      glPopMatrix();
      grafik::check_error();

      glPushMatrix(); // AXE_Y
      grafik::check_error();
      glTranslated( _axispos_x, 0.0, 0.0 );
      grafik::check_error();
      glRotated( 90.0, 0.0, 0.0, 1.0 );
      grafik::check_error();
      _axis_y.render( screen_ratio_y, screen_ratio_x);
      grafik::check_error();
      glPopMatrix();
      grafik::check_error();
    }

    // GraphicText
    for( auto& txt: _text_list) {
      glColor3d( txt.col.r, txt.col.g, txt.col.b );
      grafik::check_error();
      glPushMatrix(); {
        grafik::check_error();
        glTranslated( txt.x, txt.y, 0.0);
        grafik::check_error();
        glScaled( screen_ratio_x, screen_ratio_y, 1.0 );
        grafik::check_error();
        _font->Render( txt.msg.c_str() );
        grafik::check_error();
      } glPopMatrix();
      grafik::check_error();
    }
    // Title
    if (_title != "") {
      glColor3d( 0.0, 0.0, 0.0 );
      grafik::check_error();
      glPushMatrix(); {
        grafik::check_error();
        // TODO fonts render point is lowerleft => update ??
        auto x_tit = (get_limits().x_min + get_limits().x_max) / 2.0
          - (_title_offset*screen_ratio_x);
        auto y_tit = (get_limits().y_max - 0.05 * (get_limits().y_max - get_limits().y_min));
        LOGTITLE( "__render" << _name );
        LOGTITLE( "  offset=" << _title_offset );
        LOGTITLE( "  title posx=" << x_tit << " posy=" << y_tit );
        glTranslated( x_tit, y_tit, 0.0 );
        grafik::check_error();
        glScaled( screen_ratio_x, screen_ratio_y, 1.0 );
        _font->Render( _title.c_str() );
        grafik::check_error();
      } glPopMatrix();
      grafik::check_error();
    }
    if (_debug_fg) std::cout << "__end render " << _name << std::endl;
  }
   /** Only render from _last to end */
  virtual void render_last( float screen_ratio_x = 1.0,
                            float screen_ratio_y = 1.0 )
  {
    if (_debug_fg) std::cout << "__RENDER_LAST " << _name << std::endl;

    // setup GL_PROJECTION to _bbox
    glMatrixMode(GL_PROJECTION);
    grafik::check_error();
    glLoadIdentity();
    grafik::check_error();
    glOrtho( get_limits().x_min, get_limits().x_max, get_limits().y_min, get_limits().y_max, 1.f, -1.f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    unsigned int nb_plot = 0;
    for( const auto& plotter: _plotters) {
      plotter->render_last( screen_ratio_x, screen_ratio_y );

      grafik::check_error();
      nb_plot++;
    }
    if (_debug_fg) std::cout << "__end render_last " << _name << std::endl;
  }
  // ************************************************* Figure::BBox for Window
  /** WARNING return constant value in WindowSpace */
  virtual const BoundingBox& get_bbox() const
  {
    return _bbox_winspace;
  }
  /** return the actual limits used to render Plotters*/
  const BoundingBox& get_limits() const
  {
    return _bbox;
  }
  // ******************************************************* Figure::attributs
  void set_hpos( double val )
  {
    _hpos = val;
    _axis_x.set_hpos( _hpos );
  }
public:
  std::string _title;
  bool _should_render;
  BoundingBox _bbox_winspace;
  /** X and Y axes*/
  bool _update_axes_x, _update_axes_y;
  bool _draw_axes;
  Axis _axis_x, _axis_y;
  double _axispos_x, _axispos_y;
  /** List of Graphictext */
  std::list<GraphicText> _text_list;
  /** Fonts to write text */
  FTFont* _font;
  double _title_offset; // half size of title in px
protected:
  // TODO: vertical position of XAxis
  double _hpos = 0.0;
}; // class Figure

using FigurePtr = Figure*;

}; // namespace visugl;

#endif // FIGURE_HPP
