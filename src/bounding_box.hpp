/* -*- coding: utf-8 -*- */

#ifndef BOUNDING_BOX_HPP
#define BOUNDING_BOX_HPP

/** 
 * BoundingBox is (x_min, x_max) x (y_min, y_max)
 * Empty BoundingBox is (inf, -inf) x (inf, -inf)
 *
 * Can be updated using Sample or another BoundingBox.
 * - expand.
 */

#include <limits>                // std::numeric_limits
#include <math.h>                // dmin, fmax
#include "visugl.hpp"

namespace visugl
{
class BoundingBox
{
public:
  // *************************************************** BoundingBox::creation
  BoundingBox( double x_min = std::numeric_limits<double>::max(),
               double x_max = -std::numeric_limits<double>::max(),
               double y_min = std::numeric_limits<double>::max(),
               double y_max = -std::numeric_limits<double>::max()) :
    x_min(x_min), x_max(x_max), y_min(y_min), y_max(y_max)
  {}
  
  // ****************************************************** BoundingBox::empty
  void set_empty()
  {
    x_min = std::numeric_limits<double>::max();
    x_max = -std::numeric_limits<double>::max();
    y_min = std::numeric_limits<double>::max();
    y_max = -std::numeric_limits<double>::max(); 
  }
  // ******************************************************** BoundingBox::set
  void set( double x_min, double x_max, double y_min, double y_max )
  {
    this->x_min = x_min;
    this->x_max = x_max;
    this->y_min = y_min;
    this->y_max = y_max;
  }
  void set( const BoundingBox& bbox )
  {
    set( bbox.x_min, bbox.x_max, bbox.y_min, bbox.y_max );
  }
  /**
   * Only changed if bbox bigger than *this OR
   * if smaller by less than `margin x width|height`
   *
   * Returns: true if changed.
   */
  bool set_withmargin( const BoundingBox& bbox, double margin )
  {
    bool changed = false;

    if( bbox.x_min < x_min ) {
      changed = true;
      x_min = bbox.x_min;
    }
    else if( bbox.x_min > x_min + margin * (x_max - x_min)) {
      changed = true;
      x_min = bbox.x_min;
    }
    if( bbox.x_max > x_max ) {
      changed = true;
      x_max = bbox.x_max;
    }
    else if( bbox.x_max < x_max - margin * (x_max - x_min)) {
      changed = true;
      x_max = bbox.x_max;
    }

    if( bbox.y_min < y_min ) {
      changed = true;
      y_min = bbox.y_min;
    }
    else if( bbox.y_min > y_min + margin * (y_max - y_min)) {
      changed = true;
      y_min = bbox.y_min;
    }
    if( bbox.y_max > y_max ) {
      changed = true;
      y_max = bbox.y_max;
    }
    else if( bbox.y_max < y_max - margin * (y_max - y_min)) {
      changed = true;
      y_max = bbox.y_max;
    }

    return changed;
  }
    
  // ***************************************************** BoundingBox::update
public:
  void update( const Sample& s)
  {
    x_min = fmin( x_min, s.x );
    x_max = fmax( x_max, s.x );
    y_min = fmin( y_min, s.y );
    y_max = fmax( y_max, s.y );
  }
  void update( const BoundingBox& bbox )
  {
    x_min = fmin( x_min, bbox.x_min );
    x_max = fmax( x_max, bbox.x_max );
    y_min = fmin( y_min, bbox.y_min );
    y_max = fmax( y_max, bbox.y_max );
  }
  // *************************************************** BoundingBox::min_size
  void min_size( double min_size )
  {
    if( (x_max - x_min) < min_size ) {
      double xcenter = (x_max - x_min)/2.0;
      x_min = xcenter - min_size/2.0;
      x_max = xcenter + min_size/2.0;
    }
    if( (y_max - y_min) < min_size ) {
      double ycenter = (y_max - y_min)/2.0;
      y_min = ycenter - min_size/2.0;
      y_max = ycenter + min_size/2.0;
    }    
  }
  // ****************************************************** BoundingBox::scale
  void scale( double scaling=1.0 )
  {
    double val = scaling - 1.0;
    set( x_min - (x_max-x_min) * val, x_max + (x_max-x_min) * val,
         y_min - (y_max-y_min) * val, y_max + (y_max-y_min) * val );
  }
  // ************************************************** BoundingBox::attributs
  double x_min, x_max, y_min, y_max;
}; // BoundingBox
}; // namespace visugl

std::ostream& operator<<( std::ostream& os, const visugl::BoundingBox& bbox )
{
  os << "{" << bbox.x_min <<"; " << bbox.x_max << "; ";
  os << bbox.y_min << "; " << bbox.y_max << "}";
  return os;
}

#endif // BOUNDING_BOX_HPP
