/* -*- coding: utf-8 -*- */

#ifndef PLOTTER_HPP
#define PLOTTER_HPP

/** 
 * Basic characteristics of Plotter
 * BoundingBox _bbox
 * PlotterList _plotters
 * bool _should_render
 *
 * Debug, with some examples in `render` and `render_last`
 * if (_debug_fg) std::cout << "__RENDER " << _name << std::endl;
 * if (_debug_fg) std::cout << "__end render " << _name << std::endl;
 * 
 * std::string _idname  -> "plt_XX" where XX increases
 * bool _debug_fg
 * Plotter::unsigned int _nb_plotter
 *
 * update_bbox()
 * render( ratio_x, ratio_y )
 */

#include <iostream>
#include <list>
#include <string>

#include "base_types.hpp"
#include "bounding_box.hpp"

namespace visugl {
class Plotter;
using PlotterPtr = Plotter*;
using PlotterList = std::list<PlotterPtr>;
// ***************************************************************************
// ******************************************************************* Plotter
// ***************************************************************************
class Plotter
{
public:
  // ****************************************************** Plotter::creation
  Plotter() : _plotters(), _bbox{0.0, 1.0, 0.0, 1.0}, _should_render(true),
              _debug_fg(false)
  {
    _set_default_name();
  }
  Plotter( double x_min, double x_max, double y_min, double y_max) :
    _plotters(),
    _bbox{x_min, x_max, y_min, y_max},
    _should_render(true),
    _debug_fg(false)    
  {
    _set_default_name();
    LOGBBOX( "__CREATE Plotter " << _name << " BB=" << get_bbox() );
  }
  virtual ~Plotter()
  {
  }
  // ******************************************************* Plotter::set_name
  virtual void set_name( const std::string &name )
  {
    LOGBBOX( "__RENAME Plotter " << _name << " => " << name );
    _name = name;

  }
protected:
  void _set_default_name() // and increase Plotter::_nb_plotter
  {
    _name = "plt_"+std::to_string( _nb_plotter );
    _nb_plotter += 1;
  }
  // *************************************************** Plotter::add_plotters
public:
  /* Cannot add self */
  virtual bool add_plotter( const PlotterPtr plotter )
  {
    if (plotter != this) {
      _plotters.push_back( plotter );
      return true;
    }
    return false;
  }
  virtual void remove_plotter( const PlotterPtr plotter )
  {
    _plotters.remove( plotter );
  }
  /** Usually called by children to notify of something */
  virtual void children_changed( std::string msg )
  {
    std::cout << "__Plotter::children_changed: TO IMPLEMENT" << std::endl;
  }
  // **************************************************** Plotter::update_bbox
  virtual void update_bbox()
  {
    std::cout << "__Plotter::update_bbox: TO IMPLEMENT" << std::endl;
  }
  // ********************************************************* Plotter::render
  virtual void render( float screen_ratio_x = 1.0, float screen_ratio_y = 1.0 )
  {
    if (_debug_fg) std::cout << "__RENDER " << _name << std::endl;
       
    std::cout << "__Plotter::render: TO IMPLEMENT for " << _name << std::endl;

    if (_debug_fg) std::cout << "__end render " << _name << std::endl;
  }
  virtual void render_last( float screen_ratio_x = 1.0,
                            float screen_ratio_y = 1.0 )
  {
    if (_debug_fg) std::cout << "__RENDER_LAST " << _name << std::endl;
    
    std::cout << "__Plotter::render_last: TO IMPLEMENT IF POSSIBLE for " << _name << std::endl;

    if (_debug_fg) std::cout << "__end render_last " << _name << std::endl;
  }
  // ****************************************************** Plotter::callbacks
  virtual void on_mouse_button( int button, int action, int mods )
  {
    std::cout << "__Plotter::on_mouse_btn: TO IMPLEMENT" << std::endl;
  }
  virtual void on_mouse_move( double xpos, double ypos )
  {
    std::cout << "__Plotter::on_mouse_move: TO IMPLEMENT" << std::endl;
  }
  virtual void on_mouse_scroll( double xoffset, double yoffset )
  {
    std::cout << "__Plotter::on_mouse_scroll: TO IMPLEMENT" << std::endl;
  }
  virtual void on_key( int key, int scancode, int action, int mods)
  {
    std::cout << "__Plotter::on_key: TO IMPLEMENT" << std::endl;
  }
  // ********************************************************** Plotter::debug
  virtual void set_debug( bool debug_fg )
  {
    _debug_fg = debug_fg;
  }
  // ****************************************************** Plotter::attributs
  /** get BoundingBox */
  virtual const BoundingBox& get_bbox() const {return _bbox;}
  //void set_bbox( const BoundingBox& bbox ) { _bbox = bbox; }

  /** Other things to plot */
  PlotterList _plotters;
  // TODO: private
  BoundingBox _bbox;
  bool _should_render;

  // Mostly for Debug
  std::string _name;
  bool _debug_fg;
  static unsigned int _nb_plotter;

}; // Plotter
}; // namespace visugl

unsigned int visugl::Plotter::_nb_plotter = 0;

#endif // PLOTTER_HPP
