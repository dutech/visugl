/* -*- coding: utf-8 -*- */

#ifndef TRACKBALL_HPP
#define TRACKBALL_HPP

/** 
 * Store and Manage scene position and orientation using the mouse.
 * - Left : rotate
 * - SHIFT+Left : move
 * - SCROLL or Right : zoom
 *
 * Technically, the Transformation is stored in Transform _view_m.
 * When an Action (ZOOM,ROTATE,MOVE) is initiated, a current Transform is 
 * maintained in _current_view_m. It is definitively applied to _view_m
 * when the action is completed.
 * Currently : start = MousePress, completed = MouseUp
 *
 * Do not forget that transform apply in REVERSE order.
 */

#include <iostream>
#include <sstream>
#include <iomanip>

#define GLM_FORCE_RADIANS
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/matrix_decompose.hpp>

namespace visugl {
// ***************************************************************************
// ***************************************************************** TrackBall
// ***************************************************************************
class TrackBall
{
private:
  // ******************************************************* TrackBall GLOBALS
  enum class         MouseAction {NOTHING,ZOOM,ROTATE,MOVE};
  using ScreenPos = glm::vec2;
  using Quaternion = glm::quat;
#define ZOOM_COEF  1.05f
#define ZOOM_MAX  10.0f
#define ZOOM_MIN   0.1f
#define EPSILON 0.000001f
#define TRACKBALL_SIZE 0.8
public:
  using Transform = glm::mat4;
  
public:
  // ************************************************** TrackBall::constructor
  TrackBall() :
    _action(MouseAction::NOTHING), _start(0.0),
    _zoom(1.f),
    _view_m( 1.0 ), _current_view_m( 1.0 ), // view Transform set to identity
    _scaled_view_m( 1.0f ), _identity( 1.0 )
  {
    //std::cout << "__TRACKBALL creation" << str_dump() << std::endl;
  }
  // ********************************************************** TrackBall::str
  std::string str_dump () const
  {
    std::stringstream dump;
    dump << "__TrackBall :";
    dump << " action=";
    switch(_action) {
    case MouseAction::NOTHING: dump << "NOTHING"; break;
    case MouseAction::ZOOM: dump << "ZOOM"; break;
    case MouseAction::ROTATE: dump << "ROTATE"; break;
    case MouseAction::MOVE: dump << "MOVE"; break;
    }
    dump << " _start=" << glm::to_string(_start);

    dump << std::endl << "  zoom=" << _zoom;
    dump << std::endl << pretty_4x4( _view_m );
    
    return dump.str();
  }
  std::string pretty_4x4( const Transform& mat ) const
  {
    std::stringstream pretty;
    for( unsigned int i = 0; i < 4; ++i) {
      for( unsigned int j = 0; j < 4; ++j) {
        pretty << std::setw(6) << std::setprecision(2) << mat[i][j] << ", ";
      }
      pretty << std::endl;
    }
    return pretty.str();
  }
  // ******************************************************** TrackBall::reset
  void reset()
  {
    _zoom = 1.f;
    _view_m = Transform( 1.0 );         // identity
    _current_view_m = Transform( 1.0 ); // identity
    _scaled_view_m = glm::scale( _current_view_m, 
                                 glm::vec3( _zoom, _zoom, _zoom ));
  }
  // ************************************************** TrackBall:on_mouse_btn
  /** Change _action and _start accordingly */
  void on_mouse_btn( int button, int action, int mods,
                     double xfig, double yfig )
  {
    if( action == GLFW_PRESS ) {
      if( button == GLFW_MOUSE_BUTTON_LEFT ) {
	// With SHIFT ??
	if( mods & GLFW_MOD_SHIFT ) {
	  _start = ScreenPos(xfig, yfig);
	  _action = MouseAction::MOVE;
          //std::cout << "BTN SHIFT+Left " << str_dump() << std::endl;
	}
	else {
          _start = ScreenPos(xfig, yfig);
	  _action = MouseAction::ROTATE;
          //std::cout << "BTN Left " << str_dump() << std::endl;
	}
      }
      else if( button == GLFW_MOUSE_BUTTON_RIGHT ) {
	_start = ScreenPos(xfig, yfig);
	_action = MouseAction::ZOOM;
        //std::cout << "BTN Right " << str_dump() << std::endl;
      }
    }
    else if( action == GLFW_RELEASE ) {
      // apply current Transform
      _view_m = _current_view_m;
      _scaled_view_m = glm::scale( _view_m, 
                               glm::vec3( _zoom, _zoom, _zoom ));
      _action = MouseAction::NOTHING;
      
      //std::cout << "BTN Release " << str_dump() << std::endl;
    }
  }
  // ************************************************ TrackBall::on_mouse_move
  void on_mouse_move( double xpos, double ypos )
  {
    // Depending on _action
    switch( _action ) {
    case MouseAction::ZOOM: {
      // std::cout << "  zoom" << std::endl;
      _zoom = _zoom * (1.0 - (_start[1] - ypos));
      if( _zoom > ZOOM_MAX ) _zoom = ZOOM_MAX;
      else if( _zoom < ZOOM_MIN ) _zoom = ZOOM_MIN;
      _start = ScreenPos(xpos,ypos);
      
      break;
    }
      
    case MouseAction::ROTATE: {
      // std::cout << "  rotate" << std::endl;
      // std::cout << "  ***** _view_m **** " << std::endl;
      // std::cout << pretty_4x4( _view_m ) << std::endl;
      auto current_rot = compute_rotation( (2.0 * _start[0] - 1.0),
                                       (1.0 - 2.0 * _start[1]),
                                       (2.0 * xpos - 1.0),
                                       (1.0 - 2.0 * ypos) );
      // std::cout << "  ***** current_rot " << glm::to_string(current_rot) << std::endl;
      _current_view_m = mat4_cast( current_rot ) * _view_m;
      // std::cout << "  ***** _current_view_m **** " << std::endl;
      // std::cout << pretty_4x4( _current_view_m ) << std::endl;

      break;
    }
      
    case MouseAction::MOVE: {
      // std::cout << "  move" << std::endl;
      _current_view_m = glm::translate( _identity,
                                        glm::vec3( 2*(xpos-_start[0]),
                                                   -2*(ypos-_start[1]),
                                                   0 ) ) * _view_m;
      // std::cout << "  ***** _current_view_m **** " << std::endl;
      // std::cout << pretty_4x4( _current_view_m ) << std::endl;
      break;
    }
      
    case MouseAction::NOTHING:
      //std::cout << "  nothing" << std::endl;
    default:
      break;
    }

    _scaled_view_m = glm::scale( _current_view_m, 
                                 glm::vec3( _zoom, _zoom, _zoom ));
    
    //std::cout << "MOV " << str_dump() << std::endl;
  }
  // ********************************************** Trackball::on_mouse_scroll
  virtual void on_mouse_scroll( double xoffset, double yoffset )
  {
    /** yoffset is +/- 1 */

    if( yoffset < 0 ) {
      _zoom *= ZOOM_COEF;
      if( _zoom > ZOOM_MAX ) _zoom = ZOOM_MAX;
    }
    else {
      _zoom /= ZOOM_COEF;
      if( _zoom < ZOOM_MIN ) _zoom = ZOOM_MIN;
    }

    _scaled_view_m = glm::scale( _view_m, 
                                 glm::vec3( _zoom, _zoom, _zoom ));
    
    //std::cout << "SCROLL " << str_dump() << std::endl;
  }
private:
  // ********************************************* Trackball::compute_rotation
  /** Ok, simulate a track-ball.  Project the points onto the virtual
   * trackball, then figure out the axis of rotation, which is the cross
   * product of P1 P2 and O P1 (O is the center of the ball, 0,0,0)
   * Note:  This is a deformed trackball-- is a trackball in the center,
   * but is deformed into a hyperbolic sheet of rotation away from the
   * center.  This particular function was chosen after trying out
   * several variations.
   *
   * It is assumed that the arguments to this routine are in the range
   * (-1.0 ... 1.0)
   */
  glm::quat compute_rotation( float p1x, float p1y, float p2x, float p2y)
  {
    // Initialization is important : with 3 Euler Angles
    Quaternion q( {0.f, 0.f, 0.f} );
    // // zero rotation ?
    if ((abs(p1x - p2x) < EPSILON) and (abs(p1y - p2y) < EPSILON)) {
      return q;
    }
    /*
     * First, figure out z-coordinates for projection of P1 and P2 to
     * deformed sphere
     */
    glm::vec3 p1( p1x, p1y, project_to_sphere( TRACKBALL_SIZE, p1x, p1y) );
    glm::vec3 p2( p2x, p2y, project_to_sphere( TRACKBALL_SIZE, p2x, p2y) );
    /*
     *  Now, we want the cross product of P1 and P2
     */
    //auto a = cross( p2-po, p1-po );
    auto a = cross( p2, p1 );
    /*
     *  Figure out how much to rotate around that axis.
     */
    auto d = p1 - p2;
    auto t = length(d) / (2.0 * TRACKBALL_SIZE);

    /*
     * Avoid problems with out-of-control values...
     */
    if (t > 1.0) t = 1.0;
    if (t < -1.0) t = -1.0;
    float phi = 2.0 * asin(t);

    // std::cout << "=> rotation of " << phi * 180.0 / (M_PI*2.) << " around " << glm::to_string(a) << std::endl;
    
    q = rotate( q, phi, a );
    return q;
  } 
  /** Project an x,y pair onto a sphere of radius r OR a hyperbolic sheet
   * if we are away from the center of the sphere.
   */
  float project_to_sphere(float r, float x, float y)
  {
    float d, t, z;
    
    d = sqrt(x*x + y*y);
    if (d < r * 0.70710678118654752440)
      {    /* Inside sphere */
        z = sqrt(r*r - d*d);
      }
    else
      {           /* On hyperbola */
        t = r / 1.41421356237309504880;
        z = t*t / d;
      }
    return z;
  }
public:
  // **************************************************** TrackBall::attributs
  const Transform& get_transform() const
  {
    return _scaled_view_m;
  }
  void set_transform( Transform& view )
  {
    // extract scale (and others)
    glm::vec3 scale;
    Quaternion orient;
    glm::vec3 translate;
    glm::vec3 skew;
    glm::vec4 perspective;

    glm::decompose( view, scale, orient, translate, skew, perspective );
    std::cout << "__View" << std::endl;
    std::cout << pretty_4x4(view ) << std::endl;
    std::cout << "  scale=" << glm::to_string( scale ) << std::endl;
    std::cout << "  orient=" << glm::to_string( orient ) << std::endl;
    std::cout << "  trans=" << glm::to_string( translate ) << std::endl;
    std::cout << "  skew=" << glm::to_string( skew ) << std::endl;
    std::cout << "  pers=" << glm::to_string( perspective ) << std::endl;

    // check scale is "uniform" : cross product with (1,1,1)
    auto res = glm::cross( scale, glm::vec3( 1.f, 1.f, 1.f ));
    if (glm::length2( res ) < EPSILON * EPSILON) {
      _zoom = glm::length( scale );
      if (_zoom <= ZOOM_MAX && _zoom >= ZOOM_MIN) {
        _scaled_view_m = view;
        _view_m = glm::scale( _scaled_view_m, 
                                 glm::vec3( 1.f/_zoom, 1.f/_zoom, 1.f/_zoom ));
        _current_view_m = _view_m;
        return;
      }
    }
    std::cout << "__ERROR TrackBall cannot set_transform" << std::endl;
    std::cout << "  View" << std::endl;
    std::cout << pretty_4x4(view ) << std::endl;
    std::cout << "  scale=" << glm::to_string( scale ) << std::endl;
    std::cout << "  orient=" << glm::to_string( orient ) << std::endl;
    std::cout << "  trans=" << glm::to_string( translate ) << std::endl;
    std::cout << "  skew=" << glm::to_string( skew ) << std::endl;
    std::cout << "  pers=" << glm::to_string( perspective ) << std::endl;
  }
private:
  MouseAction _action;
  ScreenPos _start;
  float _zoom;
private:
  Transform _view_m;
  Transform _current_view_m;
  Transform _scaled_view_m;
  Transform _identity;
}; // TrackBall
}; // namespace visugl
#endif // TRACKBALL_HPP


