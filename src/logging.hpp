#ifndef LOGGING_HPP
#define LOGGING_HPP

// #define LOG_BBOX
// #define LOG_TITLE

// ******************************************************************* Loggers
#ifdef LOG_BBOX
#  define LOGBBOX(msg) (std::cout << msg << std::endl)
#else
#  define LOGBBOX(msg)
#endif

#ifdef LOG_TITLE
#  define LOGTITLE(msg) (std::cout << msg << std::endl)
#else
#  define LOGTITLE(msg)
#endif

#endif // LOGGING_HPP
