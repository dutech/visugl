/* -*- coding: utf-8 -*- */

#ifndef VISUGL_HPP
#define VISUGL_HPP

/** 
 * Global header for VisuGL
 *
 * declare Color, Graphictext, BoundingBox, Plotter, Window, Figure.
 */

#include "grafik.hpp"
#include "logging.hpp"
#include "base_types.hpp"
#include "bounding_box.hpp"

// ******************************************************************* Plotter
#include "plotter.hpp"


// ************************************************************ Window, Figure
#include "window.hpp"
#include "figure.hpp"

// ***************************************************************************
#include "shape2D.hpp"

#endif // VISUGL_HPP
