/* -*- coding: utf-8 -*- */

#ifndef GL_UTILS_HPP
#define GL_UTILS_HPP

/** 
 * Utilities for OpenGL
 * - save screen as PNG
 * - check for opengl errors
 */
#include <iostream>              // std::cout
#include <png++/png.hpp>

#include <GL/gl.h>               // OpenGL

#include <memory>                // std::unique_ptr, std::shared_ptr
#include <limits>                // std::numeric_limits

namespace utils {
namespace gl {
// *********************************************************** libpng++ to_png
/** WARNING suppose the OpenGL context and window are set
 */
void to_png( const std::string& filename,
             int width, int height,
             int xoffset=0, int yoffset=0 ) 
{
  std::cout << "__ SAVE to_png in " << filename << std::endl;
  std::cout << "  w=" << width << " h=" << height << std::endl;
  GLenum err;
  while ((err = glGetError()) != GL_NO_ERROR) {
    std::cerr << "OpenGL error: " << err << std::endl;
  }

  // new image
  png::image<png::rgb_pixel> image( width-xoffset, height-yoffset );

  // get image pixels
  // new buffer, access raw with .get()
  std::unique_ptr<GLuint[]> pixels( new GLuint [3 * (width-xoffset) * (height-yoffset)]);
  // check OpenGL error
  std::cout << "__ READ buffer start" << std::endl;
  while ((err = glGetError()) != GL_NO_ERROR) {
    std::cerr << "OpenGL error: " << err << std::endl;
  }
  
  glReadPixels( xoffset, yoffset, width, height,
		GL_RGB, GL_UNSIGNED_INT, pixels.get() );
  std::cout << "__ READ buffer end" << std::endl;
  while ((err = glGetError()) != GL_NO_ERROR) {
    std::cerr << "OpenGL error: " << err << std::endl;
  }

  // copy image
  std::cout << "__ SET image start" << std::endl;
  for( auto y = 0; y < height-yoffset; ++y ) {
    for( auto x = 0 ; x < width-xoffset; ++x ) {
      image.set_pixel( x, (height-yoffset-1)-y,
                       png::rgb_pixel( pixels[3*x + y*3*width + 0],
                                       pixels[3*x + y*3*width + 1],
                                       pixels[3*x + y*3*width + 2] ));
    }
  }
  std::cout << "__ SET image end" << std::endl;
  // save image
  std::cout << "__ SAVE to "<< filename << std::endl;
  image.write( filename );
}

// *************************************************************** check_error
void _check_error(const char *file, int line);
 ///
/// Usage
/// [... some opengl calls]
/// utils::gl::check_error();
///
#define check_error() _check_error(__FILE__,__LINE__)

void _check_error(const char *file, int line) {
  GLenum err (glGetError());
 
  while(err!=GL_NO_ERROR) {
    std::string error;
 
    switch(err) {
    case GL_INVALID_OPERATION:      error="INVALID_OPERATION";      break;
    case GL_INVALID_ENUM:           error="INVALID_ENUM";           break;
    case GL_INVALID_VALUE:          error="INVALID_VALUE";          break;
    case GL_OUT_OF_MEMORY:          error="OUT_OF_MEMORY";          break;
    case GL_INVALID_FRAMEBUFFER_OPERATION:  error="INVALID_FRAMEBUFFER_OPERATION";  break;
    }
    
    std::cerr << "GL_" << error.c_str() <<" - "<<file<<":"<<line<<std::endl;
    err=glGetError();
  }
}  
  
}; // namespace gl
}; // namespace utils


#endif // GL_UTILS_HPP
