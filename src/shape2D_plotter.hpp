/* -*- coding: utf-8 -*- */

#ifndef SHAPE2D_PLOTTER_HPP
#define SHAPE2D_PLOTTER_HPP

/**
 * TODO
 */

#include "visugl.hpp"
#include "shape2D.hpp"

namespace visugl {
// ***************************************************************************
// ************************************************************ Shape2DPlotter
// ***************************************************************************
class Shape2DPlotter : public Plotter
{
public:
  using Shape2DPtr = Shape2D*;
  using Shape2DList = std::list<Shape2DPtr>;

  Shape2DPlotter() : Plotter() {}

  // ******************************************** Shape2DPlotter::add_plotters
public:
  /* Cannot add self */
  virtual bool add_shape( const Shape2DPtr shape )
  {
    _shapes.push_back( shape );
    return true;
  }
  virtual void remove_shape( const Shape2DPtr shape )
  {
    _shapes.remove( shape );
  }
  /** Usually called by children to notify of something */
  // TODO name already taken by Plotter
  // virtual void children_changed( std::string msg )
  // {
  //   std::cout << "__Plotter::children_changed: TO IMPLEMENT" << std::endl;
  // }
  virtual void render( float screen_ratio_x = 1.0, float screen_ratio_y = 1.0 )
  {
    for( const auto& s : _shapes ) {
      begin_transform2D( s->_origin, s->_orient ); {
        s->render( screen_ratio_x, screen_ratio_y  );
        utils::gl::check_error();
      }
      end_transform2D();
    }
  }

private:
  Shape2DList _shapes;
}; // class Shape2DPlotter
}; // namespace visugl

#endif // SHAPE2D_PLOTTER_HPP
