/* -*- coding: utf-8 -*- */

#ifndef LINE_PLOTTER_HPP
#define LINE_PLOTTER_HPP

#include "visugl.hpp"
#include "data.hpp"

/**
 * LinePlotter draw a continuous line (using GL_LINE_STRIP) or segments (using
 * GL_LINES) from Data.
 * - set_contiunous(bool)
 */
namespace visugl {
// ***************************************************************************
// *************************************************************** LinePlotter
// ***************************************************************************
class LinePlotter : public Plotter
{
public:
  public :
  // *************************************************** LinePlotter::creation
  LinePlotter(Data *data = nullptr) :
    Plotter(),
    _data(data),
    _fg_continuous(true),
    _fg_col( {0.f, 0.f, 0.f } ), _width( 0.1f )
  {
    LOGBBOX( "__CREATE LineP " << _name << " BB=" << get_bbox() );
    _data->add_plotter( this );
    update_bbox();
    _last_sample = _data->get_samples().begin();
    LOGBBOX( "  CREATE LineP " << _name << " BB=" << get_bbox() );
  }
  virtual ~LinePlotter()
  {
    _data->remove_plotter( this );
  }
  // ******************************************* LinePlotter::children_changed
  virtual void children_changed( std::string msg )
  {
    if( msg == "cleared" ) {
      // std::cout << "__LinePlotter::children_changed cleared" << std::endl;
      update_bbox();
      _last_sample = _data->get_samples().begin();
      // TODO redraw in static ?
      // => done "by hand" in test/013-taticplotters.cpp
    }
  }
  // ***************************************************** LinePlotter::render
  virtual void render( float screen_ratio_x = 1.0, float screen_ratio_y = 1.0 )
  {
    if (_debug_fg) std::cout << "__RENDER " << _name << std::endl;

    // Color
    glColor4d( _fg_col.r, _fg_col.g, _fg_col.b, 1.0);

    // -------------------------------------------------------------------------
    //  Rendering using GL_LINE_STRIP or GL_LINES
    // -------------------------------------------------------------------------
    glEnable (GL_BLEND);
    glEnable (GL_LINE_SMOOTH);
    glLineWidth( _width );

    if (_fg_continuous) {
      glBegin(GL_LINE_STRIP);
    }
    else {
      glBegin(GL_LINES);
    }
    for( auto& pt: _data->get_samples()) {
      glVertex3d( pt.x, pt.y, pt.z );
    }
    glEnd();

    
    // Mark _last_sample, used by render_last
    _last_sample = _data->get_samples().end();
    _last_sample--;
    
    if (_debug_fg) std::cout << "__end render " << _name << std::endl;
  }
  virtual void render_last( float screen_ratio_x = 1.0,
                            float screen_ratio_y = 1.0 )
  {
    if (_debug_fg) std::cout << "__RENDER_LAST " << _name << std::endl;
    
    // Color
    glColor4d( _fg_col.r, _fg_col.g, _fg_col.b, 1.0);
    // -------------------------------------------------------------------------
    //  Rendering using GL_LINE_STRIP or GL_LINES
    // -------------------------------------------------------------------------
    glEnable (GL_BLEND);
    glEnable (GL_LINE_SMOOTH);
    glLineWidth( _width );

    if (_fg_continuous) {
      glBegin(GL_LINE_STRIP);
    }
    else {
      glBegin(GL_LINES);
    }
    for( ; _last_sample != _data->get_samples().end(); ++_last_sample ) {
      // std::cout << "  pt= " << its->x << ", " << its->y << ", " << its->z<<std::endl;
      glVertex3d( _last_sample->x, _last_sample->y, _last_sample->z );
    }
    glEnd();
    _last_sample--;

    if (_debug_fg) std::cout << "__end render_last " << _name << std::endl;
  }

  // ************************************************ LinePlotter::update_bbox
  virtual void update_bbox()
  {
    LOGBBOX( "__UPD " << _name << " start BB=" << get_bbox() );
    _bbox.set( _data->get_bbox() );
    LOGBBOX( "  UPD " << _name << " end BB= " << get_bbox() );
  }

  // ************************************************** LinePlotter::attributs
  void set_color( const Color& col )
  {
    _fg_col = col;
  }
  void set_width( const GLfloat& width )
  {
    _width = width;
  }
  void set_continuous( bool continuous )
  {
    _fg_continuous = continuous;
  }
  bool get_continuous() { return _fg_continuous; }
private:
  Data* _data;
  bool _fg_continuous;
  Color _fg_col;
  GLfloat _width;
  /** Iterator to the last sample plotted */
  std::list<Sample>::iterator _last_sample;
  
}; // class LinePlotter
}; // namespace visugl

#endif // LINE_PLOTTER_HPP
