/* -*- coding: utf-8 -*- */

#ifndef ARROW_PLOTTER_HPP
#define ARROW_PLOTTER_HPP


#include "visugl.hpp"
#include "data.hpp"
#include <algorithm> // std::max

#define GLM_FORCE_RADIANS
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/string_cast.hpp>

namespace visugl {
// ***************************************************************************
// ************************************************************** ArrowPlotter
// ***************************************************************************
class ArrowPlotter : public Plotter
{
  glm::vec3 vec_Ox = glm::vec3( 1.0, 0.0, 0.0 );
public:
  // ***************************************************ArrowPlotter::creation
  ArrowPlotter( Data *pos = nullptr, Data *vec = nullptr) :
    Plotter(),
    _pos(pos), _vec(vec), _scale(1.0),
    _head_length(0.2), _head_width(0.1/2.0), _head_ratio(0.1)
  {
    //TODO
  }
  virtual ~ArrowPlotter()
  {
  }

  // **************************************************** ArrowPlotter::render
  virtual void render( float screen_ratio_x = 1.0, float screen_ratio_y = 1.0 )
  {
    if (_debug_fg) std::cout << "__RENDER " << _name << std::endl;

    // Color
    glColor4d( _fg_col.r, _fg_col.g, _fg_col.b, 1.0);

    // ----------------------------------------------------------------------
    // -- First, Arrow
    // ----------------------------------------------------------------------
    auto itp = _pos->get_samples().begin();
    auto itv = _vec->get_samples().begin();
    for( ;
         itp != _pos->get_samples().end() &&
           itv != _vec->get_samples().end();
         itp++, itv++ ) {

      auto& pos = *itp;
      auto& vec = *itv;
      draw_head( pos, vec );
      glBegin(GL_LINES);
      glVertex3d( pos.x, pos.y, pos.z );
      glVertex3d( pos.x+_scale*vec.x, pos.y+_scale*vec.y, pos.z+_scale*vec.z );
      glEnd();
    }

    if (_debug_fg) std::cout << "__end render " << _name << std::endl;
  }
  // ************************************************* ArrowPlotter::draw_head
  void draw_head( Sample& pos, Sample& vec)
  {
    // need to a normalized direction vector to compute rotation
    glm::vec3 dir( vec.x, vec.y, vec.z );
    dir = glm::normalize( dir );
    glm::quat rotation_q = glm::rotation( vec_Ox, dir );
    glm::mat4 rotation_mat = glm::toMat4( rotation_q );

    glm::mat4 identity_m( 1.0 ); // identity
    glm::mat4 translate_m = glm::translate( identity_m,
                                glm::vec3( pos.x+_scale*vec.x,
                                           pos.y+_scale*vec.y,
                                           pos.z+_scale*vec.z ));

    glm::mat4 transform_m = translate_m * rotation_mat;
    
    GLdouble head_length = _head_ratio * sqrt( vec.x*vec.x + vec.y*vec.y + vec.z*vec.z );
    GLdouble head_width = _head_width * (head_length / _head_length); 
    head_length = _scale * std::min( head_length, _head_length );
    head_width = _scale * std::min( head_width, _head_width );
    
    glPushMatrix(); {
      glMultMatrixf(  glm::value_ptr( transform_m ));
      //glScalef( 1.f, 1.f, -1.f );
      
      glBegin( GL_TRIANGLE_FAN ); {
        glVertex3d( 0.0, 0.0, 0.0 );                             // 0 at center
        glVertex3d( -head_length, head_width, head_width );   // 1 at Y,Z
        glVertex3d( -head_length, -head_width, head_width );  // 2 at -Y,Z

        glVertex3d( -head_length, -head_width, -head_width ); // 3 at -Y,-Z

        glVertex3d( -head_length, head_width, -head_width );  // 4 at Y,-Z

        glVertex3d( -head_length, head_width, head_width );   // 5 at Y,Z
      }
      glEnd();
    }
    glPopMatrix();
  }
  // ************************************************* ArrowPlotter::attributs
  void set_color( const Color& col )
  {
    _fg_col = col;
  }
  void set_head_length( const GLdouble& head_length )
  {
    _head_length = head_length;
  }
  void set_head_width( const GLdouble& head_width )
  {
    _head_width = head_width;
  }
  void set_head_ratio( const GLdouble& head_ratio )
  {
    _head_ratio = head_ratio;
  }
  void set_scale( const GLdouble& scale )
  {
    if (scale > 0.0) {
      _scale = scale;
    }
  }
  GLdouble get_scale()
  {
    return _scale;
  }
private:
  Data* _pos;
  Data* _vec;
  GLdouble _scale;
  Color _fg_col;
  /** size of arrow head */
  GLdouble _head_length;
  GLdouble _head_width;
  GLdouble _head_ratio;
  
}; // class ArrowPlotter
}; // namespace visugl


#endif // ARROW_PLOTTER_HPP
