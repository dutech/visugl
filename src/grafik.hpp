#ifndef GRAFIK_HPP
#define GRAFIK_HPP

#include <iostream>
#include <memory>                // std::unique_ptr, std::shared_ptr
#include <png++/png.hpp>

// include OpenGL > 1.1
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#ifdef USE_ANTTWEAKBAR
#include <AntTweakBar.h>
#endif

// Fonts
#include <FTGL/ftgl.h>               // Fonts in OpenGL
#define FONT_PATH "ressources/Consolas.ttf"
#define FONT_SIZE 12
// Default scale for fonts : screen width=800, axe from -1 to 1.
#define FONT_SCALE ((1.0 - -1.0) / 800.0)

//#include "gl_utils.hpp"              // utils::gl::to_png, error
#include <GL/gl.h>

namespace visugl {
namespace grafik {
// ***************************************************************************
// ********************************************************************** GLFW
// ***************************************************************************
static void error_callback(int error, const char* description)
{
  std::cerr <<  description << std::endl;
  //fputs(description, stderr);
}

static bool _end_render = false;
inline void init() {
  std::cout << "__GLFW Init" << std::endl;

  glfwSetErrorCallback(error_callback);

  if (!glfwInit())
        exit(EXIT_FAILURE);
}
inline void end() {
  glfwTerminate();
  std::cout << "__GLFW destroyed" << std::endl;
}
inline bool should_stop() {
  // TODO when several windows, which one should close. See AgentIDSM
  return (_end_render); // and !glfwWindowShouldClose(_win->_window));
}

// ***************************************************************************
// ********************************************************************** FONT
// ***************************************************************************
/**
   "size" for fonts (for example FONT_SIZE) are given in pt or pixels.
   It is also the case when asked for the bouding box of a text returned by:
   '_font->BBox( "some important text");'

   So, when setting the pos for rendering a text, these 'size'/'box' info must
   be multiplied by (1./nb_pixels_in_1_unit_of_current_window/fig/screen_frame).

   I.e: a text of width 90 px in a frame where the window_width of 640 corresponds
   to the range (-2,2) has a local size of 90 * (2 - (-2)) / 640 = 0.562.

   @see some usage in 'figure.hpp'
   */
static FTGLTextureFont* _font = nullptr;
FTGLTextureFont* font() {
  if (_font) {
    return _font;
  }

  /** Init Fonts */
  _font = new FTGLTextureFont( FONT_PATH );
  if (! _font) {
    std::cerr << "ERROR: Unable to create Font" << std::endl;
  }
  else if (_font->Error()) {
    std::cerr << "ERROR: Unable to open file " << FONT_PATH << std::endl;
  }
  else {
    if (! _font->FaceSize(FONT_SIZE)) {
      std::cerr << "ERROR: Unable to set font face size " << FONT_SIZE << std::endl;
    }
  }
  return _font;
}

// ***************************************************************************
// ************************************************************* OpenGL Errors
// ***************************************************************************
void _check_error(const char *file, int line);
///
/// Usage
/// [... some opengl calls]
/// visugl::grafik::check_error();
///
#define check_error() _check_error(__FILE__,__LINE__)

inline void _check_error(const char *file, int line) {
  GLenum err (glGetError());

  while(err!=GL_NO_ERROR) {
    std::string error;

    switch(err) {
    case GL_INVALID_OPERATION:      error="INVALID_OPERATION";      break;
    case GL_INVALID_ENUM:           error="INVALID_ENUM";           break;
    case GL_INVALID_VALUE:          error="INVALID_VALUE";          break;
    case GL_OUT_OF_MEMORY:          error="OUT_OF_MEMORY";          break;
    case GL_INVALID_FRAMEBUFFER_OPERATION:  error="INVALID_FRAMEBUFFER_OPERATION";  break;
    }

    std::cerr << "GL_" << error.c_str() <<" - "<<file<<":"<<line<<std::endl;
    err=glGetError();
  }
}

// ***************************************************************************
// ******************************************************************** TO_PNG
// ***************************************************************************
// *********************************************************** libpng++ to_png
/** WARNING suppose the OpenGL context and window are set
 */
inline void to_png( const std::string& filename,
             int width, int height,
             int xoffset=0, int yoffset=0 )
{
  std::cout << "__ SAVE to_png in " << filename << std::endl;
  std::cout << "  w=" << width << " h=" << height << std::endl;

  // new image
  png::image<png::rgb_pixel> image( width-xoffset, height-yoffset );

  // get image pixels
  // new buffer, access raw with .get()
  std::unique_ptr<GLuint[]> pixels( new GLuint [3 * (width-xoffset) * (height-yoffset)]);

  std::cout << "__ READ buffer start" << std::endl;
  glReadPixels( xoffset, yoffset, width, height,
		GL_RGB, GL_UNSIGNED_INT, pixels.get() );
  check_error();
  std::cout << "__ READ buffer end" << std::endl;

  // copy image
  std::cout << "__ SET image start" << std::endl;
  for( auto y = 0; y < height-yoffset; ++y ) {
    for( auto x = 0 ; x < width-xoffset; ++x ) {
      image.set_pixel( x, (height-yoffset-1)-y,
                       png::rgb_pixel( pixels[3*x + y*3*width + 0],
                                       pixels[3*x + y*3*width + 1],
                                       pixels[3*x + y*3*width + 2] ));
    }
  }
  std::cout << "__ SET image end" << std::endl;
  // save image
  std::cout << "__ SAVE to "<< filename << std::endl;
  image.write( filename );
}

}; // namespace grafik
}; // namespace visugl

#endif // GRAFIK_HPP
