/* -*- coding: utf-8 -*- */

#ifndef FIELD_PLOTTER_HPP
#define FIELD_PLOTTER_HPP

#include "visugl.hpp"
#include "data.hpp"
#include <mark_plotter.hpp>

/** 
 * TODO
 *
 * Does NOT make explicit use of Plotter::add_plotter() but, instead,
 * call _mark_p->render() in render().
 */

namespace visugl {
// ***************************************************************************
// ************************************************************** FieldPlotter
// ***************************************************************************
class FieldPlotter : public Plotter {
public:
  FieldPlotter( Data *pos_data=nullptr, Data *vec_data=nullptr,
                double scale_factor=1.0 ) :
    Plotter(),
    _p_data(pos_data), _v_data(vec_data),
    _fg_col( {0.f, 0.f, 0.f } ), _width( 0.1f ),
    _scale(scale_factor)    
  {
    LOGBBOX( "__CREATE FieldP " << _name << " BB=" << get_bbox() );
    // transform v_data to points (extremity of vectors)
    update_bbox();

    _mark_p = new MarkPlotter( _p_data );
    _mark_p->set_style( MarkPlotter::MarkerStyle::lozange );
    
    LOGBBOX( "  CREATE FieldP " << _name << " BB=" << get_bbox() );
  }
  virtual ~FieldPlotter()
  {
    delete _mark_p;
  }
  // ****************************************************** FieldPlotter::name
  virtual void set_name( const std::string &name )
  {
    LOGBBOX( "__RENAME Plotter " << _name << " => " << name );
    _name = name;
    _mark_p->set_name( _name+"_MARKP" );
  }
  // ***************************************************** FieldPlotter::debug
  virtual void set_debug( bool debug_fg )
  {
    _debug_fg = debug_fg;
    _mark_p->set_debug( debug_fg );
  }
  // **************************************************** FieldPlotter::render
  virtual void render( float screen_ratio_x = 1.0, float screen_ratio_y = 1.0 )
  {
    if (_debug_fg) std::cout << "__RENDER " << _name << std::endl;

    // Render Marker
    _mark_p->render( screen_ratio_x, screen_ratio_y );
    
    // Color
    glColor4d( _fg_col.r, _fg_col.g, _fg_col.b, 1.0);

    // -------------------------------------------------------------------------
    //  Rendering using GL_LINE_STRIP
    // -------------------------------------------------------------------------
    glEnable (GL_BLEND);
    glEnable (GL_LINE_SMOOTH);
    glLineWidth( _width );

    glBegin(GL_LINES);
    auto itp = _p_data->get_samples().begin();
    auto itv = _v_data->get_samples().begin();
    for( ; itp != _p_data->get_samples().end() ;
         itp++, itv++ ) {
      glVertex3d( (*itp).x, (*itp).y, (*itp).z );
      glVertex3d( (*itp).x + (*itv).x * _scale, (*itp).y + (*itv).y * _scale, (*itp).z + (*itv).z * _scale ); 
    }
    glEnd();

    // TODO Mark _last_sample

    if (_debug_fg) std::cout << "__end render " << _name << std::endl;
  }

  // *********************************************** FieldPlotter::update_bbox
  virtual void update_bbox()
  {
    LOGBBOX( "__UPD  " << _name << " BB=" << get_bbox() );
    // at least, the positions
    auto pbox = _p_data->get_bbox();
    // compue a margin, using the bbox of v_data as insight
    auto bbvec = _v_data->get_bbox();
    double xmargin = std::max( bbvec.x_max, -bbvec.x_min ) * _scale;
    double ymargin = std::max( bbvec.y_max, -bbvec.y_min ) * _scale;
    // and thus update the bbox
    _bbox.set( pbox.x_min - xmargin, pbox.x_max + xmargin,
               pbox.y_min - ymargin, pbox.y_max + ymargin );
    LOGBBOX( "  UPD  " << _name << " BB=" << get_bbox() );
  }

  // ************************************************** LinePlotter::attributs
  /** sets color of _mark_p and lines */
  void set_color( const Color& col )
  {
    _fg_col = col;
    _mark_p->set_color( col );
  }
  void set_width( const GLfloat& width )
  {
    _width = width;
  }
  /** Sets scaling factor of inflence vector */
  void set_scale( const double scale )
  {
    _scale = scale;
  }
  /** sets size of _mark_p */
  void set_size( const GLfloat& size )
  {
    _mark_p->set_size( size );
  }
  // TODO overwrite setName, setDebug
private:
  Data* _p_data;
  Data* _v_data;
  Color _fg_col;
  GLfloat _width;
  double _scale;

  // to plot points
  MarkPlotter* _mark_p;
}; // FieldPlotter
}; // namespace visugl

#endif // FIELD_PLOTTER_HPP
