/* -*- coding: utf-8 -*- */

#ifndef DATA_HPP
#define DATA_HPP

/** 
 * Data is a list of Sample that maintain a BoundingBox.
 * Cannot be copied.
 */

#include <list>

#include <visugl.hpp>
#include <plotter.hpp>    // mainly for bbox TODO Plotter and Plotable

namespace visugl {
class Data : public Plotter
{
public:
  // ********************************************************** Data::creation
  Data() : Plotter()
  {
    clear();
    LOGBBOX( "__CREATE Data " << _name << " BB=" << get_bbox() );
  }
  Data( const Data& d)
    : Plotter( d._bbox.x_min, d._bbox.x_max, d._bbox.y_min, d._bbox.y_max ),
      _samples(d._samples)
  {
    std::cout << "Data copy CONSTRUCTOR not implemented (copy vs DeepCopy)" << std::endl;
    exit(1);
  }
  Data& operator=(const Data& d) // copy 
  {
    std::cout << "Data copy= OPERATOR not implemented (copy vs DeepCopy)" << std::endl;
    exit(1);
    
    // if(this != &d) { // protect against invalid self-assignment
    //   _samples = d._samples;
    //   set_bbox( d._bbox );
    // }
    
    // return *this;
  }
  
  // ************************************************************* Data::clear
  void clear()
  {
    _samples.clear();
    _bbox.set_empty(); //( {0.0, 1.0, 0.0, 1.0} );

    // // TODO : notify Listeners => i.e. Plotters
    // for( auto& ptr: _plotters) {
    //   ptr->children_changed( "cleared" );
    // }
  }
  // *************************************************************** Data::add
  /** Add Sample and adjust _bbox */
  virtual void add( const Sample& sample)
  {
    //std::cout << "Curve::add_data" << std::endl;
    _samples.push_back( sample );
    _bbox.update( sample );
  }
  
  // ********************************************************* Data::attributs
  std::list<Sample>& get_samples() { return _samples; }
protected:
  /** Data are a list of Samples*/
  std::list<Sample> _samples;

}; // Data
}; // namespace visugl

#endif // DATA_HPP
