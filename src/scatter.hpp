/* -*- coding: utf-8 -*- */

#ifndef SCATTER_HPP
#define SCATTER_HPP

/** 
 * A special Curve renderer that display a Curve as a scatter plot.
 *
 * WARNING : Temporary class, as it needs a Curve and uses its own
 *           curve while being a Curve !!! Ambiguous !!!!!!!!!!!!!
 * TODO: Need to be changed, but a whole lot in VisuGL would change. 
 */

#include <curve.hpp>

namespace visugl {
// ***************************************************************************
// ************************************************************ ScatterPlotter
// ***************************************************************************
class ScatterPlotter : public Curve 
{
public:
  // ********************************************* ScatterPlotter::constructor
  ScatterPlotter( Curve& curve) :
    Curve(), 
    _curve(curve), _active(true), _size(0.15f),
    _ref_data( curve.get_samples() )
  {
  }
  // ***************************************************** ScatterPlotter::set
  void set_size( const GLfloat& size )
  {
    _size = size;
  }
  void set_active( const bool& active )
  {
    _active = active;
  }
  bool is_active()
  {
    return _active;
  }
  // ************************************************** ScatterPlotter::render
  /** Draw diamonds with classic OpenGL */
  virtual void render( float screen_ratio_x = 1.0, float screen_ratio_y = 1.0 )
  {
    if (_debug_fg) std::cout << "__RENDER " << _name << std::endl;
    if (not _active ) return;
                     
    // Color
    glColor4d( _fg_col.r, _fg_col.g, _fg_col.b, 1.0);
    // -------------------------------------------------------------------------
    //  Rendering using GL_LINE_STRIP
    // -------------------------------------------------------------------------
    glEnable (GL_BLEND);
    glEnable (GL_LINE_SMOOTH);
    glLineWidth( _line_width );

    glBegin(GL_LINES);
    for( auto& pt: _ref_data ) {
      //std::cout << "DRAW (" <<  pt.x << ", " << pt.y << ")" << std::endl;
      glVertex3d( pt.x + _size, pt.y, pt.z );
      glVertex3d( pt.x, pt.y + _size, pt.z );

      glVertex3d( pt.x, pt.y + _size, pt.z );
      glVertex3d( pt.x - _size, pt.y, pt.z );

      glVertex3d( pt.x - _size, pt.y, pt.z );
      glVertex3d( pt.x, pt.y - _size, pt.z );

      glVertex3d( pt.x, pt.y - _size, pt.z );
      glVertex3d( pt.x + _size, pt.y, pt.z );
    }
    glEnd();

    // Mark _last_sample, used by render_last
    if (_ref_data.empty() ) {
        _last_sample = _ref_data.begin();
      }
    else {
      _last_sample = _ref_data.end();
      _last_sample--;
    }
    
    if (_debug_fg) std::cout << "__end render " << _name << std::endl;
  }
  virtual void render_last( float screen_ratio_x = 1.0,
                            float screen_ratio_y = 1.0 )
  {
    if (_debug_fg) std::cout << "__RENDER_LAST " << _name << std::endl;
    
    // std::cout << "__ScatterPlotter::render_last: NOT IMPLEMENTED" << std::endl;
    // exit(1);

    // Color
    glColor4d( _fg_col.r, _fg_col.g, _fg_col.b, 1.0);
    // -------------------------------------------------------------------------
    //  Rendering using GL_LINE_STRIP
    // -------------------------------------------------------------------------
    glEnable (GL_BLEND);
    glEnable (GL_LINE_SMOOTH);
    glLineWidth( _line_width );

    glBegin(GL_LINES);
    for( auto its = _last_sample; its != _ref_data.end(); ++its) {
      auto pt = *its;
      //std::cout << "DRAW (" <<  pt.x << ", " << pt.y << ")" << std::endl;
      //std::cout << "      " <<  std::distance(its, data.begin()) << std::endl;
      glVertex3d( pt.x + _size, pt.y, pt.z );
      glVertex3d( pt.x, pt.y + _size, pt.z );

      glVertex3d( pt.x, pt.y + _size, pt.z );
      glVertex3d( pt.x - _size, pt.y, pt.z );

      glVertex3d( pt.x - _size, pt.y, pt.z );
      glVertex3d( pt.x, pt.y - _size, pt.z );

      glVertex3d( pt.x, pt.y - _size, pt.z );
      glVertex3d( pt.x + _size, pt.y, pt.z );
    }
    glEnd();
    _last_sample--;

    if (_debug_fg) std::cout << "__end render_last " << _name << std::endl;
  }
  // ************************************************************ bounding_box
  virtual const BoundingBox& get_bbox() const
  {
    return _curve.get_bbox();
  }
protected:
  // *********************************************** ScatterPlotter::attributs
  Curve& _curve;
  bool _active;
  GLfloat _size;
  std::list<Sample>& _ref_data;
};
}; // namespace visugl

  
#endif // SCATTER_HPP
