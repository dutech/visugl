/* -*- coding: utf-8 -*- */

#ifndef MARK_PLOTTER_HPP
#define MARK_PLOTTER_HPP

#include "visugl.hpp"
#include "data.hpp"

namespace visugl {
// ***************************************************************************
// *************************************************************** MarkPlotter
// ***************************************************************************
class MarkPlotter : public Plotter
{
public:
  // ************************************************************* MarkerStyle
  enum class MarkerStyle {
    triangle,
    lozange
  };

  public :
  // *************************************************** MarkPlotter::creation
  MarkPlotter(Data *data = nullptr) :
    Plotter(),
    _data(data), _style(MarkerStyle::triangle),
    _fg_col( {0.f, 0.f, 0.f } ), _size( 0.05f )
  {
    _data->add_plotter( this );
    update_bbox();
    _last_sample = _data->get_samples().begin();
    LOGBBOX( "__CREATE MarkP " << _name << " BB=" << get_bbox() );
  }
  virtual ~MarkPlotter()
  {
    _data->remove_plotter( this );
  }
  // ******************************************* LinePlotter::children_changed
  virtual void children_changed( std::string msg )
  {
    if( msg == "cleared" ) {
      // std::cout << "__LinePlotter::children_changed cleared" << std::endl;
      update_bbox();
      _last_sample = _data->get_samples().begin();
      // TODO redraw in static ?
      // => done "by hand" in test/013-taticplotters.cpp
    }
  }
  // ***************************************************** MarkPlotter::render
  virtual void render( float screen_ratio_x = 1.0, float screen_ratio_y = 1.0 )
  {
    if (_debug_fg) std::cout << "__RENDER " << _name << std::endl;

    // Color
    glColor4d( _fg_col.r, _fg_col.g, _fg_col.b, 1.0);

    if( _style == MarkerStyle::lozange ) {
      // TODO : global policy for GLStates
      glEnable (GL_BLEND);
      glEnable (GL_LINE_SMOOTH);
      //glLineWidth( _size );

      glBegin(GL_LINES);
      for( auto& pt: _data->get_samples() ) {
        draw_lozange( pt.x, pt.y, pt.z );
      }
      glEnd();
      glDisable(GL_LINE_SMOOTH);
      glDisable(GL_BLEND);
    }
    else if( _style == MarkerStyle::triangle ) {
      glBegin( GL_TRIANGLES );
      for( auto& pt: _data->get_samples() ) {
        draw_triangle( pt.x, pt.y, pt.z );
      }
      glEnd();
    }

    // Mark _last_sample, used by render_last
    _last_sample = _data->get_samples().end();
    _last_sample--;
    if (_debug_fg) std::cout << "__end render " << _name << std::endl;
  }
  virtual void render_last( float screen_ratio_x = 1.0,
                            float screen_ratio_y = 1.0 )
  {
    if (_debug_fg) std::cout << "__RENDER_LAST " << _name << std::endl;
    
    // Color
    glColor4d( _fg_col.r, _fg_col.g, _fg_col.b, 1.0);

    if( _style == MarkerStyle::lozange ) {
      // TODO : global policy for GLStates
      glEnable (GL_BLEND);
      glEnable (GL_LINE_SMOOTH);
      //glLineWidth( _size );

      glBegin(GL_LINES);
      for( ; _last_sample != _data->get_samples().end(); ++_last_sample ) {
        draw_lozange( _last_sample->x, _last_sample->y, _last_sample->z );
      }
      glEnd();
      glDisable(GL_LINE_SMOOTH);
      glDisable(GL_BLEND);
    }
    else if( _style == MarkerStyle::triangle ) {
      glBegin( GL_TRIANGLES );
      for( ; _last_sample != _data->get_samples().end(); ++_last_sample ) {
        draw_triangle( _last_sample->x, _last_sample->y, _last_sample->z );
      }
      glEnd();
    }
    _last_sample--;

    if (_debug_fg) std::cout << "__end render_last " << _name << std::endl;
  }

  // ************************************************ MarkPlotter::update_bbox
  virtual void update_bbox()
  {
    LOGBBOX( "__UPD " << _name << " start BB=" << get_bbox() );
    _bbox.set( _data->get_bbox() );
    LOGBBOX( "  UPD " << _name << " end BB= " << get_bbox() );
  }

  // ************************************************** MarkPlotter::attributs
  void set_style( const MarkerStyle& style )
  {
    _style = style;
  }
  void set_color( const Color& col )
  {
    _fg_col = col;
  }
  void set_size( const GLfloat& size )
  {
    _size = size;
  }
private:
  Data* _data;
  MarkerStyle _style;
  Color _fg_col;
  GLfloat _size;
  /** Iterator to the last sample plotted */
  std::list<Sample>::iterator _last_sample;
  
private:
  // ******************************************************* MarkPlotter::draw
  /** NEED: GL_LINES */
  void draw_lozange( GLfloat x, GLfloat y, GLfloat z )
  {
    glVertex3d(x + _size, y, z);
    glVertex3d(x, y + _size, z);

    glVertex3d(x, y + _size, z);
    glVertex3d(x - _size, y, z);

    glVertex3d(x - _size, y, z);
    glVertex3d(x, y - _size, z);

    glVertex3d(x, y - _size, z);
    glVertex3d(x + _size, y, z);
  }
  /** NEED: GL_TRIANGLES */
  void draw_triangle(GLfloat x, GLfloat y, GLfloat z)
  {
    glVertex3d(x - 0.866 * _size, y - 0.5 * _size, z);
    glVertex3d(x + 0.866 * _size, y - 0.5 * _size, z);
    glVertex3d(x, y + _size, z);
  }
}; // MarkPlotter
}; // namespace visugl

#endif // MARK_PLOTTER_HPP
