/* -*- coding: utf-8 -*- */

/** 
 * 010-marker.cpp
 *
 * VisuGL example with data (Curves) and Plotters (Curve and MarkPlotter)
 * Window + Figure with title
 *   + _curve_main (as data)
 *   + _curve_view (as subset of _curve_main)
 *   + _liner  as LinePlotter of _vurve_main
 *   + _marker as MarkPlotter of _curve_view
 *
 * Options:
 * -s/static : use a WinStat as WinHolder (faster but linewidth is changed)
 * -b/bbox : force the BBox of the Figure to [-0.5,2.5] x [-1,1]
 * -u/update : update the BBox to the Data
 * -d/debug : grafik debug
 *
 * 'C' or 'c' -> switch continuous/segments
 * 'D' or 'd' -> debug grafik
 * 'S' or 's' -> save drawing as "010-marker.png"
 */
#include <iostream>
#include <string>         // std::string
#include <sstream>

// declare Window, Figure,
// #include <grafik.hpp>
#include <visugl.hpp>
// using namespace visugl; // or use visugl:: everywhere...
#include <line_plotter.hpp>
#include <mark_plotter.hpp>

/** Window, Figure and Plotters */
visugl::WinHolder* _win;
visugl::Figure* _fig;
visugl::Data*  _curve_main;
visugl::Data*  _curve_view;
visugl::LinePlotter* _liner;
visugl::MarkPlotter*  _marker;

/** Parameters */
double time_glfw = 0;
bool _static_win = false;
bool _set_bbox = false;
bool _update_bbox = false;


// /** Graphic */
// bool _end_render = false;

/** Debug */
bool _debug_fg = false;

//******************************************************************************
/**
 * Callback for keyboard events
 */
/** Update the string that will be used as Figure title text.
 * WARNING : _liner must be defined
 */
std::string str_title ()
{
  std::stringstream title;
  title << "Markers & Lines ";
  if (_liner->get_continuous()) {
    title << "(CONT)";
  }
  else {
    title << "(SEG)";
  }
  title << "[ESC:quit, D:debug, S:save as 010-marker.png]";

  return title.str();
}
static void key_callback(int key, int scancode, int action, int mods)
{
  // ESC => ask for closing window
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(_win->_window, GL_TRUE);
  }

  // D/d => set/unset DEBUG mode
  else if (key == GLFW_KEY_D && action == GLFW_PRESS) {
    _debug_fg = not _debug_fg;
    _win->set_debug( _debug_fg );   
    _fig->set_debug( _debug_fg );
    _curve_main->set_debug( _debug_fg );
    _curve_view->set_debug( _debug_fg );
    _marker->set_debug( _debug_fg );
  }

  // C/c => switch continuous/segments
  else if (key == GLFW_KEY_C && action == GLFW_PRESS) {
   _liner->set_continuous( !_liner->get_continuous() );
   _fig->set_title( str_title() );
  }
  
  // S/s => save in .png file
  else if (key == GLFW_KEY_S && action == GLFW_PRESS) {
    // save window
    _win->save( "010-marker.png" );
  }
}

// ***************************************************************************
// ********************************************************* Graphic Functions
// ***************************************************************************
// ******************************************************************** render
void render()
{
  while( not visugl::grafik::should_stop() and !glfwWindowShouldClose(_win->_window)) {
    time_glfw = glfwGetTime();
    
    _win->render();
  }
}

// ***************************************************************************
// ********************************************************************** MAIN
// ***************************************************************************
int main( int argc, char *argv[] )
{
  // check if a staticWindow is to be used
  // test for arguments 'static' or 'help'
  if (argc > 1) {
    for( unsigned int i = 1; i < argc; ++i) {
      std::string arg = argv[i];
      // help ?
      if ((arg.find( "help" ) != std::string::npos) or
          (arg.find( "-h" ) != std::string::npos)) {
        std::cout << "usage : " << argv[0] << " -h/help -d/debug -b/bbox -u/update -s/static " << std::endl;
        exit(0);
      }
      // static
      if ((arg.find( "static" ) != std::string::npos) or
          (arg.find( "-s" ) != std::string::npos)) {
        _static_win = true;
      }
      // choose bbox of figure
      else if ((arg.find( "bbox" ) != std::string::npos) or
               (arg.find( "-b" ) != std::string::npos)) {
        _set_bbox = true;
      }
      // update_bbox
      else if ((arg.find( "update" ) != std::string::npos) or
               (arg.find( "-u" ) != std::string::npos)) {
        _update_bbox = true;
      }
      // debug
      else if ((arg.find( "debug" ) != std::string::npos) or
               (arg.find( "-d" ) != std::string::npos)) {
        _debug_fg = true;
      }
      else {
        std::cout << argv[0] << " ERROR: unrecognized argument " << arg  << std::endl;
      }
    }
  }
  
  // Main Data
  _curve_main = new visugl::Data();
  _curve_main->clear();
  _curve_main->set_debug( _debug_fg );
  _curve_main->set_name( "DATA_MAIN" );
  
  const unsigned int _nb_data = 100;
  for( unsigned int i=0; i < _nb_data; ++i) {
    visugl::Sample pt;
      pt.x = 2.0 * M_PI * i / _nb_data;
      pt.y = sin( pt.x );
      pt.z = 0.0;      
      _curve_main->add( pt );
  }

  // MarkData: subset of _curve_main
  _curve_view = new visugl::Data();
  _curve_view->clear();
  _curve_view->set_debug( _debug_fg );
  _curve_view->set_name( "DATA_VIEW" );

  unsigned int id = 0;
  for( auto& pt : _curve_main->get_samples() ) {
    if( id % 10 == 0 ) {
      _curve_view->add( pt );
    }
    ++id;
  }

  // ****** GRAPHIC ********
  visugl::grafik::init();

  // Plotters
  _liner = new visugl::LinePlotter( _curve_main );
  _liner->set_continuous( true );
  _liner->set_name( "Line" );
  _liner->set_color( {1.f, 0.f, 0.f } );
  _marker = new visugl::MarkPlotter( _curve_view );
  _marker->set_name( "Mark" );

  std::cout << "__WINDOW and FIGURE" << std::endl;
  if (_static_win) {
    _win = new visugl::WindowStatic( "010-markplot", 600, 600, /* size */
                                     false /* offscreen */, 20, 20 /* position */ );
    _win->set_name( "WINSTAT" );
  }
  else {
    _win = new visugl::Window( "010-markplot", 600, 600, /* size */
                               false /* offscreen */, 20, 20 /* position */ );
    _win->set_name( "WIN" );
  }
  _win->set_debug( _debug_fg );

  _fig = new visugl::Figure( /**_win,*/ str_title() );
  _fig->set_debug( _debug_fg );
  _fig->set_name( "FIG" );
  _fig->add_plotter( _liner );
  _fig->add_plotter( _marker );

  _win->add_plotter( _fig );

  // Add a key_callback
  _win->add_keycallback( key_callback );

  // By default, Figure bbox is [-1,1] x [-1,1], at the center of the WinHolder
  // with a margin of 0.1 around it

  // Setting the bbox of the Figure
  if (_set_bbox) {
    _fig->set_bbox( -0.5, 2.5, -1.0, 1.0 );
  }
  
  // update_bbox ask its size to every sub-plotter, recursive
  // so the whole curve is displayed
  if (_update_bbox) {
    _win->update_bbox();
  }

  std::cout << "__RENDER" << std::endl;
  render();

  std::cout << "__END" << std::endl;
  delete _marker;
  delete _liner;
  delete _curve_view;
  delete _curve_main;
  delete _fig;
  delete _win;

  visugl::grafik::end();
  
  return 0;
}
