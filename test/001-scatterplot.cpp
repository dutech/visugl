/* -*- coding: utf-8 -*- */

/** 
 * 001-scatterplot.cpp
 *
 * Full VisuGL example.
 * Window + Figure with title
 *   + _scatter1 as example
 *   + _scatter2 as a copy of ScatterPlotter1 with added Samples
 *   + _scatter3 fed with Collections of double
 *   + _scatter4 fed with "time serie" (Collection of Y)
 *   + text to label the ScatterPlotters
 * 
 * 'D' or 'd' -> debug grafik
 * 'S' or 's' -> save drawing as "001-curve.png"
 */

#include <string>
#include <stdlib.h>
#include <iostream>

#include <vector>

// declare Window, Figure,
#include <visugl.hpp>
// using namespace visugl; // or use visugl:: everywhere...
#include <scatter.hpp>

/** Window, Figure and Plotters */
visugl::Window* _win;
visugl::Figure* _fig;
visugl::Curve*  _curve1;
visugl::Curve*  _curve2;
visugl::ScatterPlotter*  _scatter1;
visugl::ScatterPlotter*  _scatter2;

/** Parameters */
double time_glfw = 0;

/** Graphic */
bool _end_render = false;

/** Debug */
bool _debug_fg = false;

//******************************************************************************
/**
 * Callback for keyboard events
 * => save figure
 */
static void key_callback(int key, int scancode, int action, int mods)
{
  // ESC => ask for closing window
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(_win->_window, GL_TRUE);
  }

  // D/d => set/unset DEBUG mode
  else if (key == GLFW_KEY_D && action == GLFW_PRESS) {
    _debug_fg = not _debug_fg;
    _win->set_debug( _debug_fg );   
    _fig->set_debug( _debug_fg );
    _curve1->set_debug( _debug_fg );
    _curve2->set_debug( _debug_fg );
    _scatter1->set_debug( _debug_fg );
    _scatter2->set_debug( _debug_fg );
  }
  
  // S/s => save in .png file
  else if (key == GLFW_KEY_S && action == GLFW_PRESS) {
    // save window
    _win->save( "001-scatter.png" );
  }
}

// ***************************************************************************
// ********************************************************* Graphic Functions
// ***************************************************************************
/**
 * Init and end GLFW
*/
void init_glfw()
{
  std::cout << "__GLFW Init" << std::endl;
  
  glfwSetErrorCallback(visugl::Window::error_callback);
  
  if (!glfwInit())
        exit(EXIT_FAILURE);
}
void glfw_end()
{
  glfwTerminate();
  std::cout << "__GLFW destroyed" << std::endl;
}
// ******************************************************************** render
void render()
{
  while( not _end_render and !glfwWindowShouldClose(_win->_window) ) {
    time_glfw = glfwGetTime();
    
    _win->render();

  }
}
//******************************************************************************
int main( int argc, char *argv[] )
{
  // A static ScatterPlotter
  std::cout << "__CURVES & SCATTER" << std::endl;
  _curve1 = new visugl::Curve(); // default is red thin line
  _curve1->clear();
  _curve1->set_debug( _debug_fg );
  _curve1->set_name( "CURVE1" );

  _scatter1 = new visugl::ScatterPlotter( *_curve1 );
  _scatter1->set_debug( _debug_fg );
  _scatter1->set_name( "SCATTER1" );
  _scatter1->set_size( 0.01f );
  
  
  const unsigned int _nb_data = 100;
  for( unsigned int i=0; i < _nb_data; ++i) {
    visugl::Sample pt;
      pt.x = 2.0 * M_PI * i / _nb_data;
      pt.y = sin( pt.x );
      pt.z = 0.0;      
      _curve1->add_sample( pt );
  }
  std::cout << "  scatter1 bbox=" << _scatter1->get_bbox() << std::endl;
  std::cout << "  curve1 bbox=" << _curve1->get_bbox() << std::endl;

  // Copy to which we change points, and "go back"
  _curve2 = new visugl::Curve( *_curve1 );
  _curve2->set_debug( _debug_fg );
  _curve2->set_name( "CURVE2" );
  _scatter2 = new visugl::ScatterPlotter( *_curve2 );
  _scatter2->set_debug( _debug_fg );
  _scatter2->set_name( "SCATTER2" );
  _scatter2->set_color( {0.0, 0.1, 1.0} ); // blue line
  _scatter2->set_size( 0.05f );
  // inverse points
  // get_samples (a COPY) , inverse, and copy back to curve2
  auto samples2 = _curve2->get_samples(); // a COPY
  _curve2->clear();                       // can clear without fear
  for( auto& pt: samples2) {
    pt.y = -pt.y;
    _curve2->add_sample( pt );
  }
  // then add some "weird" points
  _curve2->add_sample( {3.14, 0.4, 0.0} ); // NOT working (because inside bbox)
  _curve2->add_data( {3.14, -0.4, 0.0} );  // working (even inside bbox)
  std::cout << "  c2 bbox=" << _scatter2->get_bbox() << std::endl;
    
  // ****** GRAPHIC ********
  init_glfw();
  std::cout << "__WINDOW and FIGURE" << std::endl;
  _win = new visugl::Window( "001-curve", 600, 600, /* size */
                             false /* offscreen */, 20, 20 /* position */ );
  _win->set_debug( _debug_fg );
  _win->set_name( "WIN" );
  _fig = new visugl::Figure( *_win, "Several ScatterPlotters          [ESC:quit, D:debug, S:save as 001-scatter.png]" );
  _fig->set_debug( _debug_fg );
  _fig->set_name( "FIG" );
  
  // Add a key_callback
  _win->add_keycallback( key_callback );

  _win->add_plotter( _fig );
  _fig->add_plotter( _scatter1 );
  _fig->add_text("scatter1", 2.0, 0.95, {1.0,0.0,0.0});
  _fig->add_plotter( _scatter2 );
  _fig->add_text("scatter2", 5.2, 0.95, {0.0,0.0,1.0});
  _win->update_bbox();
  
  std::cout << "__RENDER" << std::endl;
  render();

  std::cout << "__END" << std::endl;
  delete _curve1;
  delete _curve2;
  delete _scatter1;
  delete _scatter2;
  delete _fig;
  delete _win;

  glfw_end();
  
  return 0;
}
