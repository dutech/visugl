/* -*- coding: utf-8 -*- */

/** 
 * 002-dyncurve.cpp
 *
 * VisuGL example.
 * Window + Figure with title
 *   + Curve to which we periodically add DATA (as sample is for time series)
 *   + Curve has a random start point (_startx,_starty) and a random mean
 *     direction (_dir) to which some sinus perturbation is added.
 *   + CurveMean that can switch between "NORMAL" and "MEAN" mode (key W)
 *
 * 'D' or 'd' -> debug grafik
 * 'W' or 'w' -> switch between "normal" and "mean" mode
 * 'N' or 'n' -> print nb of samples in each curve
 * 'SPC'      -> pause simulation
 */

#include <string>
#include <stdlib.h>
#include <iostream>

#include <vector>

// ************************************************************ Random GLOBALS
#include <random>
// Random Generator
std::random_device _random_seeder;
std::default_random_engine _rnd_engine( _random_seeder() );
std::uniform_real_distribution<double> _rnd_unif = std::uniform_real_distribution<double>(0.0, 1.0);
// random parameters for curve
double _startx, _starty;
double _dir;
double _time = 0.0;
double _old_time;

// declare Window, Figure,
#include <visugl.hpp>
using namespace visugl;
#include <curve.hpp>

/** Window, Figure and Plotters */
Window* _win;
Figure* _fig;
Curve*  _curve;
CurveMean* _curve_mean;

/** Graphic */
bool _end_render = false;

/** Debug */
bool _debug_fg = false;
bool _play_flag = true;

//******************************************************************************
/**
 * Callback for keyboard events
 * deals with W and D
 */
static void key_callback(int key, int scancode, int action, int mods)
{
  // ESC => ask for closing window
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(_win->_window, GL_TRUE);
  }

  // D/d => set/unset DEBUG mode
  else if (key == GLFW_KEY_D && action == GLFW_PRESS) {
    _debug_fg = not _debug_fg;
    _win->set_debug( _debug_fg );   
    _fig->set_debug( _debug_fg );   
    _curve->set_debug( _debug_fg );
    _curve_mean->set_debug( _debug_fg );
  }
  
  // W or w : switch _curve_mean_mode 
  else if ( key == GLFW_KEY_Z && action == GLFW_PRESS) {
    // toggle _mean
    if( _curve_mean->get_mean_mode() ) {
      std::cout << "  Normal mode" << std::endl;
      _curve_mean->set_mean_mode( false );
    }
    else {
      std::cout << "  MEAN mode" << std::endl;
      _curve_mean->set_mean_mode( true );
      _curve_mean->recompute_means();
    }
  }
  // display nb of samples in curves
  else if ( key == GLFW_KEY_N && action == GLFW_PRESS ) {
    std::cout << _curve->get_samples().size() << " samples in each curve" << std::endl;
  }
  // SPACE => pause/run
  else if (key == GLFW_KEY_SPACE && action == GLFW_PRESS) {
    _play_flag = not _play_flag;
  }
}

// ***************************************************************************
// ********************************************************* Graphic Functions
// ***************************************************************************
/**
 * Init and end GLFW
*/
void init_glfw()
{
  std::cout << "__GLFW Init" << std::endl;
  
  glfwSetErrorCallback(Window::error_callback);
  
  if (!glfwInit())
        exit(EXIT_FAILURE);
}
void glfw_end()
{
  glfwTerminate();
  std::cout << "__GLFW destroyed" << std::endl;
}
// ******************************************************************** render
void update_and_render()
{
  glfwSetTime(0.0);

  GLenum err;
  while ((err = glGetError()) != GL_NO_ERROR) {
    std::cerr << "UPD::OpenGL error: " << err << std::endl;
  }
  unsigned int nb_render = 0;
  while( not _end_render and !glfwWindowShouldClose(_win->_window) ) {
    // add data to _curve
    auto ctime = glfwGetTime();
    if (_play_flag ) {
      _time += (ctime - _old_time);
      auto new_x = _startx + _time/5.0 * cos(_dir);
      auto new_y = _starty + _time/5.0 * sin(_dir) + sin(_time)+0.1*sin(50.0*_time);
      _curve->add_data( {new_x, new_y, 0.0} );
      _curve_mean->add_data( {new_x, new_y, 0.0} );
      _win->update_bbox();
    }
    _old_time = ctime;
    
    while ((err = glGetError()) != GL_NO_ERROR) {
      std::cerr << "UPDA " << nb_render << " OpenGL error: " << err << std::endl;
    }
    _win->render();

    while ((err = glGetError()) != GL_NO_ERROR) {
      std::cerr << "REND " << nb_render << " OpenGL error: " << err << std::endl;
    }
    nb_render++;
  }
}
//******************************************************************************
int main( int argc, char *argv[] )
{
  // set random parameters
  _startx = _rnd_unif(_rnd_engine) * 2.0 - 1.0;
  _starty = _rnd_unif(_rnd_engine) * 2.0 - 1.0;
  _dir = (_rnd_unif(_rnd_engine) *2.0 - 1.0) * M_PI;
  std::cout << "From (" << _startx << ", " << _starty << ") in ";
  std::cout <<  _dir  << " [" << (_dir/M_PI*180.0) << "]" << std::endl;
  
  // A dynamic Curve and its mean
  std::cout << "__CURVE" << std::endl;
  _curve = new Curve(); // default is red thin line
  _curve->clear();
  _curve->set_debug( _debug_fg );
  _curve->set_name( "CURVE" );
  _curve_mean = new CurveMean();
  _curve_mean->set_color( {0.0, 0.0, 1.0} );
  _curve_mean->set_width( 2.0f );
  _curve_mean->clear();
  _curve_mean->set_debug( _debug_fg );
  _curve_mean->set_name( "CURVE_MEAN" );
  
  // ****** GRAPHIC ********
  init_glfw();
  GLenum err;
  std::cout << "__ READ buffer start" << std::endl;
  while ((err = glGetError()) != GL_NO_ERROR) {
    std::cerr << "INIT::OpenGL error: " << err << std::endl;
  }
  std::cout << "__WINDOW and FIGURE" << std::endl;
  _win = new Window( "002-dyncurve", 600, 600, /* size */
                             false /* offscreen */, 20, 20 /* position */ );
  _win->set_debug( _debug_fg );
  _win->set_name( "WIN" );
  // add a key_callback
  _win->add_keycallback( key_callback );
  
  _fig = new Figure( *_win, "Dyn Curve    [ESC:quit, W:Mean ON/OFF, N:info, D:debug, SPC:pause]" );
  _fig->set_debug( _debug_fg );
  _fig->set_name( "FIG" );
  //_fig->_update_axes_x = true;
  //_fig->_update_axes_y = true;
  _win->add_plotter( _fig );
  _fig->add_plotter( _curve_mean );
  _fig->add_plotter( _curve );      // will overwrite other curve if identical
  _win->update_bbox();
  
  std::cout << "__RENDER" << std::endl;
  _old_time = glfwGetTime();
  update_and_render();

  std::cout << "__END" << std::endl;
  delete _curve;
  delete _fig;
  delete _win;

  glfw_end();
  
  return 0;
}
