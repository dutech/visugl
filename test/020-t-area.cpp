/* WIP WIP WIP WIP
 TEST. Pour mieux comprendre la notion de Area vs Plottable vs Plotter.

 [x] Win avec un/des diamands.
 [x] drawing area un peu plus petite que Win
     [x] avec Projection matrix
     [x] avec ViewPort

 ** example
 Win avec 2 Figure, l'une qui reste autour de (-2,-2)@(2,2), l'autre qui s'adapte à la courbe qui grandit
 Bonus : dans la 2ème, une sous-figure/area, encadré, qui zoom.
 BigBnus : dans la 1ère, une sous-figure qui montre tout.
 Tracé : diamant (4coins) + une courbe qui grandit.
*/

#include <iostream>
#include <string>

// include OpenGL > 1.1
#include <GL/glew.h>
#include <GL/gl.h>               // OpenGL
#include <GLFW/glfw3.h>

// ********************************************************************* Color
struct Color {
  double r,g,b;
};
// ******************************************************************** Sample
struct Sample {
  double x,y,z;
};

// ******************************************************************** Global
bool _debug_cbk {false};
GLFWwindow* _window;
int _width  {640};
int _height {400};

// ************************************************************** plot_diamond
// plot diamand (rotated square)
void plot_diamond(const Sample& pt, const double size, const Color& _fg_col,
                  const GLfloat width)
{
    // Color
    glColor4d( _fg_col.r, _fg_col.g, _fg_col.b, 1.0);

    //  Rendering using GL_LINE_STRIP or GL_LINES
    glEnable (GL_BLEND);
    glEnable (GL_LINE_SMOOTH);
    glLineWidth( width );

    glBegin(GL_LINE_STRIP);
    glVertex3d( pt.x+size, pt.y, pt.z );
    glVertex3d( pt.x, pt.y+size, pt.z );
    glVertex3d( pt.x-size, pt.y, pt.z );
    glVertex3d( pt.x, pt.y-size, pt.z );
    glVertex3d( pt.x+size, pt.y, pt.z );
    glEnd();
}

// ***************************************************************** callbacks
void error_callback(int error, const char* description)
{
  std::cerr <<  description << std::endl;
  //fputs(description, stderr);
}
void resize_callback( GLFWwindow* window, int width, int height )
{
  if (_debug_cbk) {
    std::cout << "__resize_cbk w=" << width << " h=" << height << std::endl;
  }
  glfwGetFramebufferSize( _window, &_width, &_height);
  if (_debug_cbk) {
    std::cout << "  after w=" << _width << " h=" << _height << std::endl;
  }
}
void move_callback( GLFWwindow *window, int xpos, int ypos )
{
  if (_debug_cbk) {
    std::cout << "__move_cbk x=" << xpos << " y=" << ypos << std::endl;
  }
}
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
  if (_debug_cbk) {
    std::cout << "__key_cbk key=" << key << " scancode=" << scancode;
    std::cout << " act=" << action << " mods=" << mods << std::endl;
  }

  // ESC => ask for closing window
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(_window, GL_TRUE);
  }
}

// ************************************************************* create_window
void create_window()
{
  std::string _title = "WinHolder";
  const int posx=-1; const int posy = -1;

  // Create window _________________________________________________
  glfwSetErrorCallback(error_callback);

  if (!glfwInit())
    exit(EXIT_FAILURE);

  _window = glfwCreateWindow(_width, _height, _title.c_str(), NULL, NULL);
  if (! _window ) {
    glfwTerminate();
    exit(EXIT_FAILURE);
  }
  glfwSetWindowPos( _window, posx, posy );
  glfwMakeContextCurrent( _window );
  // TODO can also be set to another DataStructure
  //glfwSetWindowUserPointer( _window, this);
  glfwSetKeyCallback( _window, key_callback);
  glfwSetWindowSizeCallback( _window, resize_callback );
  glfwSetWindowPosCallback( _window, move_callback);
}
// ****************************************************************** clean_up
void clean_up()
{
  if (_window) {
    glfwSetWindowShouldClose(_window, GL_TRUE);
    glfwDestroyWindow( _window);
    _window = nullptr;
  }
}
// ******************************************************************** render
void render()
{
  // white area with padded grey area
  // clear all buffer (except if glScissor)
  glClearColor( 0.7, 0.7, 0.7, 1.0); // grey
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

  // then white in the inside
  // glEnable(GL_SCISSOR_TEST);
  // glScissor( 10, 10, _width-20, _height-20 );
  // glClearColor( 1.0, 1.0, 1.0, 1.0);
  // glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
  // glDisable(GL_SCISSOR_TEST);

  // Viewport affects only vertex
  // by default, Viewport maps (-1,-1)@(1,1) to the area selected
  double xratio = 1.0 / static_cast<double>(_width);
  double yratio = 1.0 / static_cast<double>(_height);
  int vx = 0.0 + 0.05 * xratio;
  int vy = 0.0 + 0.05 * yratio;
  int vw = 0.47 * xratio;
  int vh = 0.9 * yratio; // ststic_cast<int>(std::round())
  glViewport( 20, 20, _width/2-30, _height-40 );
  glEnable(GL_SCISSOR_TEST);
  glScissor( 20, 20, _width/2-30, _height-40 );
  glClearColor( 1.0, 1.0, 1.0, 1.0 );
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
  glDisable(GL_SCISSOR_TEST);

  // setup GL_PROJECTION to _bbox
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho( -1.0, 1.0, -1.0, 1.0, 1.f, -1.f); // multiply current mat
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // setup GL_PROJECTION to _bbox
  // glMatrixMode(GL_PROJECTION);
  // glLoadIdentity();
  // glOrtho( _bbox.x_min, _bbox.x_max, _bbox.y_min, _bbox.y_max, 1.f, -1.f);
  // glMatrixMode(GL_MODELVIEW);
  // glLoadIdentity();

  plot_diamond( {0,0,0}, 0.5, {1,0,0}, 1.0 ); // center, size, rgb, width
  plot_diamond( {0,0,0}, 1.0, {0,1,0}, 1.0 ); // center, size, rgb, width
  plot_diamond( {0,0,0}, 1.5, {0,0,1}, 1.0 ); // center, size, rgb, width

  // Viewport affects only vertex
  // by default, Viewport maps (-1,-1)@(1,1) to the area selected
  glViewport( _width/2+10, 20, _width/2-30, _height-40 );
  glEnable(GL_SCISSOR_TEST);
  glScissor( _width/2+10, 20, _width/2-30, _height-40 );
  glClearColor( 1.0, 1.0, 1.0, 1.0 );
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
  glDisable(GL_SCISSOR_TEST);

  // setup GL_PROJECTION to _bbox
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho( -2.0, 2.0, -2.0, 2.0, 1.f, -1.f); // multiply current mat
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  plot_diamond( {0,0,0}, 0.5, {1,0,0}, 1.0 ); // center, size, rgb, width
  plot_diamond( {0,0,0}, 1.0, {0,1,0}, 1.0 ); // center, size, rgb, width
  plot_diamond( {0,0,0}, 1.5, {0,0,1}, 1.0 ); // center, size, rgb, width

  // Viewport affects only vertex
  // by default, Viewport maps (-1,-1)@(1,1) to the area selected
  glViewport( _width/2+10+50, 20+50, _width/6, _height/6 );
  glEnable(GL_SCISSOR_TEST);
  glScissor( _width/2+10+50, 20+50, _width/6, _height/6 );
  glClearColor( 0.3, 0.3, 0.3, 1.0 );
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
  glDisable(GL_SCISSOR_TEST);

  // setup GL_PROJECTION to _bbox
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho( -2.0, 2.0, -2.0, 2.0, 1.f, -1.f); // multiply current mat
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  plot_diamond( {0,0,0}, 0.5, {1,0,0}, 1.0 ); // center, size, rgb, width
  plot_diamond( {0,0,0}, 1.0, {0,1,0}, 1.0 ); // center, size, rgb, width
  plot_diamond( {0,0,0}, 1.5, {0,0,1}, 1.0 ); // center, size, rgb, width

  glfwSwapBuffers( _window );
  glfwPollEvents();
}

// ***************************************************************************
// ********************************************************************** main
// ***************************************************************************
int main(int argc, char *argv[]) {

  if (!glfwInit()) {
    exit(EXIT_FAILURE);
  }

  create_window();

  while( !glfwWindowShouldClose(_window) ) {
    render();
  }

  clean_up();

  glfwTerminate();

  return 0;
}
