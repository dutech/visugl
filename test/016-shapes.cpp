/* -*- coding: utf-8 -*- */

/** 
 * 016-shapes.cpp
 *
 * VisuGL example with Shape2D and Shape2DPlotters
 * Window + Figure with title
 *   + _circle (as Cirle2D; kind of Shape2D)
 *   + _shaper  as Shap2DPlotter of _circle
 *
 * Options:
 * -s/static : use a WinStat as WinHolder (faster but linewidth is changed)
 * -b/bbox : force the BBox of the Figure to [-0.5,2.5] x [-1,1]
 * -u/update : update the BBox to the Data
 * -d/debug : grafik debug
 *
 * 'D' or 'd' -> debug grafik
 * 'S' or 's' -> save drawing as "016-shapes.png"
 */
#include <iostream>
#include <string>         // std::string
#include <sstream>

// declare Window, Figure,
#include <visugl.hpp>
// using namespace visugl; // or use visugl:: everywhere...
#include <shape2D_plotter.hpp>

/** Window, Figure and Plotters */
visugl::WinHolder* _win;
visugl::Figure* _fig;
visugl::Circle2D* _circle1;
visugl::Circle2D* _circle2;
visugl::Rect2D* _rect;
visugl::Shape2DPlotter* _shaper;

/** Parameters */
double time_glfw = 0;
bool _static_win = false;
bool _set_bbox = false;
bool _update_bbox = false;

/** Debug */
bool _debug_fg = false;

//******************************************************************************
/**
 * Callback for keyboard events
 */
/** Update the string that will be used as Figure title text.
 * WARNING : _liner must be defined
 */
std::string str_title ()
{
  std::stringstream title;
  title << "Shapes ";
  title << "[ESC:quit, D:debug, S:save as 016-shapes.png]";

  return title.str();
}
static void key_callback(int key, int scancode, int action, int mods)
{
  // ESC => ask for closing window
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(_win->_window, GL_TRUE);
  }

  // D/d => set/unset DEBUG mode
  else if (key == GLFW_KEY_D && action == GLFW_PRESS) {
    _debug_fg = not _debug_fg;
    _win->set_debug( _debug_fg );   
    _fig->set_debug( _debug_fg );
  }

  // S/s => save in .png file
  else if (key == GLFW_KEY_S && action == GLFW_PRESS) {
    // save window
    _win->save( "016-shapes.png" );
  }
}

// ***************************************************************************
// ********************************************************* Graphic Functions
// ***************************************************************************
// ******************************************************************** render
void render()
{
  while( not visugl::grafik::should_stop() and !glfwWindowShouldClose(_win->_window) ) {
    time_glfw = glfwGetTime();
    
    _win->render();
  }
}

// ***************************************************************************
// ********************************************************************** MAIN
// ***************************************************************************
int main( int argc, char *argv[] )
{
  // check if a staticWindow is to be used
  // test for arguments 'static' or 'help'
  if (argc > 1) {
    for( unsigned int i = 1; i < argc; ++i) {
      std::string arg = argv[i];
      // help ?
      if ((arg.find( "help" ) != std::string::npos) or
          (arg.find( "-h" ) != std::string::npos)) {
        std::cout << "usage : " << argv[0] << " -h/help -d/debug -b/bbox -u/update -s/static " << std::endl;
        exit(0);
      }
      // static
      if ((arg.find( "static" ) != std::string::npos) or
          (arg.find( "-s" ) != std::string::npos)) {
        _static_win = true;
      }
      // choose bbox of figure
      else if ((arg.find( "bbox" ) != std::string::npos) or
               (arg.find( "-b" ) != std::string::npos)) {
        _set_bbox = true;
      }
      // update_bbox
      else if ((arg.find( "update" ) != std::string::npos) or
               (arg.find( "-u" ) != std::string::npos)) {
        _update_bbox = true;
      }
      // debug
      else if ((arg.find( "debug" ) != std::string::npos) or
               (arg.find( "-d" ) != std::string::npos)) {
        _debug_fg = true;
      }
      else {
        std::cout << argv[0] << " ERROR: unrecognized argument " << arg  << std::endl;
      }
    }
  }
  
  // Main Data
  _circle1 = new visugl::Circle2D( 0.3 );
  _circle2 = new visugl::Circle2D( 0.2 );
  _circle2->_fg_col = {1.0, 0.0, 0.0};
  _circle2->_origin = {0.2, 0.2, 0.0};
  _circle2->set_width(5.0);
  _rect = new visugl::Rect2D( 0.7, 0.3 );
  _rect->_fg_col = {0.0, 0.0, 1.0};
  _rect->_origin = {-0.2, 0.2, 0.0};
  _rect->_orient = 3.14 / 4.0;

  // ****** GRAPHIC ********
  visugl::grafik::init();

  // Plotters
  _shaper = new visugl::Shape2DPlotter();
  _shaper->add_shape( _circle1 );
  _shaper->add_shape( _circle2 );
  _shaper->add_shape( _rect );

  std::cout << "__WINDOW and FIGURE" << std::endl;
  if (_static_win) {
    _win = new visugl::WindowStatic( "016-shapes", 600, 600, /* size */
                                     false /* offscreen */, 20, 20 /* position */ );
    _win->set_name( "WINSTAT" );
  }
  else {
    _win = new visugl::Window( "016-shapes", 600, 600, /* size */
                               false /* offscreen */, 20, 20 /* position */ );
    _win->set_name( "WIN" );
  }
  _win->set_debug( _debug_fg );

  _fig = new visugl::Figure( str_title() );
  _fig->set_debug( _debug_fg );
  _fig->set_name( "FIG" );
  _fig->add_plotter( _shaper );

  _win->add_plotter( _fig );

  // Add a key_callback
  _win->add_keycallback( key_callback );

  // By default, Figure bbox is [-1,1] x [-1,1], at the center of the WinHolder
  // with a margin of 0.02 around it

  // Setting the bbox of the Figure
  if (_set_bbox) {
    _fig->set_bbox( -0.5, 2.5, -1.0, 1.0 );
  }
  
  // update_bbox ask its size to every sub-plotter, recursive
  // so the whole curve is displayed
  if (_update_bbox) {
    _win->update_bbox();
  }

  std::cout << "__RENDER" << std::endl;
  render();

  std::cout << "__END" << std::endl;

  delete _rect;
  delete _circle1;
  delete _circle2;
  delete _shaper;
  delete _fig;
  delete _win;

  visugl::grafik::end();
  
  return 0;
}
