/* -*- coding: utf-8 -*- */

/** 
 * 101-curve_ant.cpp
 *
 * VisuGL + AntTweakBar example with Curve and Scatter
 */
// to be sure
#ifndef USE_ANTTWEAKBAR
#define USE_ANTTWEAKBAR
#endif

// declare Window, Figure,
#include <visugl.hpp>
using namespace visugl;

#include <curve.hpp>
#include <scatter.hpp>

Window *_win;
Figure *_fig;
Curve *_c_sin;
float _curve_color[] = { 0.7f, 0.5f, 0.6f };
ScatterPlotter* _scatter;
bool _scatter_active = false;
float _scatter_size = 0.15;
float _scatter_color[] = { 1.0f, 0.0f, 0.0f };

/** Parameters */
double time_glfw = 0;

/** Graphic */
bool _end_render = false;

//******************************************************************************
/**
 * Callback for keyboard events
 */
static void key_callback(int key, int scancode, int action, int mods)
{
  // ESC => ask for closing window
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(_win->_window, GL_TRUE);
  }
  
  else if (key == GLFW_KEY_S && action == GLFW_PRESS) {
    // save window
    _win->save( "101-curve.png" );
  }
}

// ***************************************************************************
// ********************************************************* Graphic Functions
// ***************************************************************************
/**
 * Init and end GLFW
*/
void init_glfw()
{
  std::cout << "__GLFW Init" << std::endl;
  
  glfwSetErrorCallback(Window::error_callback);
  
  if (!glfwInit())
        exit(EXIT_FAILURE);
}
void glfw_end()
{
  glfwTerminate();
  std::cout << "__GLFW destroyed" << std::endl;
}

// ***************************************************************************
// ********************************************************************** MAIN
// ***************************************************************************
int main(int argc, char *argv[])
{
  init_glfw();
  
  // ****** Grafik *****
  std::cout << "__WINDOW and FIGURE" << std::endl;
  _win = new Window( "101-CURVE WITH AntTweakBAR", 600, 600,
                     false /*offscreen*/, 20, 20);
  // add a key_callback
  _win->add_keycallback( key_callback );
  
  _fig = new Figure( *_win, "Curves          [ESC:quit, S:save as 101-curve.png]" );
  _win->add_plotter( _fig );

  // ****** Plotters *****
  std::cout << "__CURVE" << std::endl;
  _c_sin = new Curve();
  // populate with a sinus
  const unsigned int _nb_data = 100;
  for( unsigned int i=0; i < _nb_data; ++i) {
    Curve::Sample pt;
    pt.x = 2.0 * M_PI * i / _nb_data;
    pt.y = sin( pt.x );
    pt.z = 0.0;      
    _c_sin->add_sample( pt );
  }
  _fig->add_plotter( _c_sin );
  // ScatterPlotter
  _scatter = new ScatterPlotter( *_c_sin );
  _fig->add_plotter( _scatter );
  
  _win->update_bbox();

  // ****** Anttweakbar *****
  std::cout << "__ANTTWEAKBAR" << std::endl;
  // Add 'time' to 'bar': it is a read-only (RO) variable of type TW_TYPE_DOUBLE, with 1 precision digit */
  TwAddVarRO( _win->_bar, "time", TW_TYPE_DOUBLE, &time_glfw,
             " label='Time' precision=1 help='Time (in seconds).' ");
  
  // curve_color
  TwAddVarRW( _win->_bar, "curve_color", TW_TYPE_COLOR3F, &_curve_color, 
              " label='Curve color' help='Color of the curve.' ");

  // scatter active ?
  TwAddVarRW( _win->_bar, "scattter ?", TW_TYPE_BOOLCPP, &_scatter_active,
              " label='Plot scatter ?' help='Switch between plot/no-plot' key=SPACE");

  // scatter size ?
  TwAddVarRW( _win->_bar, "size", TW_TYPE_FLOAT, &_scatter_size,
              " label='Scatter Size' min=0 step=0.01 help='Size of each marker.' " );
  // scatter_color
  TwAddVarRW( _win->_bar, "scatter_color", TW_TYPE_COLOR3F, &_scatter_color, 
              " label='Scatter color' help='Color of the scatter marker.' ");
  
  // ****** Render *****
  while( not _end_render and !glfwWindowShouldClose(_win->_window) ) {
    time_glfw = glfwGetTime();
    
    // update plotters
    _c_sin->set_color( {_curve_color[0], _curve_color[1], _curve_color[2]} );
    _scatter->set_active( _scatter_active );
    _scatter->set_size( _scatter_size );
    _scatter->set_color( {_scatter_color[0], _scatter_color[1], _scatter_color[2]} );
    
    _win->render();
  }

  // ****** Clean up *****
  std::cout << "__END" << std::endl;
  delete _c_sin;
  delete _scatter;
  delete _fig;
  delete _win;

  glfw_end();
  
  return 0;
}
