/* -*- coding: utf-8 -*- */

/** 
 * 001-curveoffscreen.cpp
 *
 * Full VisuGL example rendered OFFSCREEN and saved as 001-curveoffscreen.png
 * Window + Figure with title
 *   + Curve as example
 * 
 * 'S' or 's' -> save drawing as "001-curve.png"
 */

#include <string>
#include <stdlib.h>
#include <iostream>

// declare Window, Figure,
#include <visugl.hpp>
using namespace visugl;
#include <curve.hpp>

/** Window, Figure and Plotters */
Window* _win;
Figure* _fig;
Curve*  _curve;

/** Parameters */
double time_glfw = 0;

/** Graphic */
bool _end_render = false;

// ***************************************************************************
// ********************************************************* Graphic Functions
// ***************************************************************************
/**
 * Init and end GLFW
*/
void init_glfw()
{
  std::cout << "__GLFW Init" << std::endl;
  
  glfwSetErrorCallback(Window::error_callback);
  
  if (!glfwInit())
        exit(EXIT_FAILURE);
}
void glfw_end()
{
  glfwTerminate();
  std::cout << "__GLFW destroyed" << std::endl;
}

//******************************************************************************
int main( int argc, char *argv[] )
{
  // A static Curve
  std::cout << "__CURVE" << std::endl;
  _curve = new Curve(); // default is red thin line
  _curve->clear();
  
  const unsigned int _nb_data = 100;
  for( unsigned int i=0; i < _nb_data; ++i) {
    Sample pt;
      pt.x = 2.0 * M_PI * i / _nb_data;
      pt.y = sin( pt.x );
      pt.z = 0.0;      
      _curve->add_sample( pt );
  }

  
  // ****** GRAPHIC ********
  init_glfw();
  std::cout << "__WINDOW and FIGURE" << std::endl;
  _win = new Window( "001-curveoffscreen", 600, 600, true /*offscreen*/ );
  _fig = new Figure( *_win, "sin( 2 * \\pi * x )     [Curve as offscreen]" );
  _win->add_plotter( _fig );
  _fig->add_plotter( _curve );

  _win->update_bbox();
  
  std::cout << "__RENDER" << std::endl;
  // render only once then save
  _win->render();
  _win->save( "001-curveoffscreen.png" );

  std::cout << "__END" << std::endl;
  delete _curve;
  delete _fig;
  delete _win;

  glfw_end();
  
  return 0;
}
