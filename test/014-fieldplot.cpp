/* -*- coding: utf-8 -*- */

/** 
 * 014-fieldplot.cpp
 *
 * VisuGL example with a FieldPlot of a field converging to a paraboloid.
 * Generate some Data and debug BBoxes.
 *
 * TODO
 * Window + Figure with title
 *   + _pos_mesh : Samples with position of points with influence
 *   + _vec_mesh : Samples with influce (vector) at eash point
 *   + _field_p as FieldPlotter of _pos_mesh and _vec_mesh
 *
 * Demonstrate various options of FieldPlotter : color, width, size, scale
 *
 * Options:
 * -d/debug : grafik debug
 *
 * 'D' or 'd' -> debug grafik
 * 'S' or 's' -> save drawing as "014-field.png"
 * 'C' or 'c' -> change color (blue/red)
 * 'V' or 'v' -> change width (0.5/2.0)
 * 'B' or 'b' -> change scale (1.0/0.3)
 * 'N' or 'n' -> change size (0.05/0.01)
 */
#include <iostream>
#include <string>         // std::string

// declare Window, Figure,
#include <visugl.hpp>
// using namespace visugl; // or use visugl:: everywhere...
#include <field_plotter.hpp>

/** Window, Figure and Plotters */
visugl::WinHolder* _win;
visugl::Figure* _fig;
visugl::Data* _pos_mesh;
visugl::Data* _vec_mesh;
visugl::FieldPlotter* _field_p;

/** Parameters */
double time_glfw = 0;
const unsigned int _mesh_density = 5;
// GUI parameters to demonstrate various options
bool _opt_color = true; // true is blue
bool _opt_width = true; // true is 0.5f
bool _opt_scale = true; // true is 1.0f
bool _opt_size = true; // true is 0.05f

/** Graphic */
const bool _static_win = false;

/** Debug */
bool _debug_fg = false;

//******************************************************************************
/**
 * Callback for keyboard events
 * => save figure
 */
static void key_callback(int key, int scancode, int action, int mods)
{
  // ESC => ask for closing window
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(_win->_window, GL_TRUE);
  }

  // D/d => set/unset DEBUG mode
  else if (key == GLFW_KEY_D && action == GLFW_PRESS) {
    _debug_fg = not _debug_fg;
    _win->set_debug( _debug_fg );   
    _fig->set_debug( _debug_fg );
  }
  
  // S/s => save in .png file
  else if (key == GLFW_KEY_S && action == GLFW_PRESS) {
    // save window
    _win->save( "014-field.png" );
  }

  // C/c => change color (blue/red)
  else if (key == GLFW_KEY_C && action == GLFW_PRESS) {
    if (_opt_color) {
      _field_p->set_color( {1.0f, 0.0f, 0.0f} );
    }
    else {
      _field_p->set_color( {0.0f, 0.0f, 1.0f} );
    }
    _opt_color = !_opt_color;
  }
  // V/v => change width (0.5/2.0)
  else if (key == GLFW_KEY_V && action == GLFW_PRESS) {
    if (_opt_width) {
      _field_p->set_width( 2.f );
    }
    else {
      _field_p->set_width( 0.5f );
    }
    _opt_width = !_opt_width;
  }
  // B/b => change scale (1.0/0.3)
  else if (key == GLFW_KEY_B && action == GLFW_PRESS) {
    if (_opt_scale) {
      _field_p->set_scale( 0.3f );
    }
    else {
      _field_p->set_scale( 1.0f );
    }
    _opt_scale = !_opt_scale;
  }
  // N/n => change size (0.05/0.01)
  else if (key == GLFW_KEY_N && action == GLFW_PRESS) {
    if (_opt_size) {
      _field_p->set_size( 0.01f );
    }
    else {
      _field_p->set_size( 0.05f );
    }
    _opt_size = !_opt_size;
  }
}

// ***************************************************************************
// ********************************************************* Graphic Functions
// ***************************************************************************
// ******************************************************************** render
void render()
{
  while( not visugl::grafik::should_stop() and !glfwWindowShouldClose(_win->_window) ) {
    time_glfw = glfwGetTime();
    
    _win->render();

  }
}

// ***************************************************************************
// ********************************************************************** MAIN
// ***************************************************************************
int main( int argc, char *argv[] )
{
  // check if a staticWindow is to be used
  // test for arguments 'static' or 'help'
  if (argc > 1) {
    for( unsigned int i = 1; i < argc; ++i) {
      std::string arg = argv[i];
      // help ?
      if ((arg.find( "help" ) != std::string::npos) or
          (arg.find( "-h" ) != std::string::npos)) {
        std::cout << "usage : " << argv[0] << " -h/help -d/debug " << std::endl;
        exit(0);
      }
      // debug
      else if ((arg.find( "debug" ) != std::string::npos) or
               (arg.find( "-d" ) != std::string::npos)) {
        _debug_fg = true;
      }
      else {
        std::cout << argv[0] << " ERROR: unrecognized argument " << arg  << std::endl;
      }
    }
  }
  
  // Main Data
  // Generate the MeshGrid in [0,1.0] x [0,1]
  _pos_mesh = new visugl::Data();
  for( unsigned int ix = 0; ix < _mesh_density; ++ix) {
    for( unsigned int iy = 0; iy < _mesh_density; ++iy) {
      double x_mesh = static_cast<double>(ix) * (1.0 - 0.0) / static_cast<double>(_mesh_density - 1);
      double y_mesh = static_cast<double>(iy) * (1.0 - 0.0) / static_cast<double>(_mesh_density - 1);
      _pos_mesh->add( {x_mesh, y_mesh, 0.0} );
    }
  }
  std::cout << "__POS_MESH creation BB=" << _pos_mesh->get_bbox() << std::endl;

  // Generate the MeshGrid influence by pointint outward from (1/2, 1/2)
  _vec_mesh = new visugl::Data();
  for( auto& pt: _pos_mesh->get_samples()) {
    double x_vec = (pt.x - 0.5) / 2.0;
    double y_vec = (pt.y - 0.5) / 2.0;
    _vec_mesh->add( {x_vec, y_vec, 0.0} );
  }
  std::cout << "__VEC_MESH creation BB=" << _vec_mesh->get_bbox() << std::endl;
      

  // // MarkData: subset of _curve_main
  // _curve_view = new visugl::Data();
  // _curve_view->clear();
  // _curve_view->set_debug( _debug_fg );
  // _curve_view->set_name( "DATA_VIEW" );

  // ****** GRAPHIC ********
  visugl::grafik::init();
  std::cout << "__WINDOW and FIGURE" << std::endl;
  if (_static_win) {
    _win = new visugl::WindowStatic( "014-fieldplot", 600, 600, /* size */
                                     false /* offscreen */, 20, 20 /* position */ );
    _win->set_name( "WINSTAT" );
  }
  else {
    _win = new visugl::Window( "014-fieldplot", 600, 600, /* size */
                               false /* offscreen */, 20, 20 /* position */ );
    _win->set_name( "WIN" );
  }
  _win->set_debug( _debug_fg );

  _fig = new visugl::Figure( "Field          [ESC:quit, D:debug, S:save as 014-field.png]" );
  _fig->set_debug( _debug_fg );
  _fig->set_name( "FIG" );

  // Add a key_callback
  _win->add_keycallback( key_callback );

  // Plotters
  _win->add_plotter( _fig );
  _field_p = new visugl::FieldPlotter( _pos_mesh, _vec_mesh );
  _field_p->set_name( "FIELD_P" );
  _field_p->set_color( {0.0f, 0.0f, 1.0f} );
  _field_p->set_width( 0.1f );
  _fig->add_plotter( _field_p );
  // _liner = new visugl::LinePlotter( _curve_main );
  // _liner->set_name( "Line" );
  // _liner->set_color( {1.f, 0.f, 0.f } );
  // _fig->add_plotter( _liner );
  // _marker = new visugl::MarkPlotter( _curve_view );
  // _marker->set_name( "Mark" );
  // _fig->add_plotter( _marker );

  // // By default, Figure bbox is [-1,1] x [-1,1], at the center of the WinHolder
  // // with a margin of 0.1 around it

  // // Setting the bbox of the Figure
  // if (_set_bbox) {
  //   _fig->set_bbox( -0.5, 2.5, -1.0, 1.0 );
  // }
  
  // // update_bbox ask its size to every sub-plotter, recursive
  // // so the whole curve is displayed
  // if (_update_bbox) {
  _win->update_bbox();
  // }

  std::cout << "__RENDER" << std::endl;
  render();

  std::cout << "__END" << std::endl;
  delete _vec_mesh;
  delete _pos_mesh;
  delete _fig;
  delete _win;

  visugl::grafik::end();
  
  return 0;
}
