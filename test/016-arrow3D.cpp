/* -*- coding: utf-8 -*- */

/** 
 * 016-arrow3D.cpp
 *
 * VisuGL example with Date (Curve) and Plotters (Arrow) in a Figure3D,
 * using Trackball to orient and move the point of View of the Figure3D.
 *
 * Window + Figure3D with title
 *   + _pos and _vec as Data 
 *   + _arrower as LinePlotter
 *
 * Trackball :
 *   - orient: leftClick + move
 *   - zoom: wheel or rightClick + move
 *   - position: SHIFT + leftClick + move
 *
 * 'R' or 'r' -> reset the trackball orientation, zoom, position
 * 'S' or 's' -> save drawing as "010-marker.png"
 */

#include <iostream>

// declare Window, Figure,
#include <visugl.hpp>
// Figure3D must be added
#include <figure3D.hpp>
// using namespace visugl; // or use visugl:: everywhere...
#include <arrow_plotter.hpp>

/** Window, Figure and Plotters */
visugl::Window* _win;
visugl::Figure3D* _fig3d;
visugl::Data*  _pos;
visugl::Data*  _vec;
visugl::ArrowPlotter* _arrower;

/** Debug */
bool _debug_fg = false;

// ********************************************** Callback for key [optionnal]
// this is optionnal
//******************************************************************************
/**
 * Callback for keyboard events
 */
static void key_callback(int key, int scancode, int action, int mods)
{
  // ESC => ask for closing window
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(_win->_window, GL_TRUE);
  }
  
  else if (key == GLFW_KEY_S && action == GLFW_PRESS) {
    // save window
    _win->save( "016-arrow3D.png" );
  }

  // D/d => set/unset DEBUG mode
  else if (key == GLFW_KEY_D && action == GLFW_PRESS) {
    _debug_fg = not _debug_fg;
    _win->set_debug( _debug_fg );   
    _fig3d->set_debug( _debug_fg );   
    _pos->set_debug( _debug_fg );
    _vec->set_debug( _debug_fg );
    _arrower->set_debug( _debug_fg );
  }
}

// ***************************************************************************
// ********************************************************* Graphic Functions
// ***************************************************************************
// ******************************************************************** render
void render()
{
  while( not visugl::grafik::should_stop() and !glfwWindowShouldClose(_win->_window) ) {
    //time_glfw = glfwGetTime();
    
    _win->render();

  }
}

//******************************************************************************
int main( int argc, char *argv[] )
{
  // ****** DATA ***********
  std::cout << "__DATA" << std::endl;
  _pos = new visugl::Data();
  _pos->set_debug( _debug_fg );
  _pos->set_name( "_pos" );

  _vec = new visugl::Data();
  _vec->set_debug( _debug_fg );
  _vec->set_name( "_vec" );
  
  for( double t=0.0; t < 2*M_PI; t += M_PI/4.0 ) {
    visugl::Sample pt;
    pt.x = 0.5 * cos( t );
    pt.y = 0.5 * sin( t );
    pt.z = t / 5.0;
    _pos->add( pt );
    std::cout << "_pos:" << pt.x << ", " << pt.y << ", " << pt.z << std::endl;
    glm::vec3 pos( pt.x, pt.y, pt.z );
    
    pt.x = -0.4 * sin( t );
    pt.y = 0.4 * cos( t );
    pt.z = t / 10.0;
    std::cout << "_vec:" << pt.x << ", " << pt.y << ", " << pt.z << std::endl;
    _vec->add( pt );
    glm::vec3 vec( pt.x, pt.y, pt.z );

    
    glm::vec3 vec_Ox = glm::vec3( 1.0, 0.0, 0.0 );
    std::cout << "Ox:" << glm::to_string(vec_Ox) << std::endl;

    std::cout << "pos:" << glm::to_string(pos) << std::endl;
    std::cout << "vec:" << glm::to_string(vec) << std::endl;

    glm::vec3 dir( vec.x, vec.y, vec.z );
    std::cout << "dir:" << glm::to_string(dir) << std::endl;
    dir = glm::normalize( dir );
    std::cout << "normed_dir:" << glm::to_string(dir) << std::endl;
    glm::quat rotation_q = glm::rotation( vec_Ox, dir );
    std::cout << "rot_q:" << glm::to_string(rotation_q) << std::endl;
    glm::mat4 rotation_mat = glm::toMat4( rotation_q );
    std::cout << "rot_m:" << glm::to_string(rotation_mat) << std::endl;
    
    glm::mat4 transform_m( 1.0 ); // identity
    std::cout << "ID :" << glm::to_string(transform_m) << std::endl;
    transform_m = glm::translate( transform_m,
                                  glm::vec3( pos.x+vec.x,
                                             pos.y+vec.y,
                                             pos.z+vec.z ));
    std::cout << "TR :" << glm::to_string(transform_m) << std::endl;
    transform_m = rotation_mat * transform_m;
    std::cout << "ROT :" << glm::to_string(transform_m) << std::endl;
  }
  
  // ****** GRAPHIC ********
  visugl::grafik::init();
  std::cout << "__WINDOW and FIGURE" << std::endl;
  _win = new visugl::Window( "016-arrow3D", 600, 600,
                             false /* offscreen */, 20, 20 /* position */ );
  _win->set_debug( _debug_fg );
  _win->set_name( "WIN" );
  _fig3d = new visugl::Figure3D( *_win, "Lines  [R/r: reset, s/S: save, d/D: debug, ESC: quit]" );
  _fig3d->set_debug( _debug_fg );
  _fig3d->set_name( "FIG3D" );
  
  _win->add_plotter( _fig3d );

  // Optionnal : add a key_callback
  _win->add_keycallback( key_callback );

  // Plotters
  _win->add_plotter( _fig3d );
  _arrower = new visugl::ArrowPlotter( _pos, _vec );
  _arrower->set_name( "Arrow" );
  _arrower->set_color( {0.f, 0.f, 1.f } );
  _arrower->set_head_ratio( 0.1 );
  _fig3d->add_plotter( _arrower );

  _win->update_bbox();

  std::cout << "__RENDER" << std::endl;
  render();

  std::cout << "__END" << std::endl;
  delete _arrower;
  delete _pos;
  delete _vec;
  delete _fig3d;
  delete _win;

  visugl::grafik::end();

  return 0;
}
