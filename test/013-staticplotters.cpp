/* -*- coding: utf-8 -*- */

/** 
 * 013-staticplotters.cpp
 *
 * Show difference between Window and WindowStatic (Double or Single Buffer)
 * for displaying incremental graphic (no animation, noting to delete/erase).
 * using Line and Mark Plotters
 *
 * Random Walk in red, with changes in dark triangles.
 */

// ******************************************************************* GLOBALS
bool _static_window = true;
double _xpos = 0.0;
double _ypos = 0.0;
double _xspd, _yspd;
double _tsim = 0.0;
const double _delta_tsim = 0.01;

/** Debug */
bool _debug_fg = false;

// ************************************************************ Random GLOBALS
#include <random>
// Random Generator
std::random_device _random_seeder;
std::default_random_engine _rnd_engine( _random_seeder() );
auto _unif = std::uniform_real_distribution<double>(0.0, 1.0);
auto _normal = std::normal_distribution<double>(0.0, sqrt(2.0));

// ************************************************************ Grafic GLOBALS

// declare Window, Figure,
#include <visugl.hpp>
using namespace visugl;

#include <data.hpp>
#include <line_plotter.hpp>
#include <mark_plotter.hpp>

/** Window, Figure and Plotters */
Window *_win_traj;
WindowStatic *_winstat_traj;
Figure *_fig_traj;
Data *_d_traj;
Data *_d_change;
LinePlotter *_liner;
MarkPlotter *_marker;

void init_traj();
//******************************************************************************
/**
 * Callback for keyboard events
 */
static void key_callback(int key, int scancode, int action, int mods)
{
  // ESC => ask for closing window
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    grafik::_end_render = true;
  }

  // SPACE => clear_traj
  else if( key == GLFW_KEY_SPACE && action == GLFW_PRESS ) {
    init_traj();
    if (_static_window) {
      _winstat_traj->_should_redraw = true;
    }
    else {
      _win_traj->_should_redraw = true;
    }
  }
  
  // D/d => set/unset DEBUG mode
  else if (key == GLFW_KEY_D && action == GLFW_PRESS) {
    _debug_fg = not _debug_fg;
    if (_static_window) {
      _winstat_traj->set_debug( _debug_fg );
    }
    else {
      _win_traj->set_debug( _debug_fg );
    }
    _fig_traj->set_debug( _debug_fg );
    _liner->set_debug( _debug_fg );
    _marker->set_debug( _debug_fg );
  }
}

// ***************************************************************************
// ********************************************************* Graphic Functions
// ***************************************************************************
// ***************************************************************** init_traj
void init_traj()
{
  _d_traj->clear();
  _d_change->clear();
  
  _xpos = 0.0;
  _ypos = 0.0;

  _xspd = _normal( _rnd_engine );
  _yspd = _normal( _rnd_engine );
}
// ******************************************************************** render
void render()
{
  // init dynamics
  _xspd = _normal( _rnd_engine );
  _yspd = _normal( _rnd_engine );
  
  while( not grafik::should_stop() ) {

    // update new speed
    // change speed dynamics ?
    if( _unif( _rnd_engine ) < 0.02 ) {
      _xspd = _normal( _rnd_engine );
      _yspd = _normal( _rnd_engine );
      _d_change->add( {_xpos, _ypos, 0.0 } );
    }

    // update position and time
    _xpos += _xspd * _delta_tsim;
    _ypos += _yspd * _delta_tsim;
    _tsim += _delta_tsim;

    // rebound on world limits
    if (_xpos > 1.0) {
      _xpos = 1.0 - (_xpos - 1.0);
      _xspd = -_xspd;
    }
    if (_xpos < -1.0) {
      _xpos = -1.0 + (-1.0 - _xpos);
      _xspd = -_xspd;
    }
    if (_ypos > 1.0) {
      _ypos = 1.0 - (_ypos - 1.0);
      _yspd = -_yspd;
    }
    if (_ypos < -1.0) {
      _ypos = -1.0 + (-1.0 - _ypos);
      _yspd = -_yspd;
    }
    
    // update grafik
    _d_traj->add( {_xpos, _ypos, 0.0} );

    if (_static_window)
      _winstat_traj->render();
    else
      _win_traj->render();

    // test if windows ask for closing
    if (_static_window) {
      if (glfwWindowShouldClose(_winstat_traj->_window)) {
        grafik::_end_render = true;
      }
    }
    else {
      if (glfwWindowShouldClose(_win_traj->_window)) {
        grafik::_end_render = true;
      }
    }
  }
}

// ********************************************************************** MAIN
int main(int argc, char *argv[])
{
  // test for arguments
  if (argc != 2) {
    std::cout << "usage : " << argv[0] << " static | swap" << std::endl;
    exit(1);
  }
  // static - single buffer
  if ( strcmp( argv[1], "static" ) == 0 ) {
    _static_window = true;
  }
  // swap - double buffer
  else if ( strcmp( argv[1], "swap" ) == 0 ) {
    _static_window = false;
  }
  else {
    std::cout << "Unknown argument : " << argv[1] << std::endl;
    exit(1);
  }

  // init grafic
  std::cout << "__DATA holders" << std::endl;
  _d_traj = new Data();
  _d_traj->set_debug( _debug_fg );
  _d_traj->set_name( "D_TRAJ" );
  _d_change = new Data();
  _d_change->set_debug( _debug_fg );
  _d_change->set_name( "D_CHANGE" );
  
  
  grafik::init();
  std::cout << "__WINDOW and FIGURE" << std::endl;
  if (_static_window) {
    _winstat_traj = new WindowStatic( "013-staticplotters : STATIC", 600, 600, false, 10, 20 );
    _winstat_traj->set_debug( _debug_fg );
    _winstat_traj->set_name( "WINSTAT_TRAJ" );
    _winstat_traj->add_keycallback( key_callback );
    
    _fig_traj = new Figure( "[ESC:quit, D:debug]",
                       {-1.0, 1.0, 10, 2}, {-1.0, 1.0, 10, 2} );
    _fig_traj->set_debug( _debug_fg );
    _fig_traj->set_name( "FIG_TRAJ" );
    _winstat_traj->add_plotter( _fig_traj );
  }
  else {
    _win_traj = new Window( "013-staticplotters : SWAPPING", 600, 600, false, 10, 20  );
    _win_traj->add_keycallback( key_callback );
    _win_traj->set_debug( _debug_fg );
    _win_traj->set_name( "WIN_TRAJ" );
  
    _fig_traj = new Figure( "[ESC:quit, D:debug]",
                       {-1.0, 1.0, 10, 2}, {-1.0, 1.0, 10, 2} );
    _fig_traj->set_debug( _debug_fg );
    _fig_traj->set_name( "FIG_TRAJ" );
    _win_traj->add_plotter( _fig_traj );
  }

  std::cout << "__PLOTTERS" << std::endl;
  _liner = new LinePlotter( _d_traj );
  _liner->set_color( {1.f, 0.f, 0.f} );
  _marker = new MarkPlotter( _d_change );
  _marker->set_color( {0.f, 0.f, 0.f} );
  _marker->set_size( 0.01f );
  
  _fig_traj->add_plotter( _liner );
  _fig_traj->add_plotter( _marker );

  std::cout << "__RENDER" << std::endl;
  render();
  
  std::cout << "__END" << std::endl;
  std::cout << "Added " <<  _d_traj->get_samples().size() << " points ";
  std::cout << " simulated time=" << _tsim << " s." << std::endl;

  delete _liner;
  delete _marker;
  delete _d_traj;
  delete _fig_traj;
  if (_static_window) 
    delete _winstat_traj;
  else
    delete _win_traj;

  grafik::end();

  return 0;
}

