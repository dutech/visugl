/* -*- coding: utf-8 -*- */

/** 
 * 012-dyncurve.cpp
 *
 * VisuGL example.
 * Window + Figure with title
 *   + Data to which we periodically add DATA (as sample is for time series)
 *   + Data has a random start point (_startx,_starty) and a random mean
 *     direction (_dir) to which some sinus perturbation is added.
 *   + DataMean ?? that can switch between "NORMAL" and "MEAN" mode (key W)
 *
 * 'C' or 'c' -> switch continuous/segments
 * 'D' or 'd' -> debug grafik
 * 'W' or 'w' -> switch between "normal" and "mean" mode
 * 'N' or 'n' -> print nb of samples in each curve
 * 'SPC'      -> pause simulation
 */

#include <string>
#include <stdlib.h>
#include <iostream>
#include <sstream>

#include <vector>

// ************************************************************ Random GLOBALS
#include <random>
// Random Generator
std::random_device _random_seeder;
std::default_random_engine _rnd_engine( _random_seeder() );
std::uniform_real_distribution<double> _rnd_unif = std::uniform_real_distribution<double>(0.0, 1.0);
// random parameters for curve
double _startx, _starty;
double _dir;
double _time = 0.0;
double _old_time;

// declare Window, Figure,
#include <visugl.hpp>
#include <data.hpp>
#include <line_plotter.hpp>
using namespace visugl;

/** Window, Figure and Plotters */
Window* _win;
Figure* _fig;
LinePlotter* _liner;
Data*  _data;
//DataMean* _data_mean;

/** Debug */
bool _debug_fg = false;
bool _play_flag = true;

//******************************************************************************
/**
 * Callback for keyboard events
 * deals with W and D
 */
/** Update the string that will be used as Figure title text.
 * WARNING : _liner must be defined
 */
std::string str_title ()
{
  std::stringstream title;
  title << "Dyn Data ";
  if (_liner->get_continuous()) {
    title << "(CONT)";
  }
  else {
    title << "(SEG)";
  }
  title << " [ESC:quit, C: seg/line, W:Mean ON/OFF, N:info, D:debug, SPC:pause]";

  return title.str();
}
static void key_callback(int key, int scancode, int action, int mods)
{
  // ESC => ask for closing window
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(_win->_window, GL_TRUE);
  }

  // D/d => set/unset DEBUG mode
  else if (key == GLFW_KEY_D && action == GLFW_PRESS) {
    _debug_fg = not _debug_fg;
    _win->set_debug( _debug_fg );   
    _fig->set_debug( _debug_fg );   
    _data->set_debug( _debug_fg );
    //_data_mean->set_debug( _debug_fg );
  }

  // C/c => switch continuous/segments
  else if (key == GLFW_KEY_C && action == GLFW_PRESS) {
   _liner->set_continuous( !_liner->get_continuous() );
   _fig->set_title( str_title() );
  }

  // W or w : switch _data_mean_mode 
  // else if ( key == GLFW_KEY_Z && action == GLFW_PRESS) {
  //   // toggle _mean
  //   if( _data_mean->get_mean_mode() ) {
  //     std::cout << "  Normal mode" << std::endl;
  //     _data_mean->set_mean_mode( false );
  //   }
  //   else {
  //     std::cout << "  MEAN mode" << std::endl;
  //     _data_mean->set_mean_mode( true );
  //     _data_mean->recompute_means();
  //   }
  // }
  // display nb of samples in datas
  else if ( key == GLFW_KEY_N && action == GLFW_PRESS ) {
    std::cout << _data->get_samples().size() << " samples in each data" << std::endl;
  }
  // SPACE => pause/run
  else if (key == GLFW_KEY_SPACE && action == GLFW_PRESS) {
    _play_flag = not _play_flag;
  }
}

// ***************************************************************************
// ********************************************************* Graphic Functions
// ***************************************************************************
// ******************************************************************** render
void update_and_render()
{
  glfwSetTime(0.0);

  unsigned int nb_render = 0;
  while( not grafik::should_stop() and !glfwWindowShouldClose(_win->_window) ) {
    // add data to _data
    auto ctime = glfwGetTime();
    if (_play_flag ) {
      _time += (ctime - _old_time);
      auto new_x = _startx + _time/5.0 * cos(_dir);
      auto new_y = _starty + _time/5.0 * sin(_dir) + sin(_time)+0.1*sin(50.0*_time);
      _data->add( {new_x, new_y, 0.0} );
      //_data_mean->add_data( {new_x, new_y, 0.0} );
      _win->update_bbox();
    }
    _old_time = ctime;

    grafik::check_error();
    _win->render();
    grafik::check_error();
    nb_render++;
  }
}
//******************************************************************************
int main( int argc, char *argv[] )
{
  // set random parameters
  _startx = _rnd_unif(_rnd_engine) * 2.0 - 1.0;
  _starty = _rnd_unif(_rnd_engine) * 2.0 - 1.0;
  _dir = (_rnd_unif(_rnd_engine) *2.0 - 1.0) * M_PI;
  std::cout << "From (" << _startx << ", " << _starty << ") in ";
  std::cout <<  _dir  << " [" << (_dir/M_PI*180.0) << "]" << std::endl;
  
  // A dynamic Data and its mean
  std::cout << "__DATA" << std::endl;
  _data = new Data();
  _data->clear();
  _data->set_debug( _debug_fg );
  _data->set_name( "DATA" );
  // _data_mean = new DataMean();
  // _data_mean->set_color( {0.0, 0.0, 1.0} );
  // _data_mean->set_width( 2.0f );
  // _data_mean->clear();
  // _data_mean->set_debug( _debug_fg );
  // _data_mean->set_name( "DATA_MEAN" );
  
  // ****** GRAPHIC ********
  grafik::init();
  grafik::check_error();
  std::cout << "__WINDOW, FIGURE and PLOTTERS" << std::endl;
  _liner = new LinePlotter( _data );
  _liner->set_continuous( true );
  _liner->set_name( "LP" );
  _liner->set_color( {1.0, 0.0, 0.0} );

  _win = new Window( "012-dyndata", 600, 600, /* size */
                             false /* offscreen */, 20, 20 /* position */ );
  _win->set_debug( _debug_fg );
  _win->set_name( "WIN" );

  _fig = new Figure( str_title() );
  _fig->set_debug( _debug_fg );
  _fig->set_name( "FIG" );
  _fig->add_plotter( _liner );

  _win->add_plotter( _fig );
  _win->update_bbox();

  // add a key_callback
  _win->add_keycallback( key_callback );

  std::cout << "__RENDER" << std::endl;
  _old_time = glfwGetTime();
  update_and_render();

  std::cout << "__END" << std::endl;
  delete _win;
  delete _fig;
  delete _liner;
  delete _data;

  grafik::end();
  
  return 0;
}
