/* WIP WIP WIP WIP
 TEST. Pour mieux comprendre la notion de Fonts

*/

#include <iostream>
#include <memory>

#include <visugl.hpp>

std::unique_ptr<visugl::Window> _win;
std::unique_ptr<visugl::Figure> _fig;

static void key_callback(int key, int scancode, int action, int mods)
{
  // ESC => ask for closing window
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(_win->_window, GL_TRUE);
  }
}
// ***************************************************************************
// ********************************************************************** main
// ***************************************************************************
int main(int argc, char *argv[])
{
  int nb_rendering = 0;
  // check how many rendering to to (minimum 0)
  if (argc > 1) {
    for( unsigned int i = 1; i < argc; ++i) {
      std::string arg = argv[i];
      // help ?
      if ((arg.find( "help" ) != std::string::npos) or
          (arg.find( "-h" ) != std::string::npos)) {
        std::cout << "usage : " << argv[0] << " -h/help [0-3] /*nb_rendering*/" << std::endl;
        exit(0);
      }
      else {
        nb_rendering = std::stoi(arg);
        if (nb_rendering < 0) nb_rendering = 0;
        else if (nb_rendering > 3) nb_rendering = 3;
      }
    }
  }

  visugl::grafik::init();

  _win = std::make_unique<visugl::Window>( "020-t-fonts", 600, 600, false, 20, 20 );
  _fig = std::make_unique<visugl::Figure>( "UN SUPER TITRE");
  _win->add_plotter( _fig.get() );
  _win->add_keycallback( key_callback );

  if (nb_rendering > 0) {
    std::cout << "__Render 01" << std::endl;
    _win->render();
    std::cout << "  w=" << _win->_width << " h=" << _win->_height << std::endl;

    if (nb_rendering > 1) {
      std::cout << "__Render 02" << std::endl;
      _win->render();
      std::cout << "  w=" << _win->_width << " h=" << _win->_height << std::endl;

      if (nb_rendering > 2) {
        std::cout << "__Render 03" << std::endl;
        _fig->set_bbox( -1, 1, -1, 1 );
        _win->render();
        std::cout << "  w=" << _win->_width << " h=" << _win->_height << std::endl;
      }
    }
  }
  while( !glfwWindowShouldClose(_win->_window) ) {
    glfwPollEvents();
  }

  // Must destroy grafik objects before terminating GLFW/grafik
  _win->destroy_grafik();
  visugl::grafik::end();

  return 0;
}
