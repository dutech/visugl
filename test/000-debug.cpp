/* -*- coding: utf-8 -*- */

/** 
 * 000-debug.cpp
 *
 * Simple Window+Figure+Curve example with name and debug.
 * Show the use of `set_debug()` and `set_name()`.
 *
 * use with args 'named' to set names
 */

// declare Window, Figure,
#include <visugl.hpp>
// using namespace visugl; // or use visugl:: everywhere...
#include <curve.hpp>

/** Window, Figure and Plotters */
visugl::Window* _win;
visugl::Figure* _fig;
visugl::Curve*  _curve1;
visugl::Curve*  _curve2;

/** Graphic */
bool _end_render = false;

/** Do we give name to elements */
bool _with_name_fg = false;

//******************************************************************************
/**
 * Callback for keyboard events
 */
static void key_callback(int key, int scancode, int action, int mods)
{
  // ESC => ask for closing window
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(_win->_window, GL_TRUE);
  }
}

// ***************************************************************************
// ********************************************************* Graphic Functions
// ***************************************************************************
/**
 * Init and end GLFW
*/
void init_glfw()
{
  std::cout << "__GLFW Init" << std::endl;
  
  glfwSetErrorCallback(visugl::Window::error_callback);
  
  if (!glfwInit())
        exit(EXIT_FAILURE);
}
void glfw_end()
{
  glfwTerminate();
  std::cout << "__GLFW destroyed" << std::endl;
}
// ******************************************************************** render
void render()
{
  while( not _end_render and !glfwWindowShouldClose(_win->_window) ) {
    _win->render();
  }
}

//******************************************************************************
int main( int argc, char *argv[] )
{
  // test for arguments
  if (argc == 2) {
    if (strcmp( argv[1], "named" ) == 0 ) {
      _with_name_fg = true;
    }
    else {
      std::cout << "Unrecognized argument" << std::endl;
      std::cout << "usage : " << argv[0] << " [named]" << std::endl;
      exit(1);
    }
  }
  else if (argc > 2) {
     std::cout << "Too many arguments" << std::endl;
     std::cout << "usage : " << argv[0] << " [named]" << std::endl;
     exit(2);
  }
  
  // Basic curve
  std::cout << "__CURVE" << std::endl;
  _curve1 = new visugl::Curve();
  _curve1->set_debug( true );
  if (_with_name_fg) _curve1->set_name( "CURVE1" );
  _curve1->clear();

  const unsigned int _nb_data = 100;
  for( unsigned int i=0; i < _nb_data; ++i) {
    visugl::Sample pt;
      pt.x = 2.0 * M_PI * i / _nb_data;
      pt.y = sin( pt.x );
      pt.z = 0.0;      
      _curve1->add_sample( pt );
  }

  // Copy to which we change points, and "go back"
  _curve2 = new visugl::Curve( *_curve1 );
  _curve2->set_debug( true );
  if (_with_name_fg) _curve2->set_name( "CURVE2" );
  _curve2->set_color( {0.0, 0.1, 1.0} ); // blue line
  _curve2->set_width( 3.f );             // thicker
  // inverse points
  // get_samples (a COPY) , inverse, and copy back to curve2
  auto samples2 = _curve2->get_samples(); // a COPY
  _curve2->clear();                       // can clear without fear
  for( auto& pt: samples2) {
    pt.y = -pt.y;
    _curve2->add_sample( pt );
  }

  // ****** GRAPHIC ********
  init_glfw();
  std::cout << "__WINDOW and FIGURE" << std::endl;
  _win = new visugl::Window( "000-debug", 600, 600, /* size */
                             false /* offscreen */, 20, 20 /* position */ );
  _win->set_debug( true );
  if (_with_name_fg) _win->set_name( "WIN" );
  
  _fig = new visugl::Figure( *_win, "Basic Curve          [ESC:quit]" );
  _fig->set_debug( true );
  if (_with_name_fg) _fig->set_name( "FIG" );
  
  // Add a key_callback
  _win->add_keycallback( key_callback );

  _win->add_plotter( _fig );
  _fig->add_plotter( _curve1 );
  _fig->add_plotter( _curve2 );
  _win->update_bbox();
  
  std::cout << "__RENDER" << std::endl;
  render();

  std::cout << "__END" << std::endl;
  delete _curve1;
  delete _curve2;
  delete _fig;
  delete _win;

  glfw_end();
  
  return 0;
}
