/* -*- coding: utf-8 -*- */

/** 
 * Test Figure3D
 * - draw simple 3D cube : 
 */

#include <visugl.hpp>
#include <figure3D.hpp>

visugl::Window *_win;
visugl::Figure3D *_fig3d;

/** Graphic */
bool _end_render = false;

// //******************************************************************************
// /**
//  * Callback for keyboard events
//  */
// static void visugl::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
// {
//   if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
//     glfwSetWindowShouldClose(window, GL_TRUE);
//   }
// }
// ***************************************************************************
// ********************************************************* Graphic Functions
// ***************************************************************************
/**
 * Init and end GLFW
*/
void init_glfw()
{
  std::cout << "__GLFW Init" << std::endl;
  
  glfwSetErrorCallback(visugl::Window::error_callback);
  
  if (!glfwInit())
        exit(EXIT_FAILURE);
}
void glfw_end()
{
  glfwTerminate();
  std::cout << "__GLFW destroyed" << std::endl;
}
// ******************************************************************** render
void render()
{
  while( not _end_render and !glfwWindowShouldClose(_win->_window) ) {
    _win->render();

  }
}

// ***************************************************************************
// ********************************************************************** MAIN
// ***************************************************************************
int main(int argc, char *argv[])
{
  init_glfw();

  // ***** Grafik *****
  std::cout << "__WINDOW and FIGURE" << std::endl;
  _win = new visugl::Window( "Test Figure 3D", 600, 600,
                             false, 20, 20 );
  _fig3d = new visugl::Figure3D( *_win );
  _win->add_plotter( _fig3d );
  _win->update_bbox();

  std::cout << "__RENDER" << std::endl;
  render();

  std::cout << "__END" << std::endl;
  delete _fig3d;
  delete _win;

  glfw_end();
  
  return 0;
}
