/* -*- coding: utf-8 -*- */

/** 
 * 005-line3D.cpp
 *
 * VisuGL example with Date (Curve) and Plotters (Line) in a Figure3D,
 * using Trackball to orient and move the point of View of the Figure3D.
 *
 * Window + Figure3D with title
 *   + _curve as Data 
 *   + _liner as LinePlotter
 *
 * Trackball :
 *   - orient: leftClick + move
 *   - zoom: wheel or rightClick + move
 *   - position: SHIFT + leftClick + move
 *
 * 'R' or 'r' -> reset the trackball orientation, zoom, position
 * 'S' or 's' -> save drawing as "010-marker.png"
 */

#include <iostream>

// declare Window, Figure,
#include <visugl.hpp>
// Figure3D must be added
#include <figure3D.hpp>
// using namespace visugl; // or use visugl:: everywhere...
#include <curve.hpp>
#include <line_plotter.hpp>

/** Window, Figure and Plotters */
visugl::Window* _win;
visugl::Figure3D* _fig3d;
visugl::Data*  _curve;
visugl::LinePlotter* _liner;

/** Debug */
bool _debug_fg = false;

// ********************************************** Callback for key [optionnal]
// this is optionnal
//******************************************************************************
/**
 * Callback for keyboard events
 */
static void key_callback(int key, int scancode, int action, int mods)
{
  // ESC => ask for closing window
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(_win->_window, GL_TRUE);
  }
  
  else if (key == GLFW_KEY_S && action == GLFW_PRESS) {
    // save window
    _win->save( "015-line3D.png" );
  }

  // D/d => set/unset DEBUG mode
  else if (key == GLFW_KEY_D && action == GLFW_PRESS) {
    _debug_fg = not _debug_fg;
    _win->set_debug( _debug_fg );   
    _fig3d->set_debug( _debug_fg );   
    _curve->set_debug( _debug_fg );
    _liner->set_debug( _debug_fg );
  }
}

// ***************************************************************************
// ********************************************************* Graphic Functions
// ***************************************************************************
// ******************************************************************** render
void render()
{
  while( not visugl::grafik::should_stop() and !glfwWindowShouldClose(_win->_window) ) {
    //time_glfw = glfwGetTime();
    
    _win->render();

  }
}

//******************************************************************************
int main( int argc, char *argv[] )
{
  // ****** DATA ***********
  std::cout << "__CURVE" << std::endl;
  _curve = new visugl::Data();
  _curve->set_debug( _debug_fg );
  _curve->set_name( "CURVE" );
  
  for( double t=0.0; t < 10.0; t += 0.1 ) {
    visugl::Sample pt;
    pt.x = 0.7 * cos( t );
    pt.y = 0.7 * sin( t );
    pt.z = t / 5.0 - 0.5;
    _curve->add( pt );
  }
  
  // ****** GRAPHIC ********
  visugl::grafik::init();
  std::cout << "__WINDOW and FIGURE" << std::endl;
  _win = new visugl::Window( "015-line3D", 600, 600,
                             false /* offscreen */, 20, 20 /* position */ );
  _win->set_debug( _debug_fg );
  _win->set_name( "WIN" );
  _fig3d = new visugl::Figure3D( *_win, "Lines  [R/r: reset, s/S: save, d/D: debug, ESC: quit]" );
  _fig3d->set_debug( _debug_fg );
  _fig3d->set_name( "FIG3D" );
  
  _win->add_plotter( _fig3d );

  // Optionnal : add a key_callback
  _win->add_keycallback( key_callback );

  // Plotters
  _win->add_plotter( _fig3d );
  _liner = new visugl::LinePlotter( _curve );
  _liner->set_name( "Line" );
  _liner->set_color( {0.f, 0.f, 1.f } );
  _liner->set_width( 2.f );

  _fig3d->add_plotter( _liner );

  _win->update_bbox();

  std::cout << "__RENDER" << std::endl;
  render();

  std::cout << "__END" << std::endl;
  delete _liner;
  delete _curve;
  delete _fig3d;
  delete _win;

  visugl::grafik::end();

  return 0;
}
