#!/usr/bin/env python
# encoding: utf-8
# waf configuration file .waf --help pour commande par défaut
# Utilisé par CMD dist

## Par défaut ./waf configure va utiliser buildir=wbuild et CXX=g++
## MAIS avec ./waf configure --out=cbuild --check-cxx-compiler=clang++
##      on peut utilise clang :o)
## l'option --color permet de coloriser la sortie du compilo

APPNAME = 'visugl'
VERSION = '0.4'

# root, build dir
top = '.'
out = 'wbuild'

opt_flags = '-O3'
debug_flags = '-O0 -g'

import os
#
# ************************************************************************* help
def help(ctx):
    print( "**** WAF for VisuGL, usual commands ************" )
    print( "configuration :  ./waf configure --out=cbuild [--use_clang => --check_cxx_compiler=clang++]" )
    print( "                       --prefix=[/usr/local]" )
    print( "build :          ./waf build ")
    print( "build specific : ./waf build --targets=test/001-curve" )
    print( "install :        ./waf install (in --prefix) " )
    print( "clean :          ./waf clean" )
    print( "detailed help :  ./waf --help or see https://waf.io/book" )
    print( "  options :      --use_clang --compil_db --atb --old --debug" )
# ********************************************************************** options
def options( opt ):
    """
    opt : OptionContext
    options use the optparse https://docs.python.org/3/library/optparse.html
    """
    opt.load( 'compiler_cxx' )

    # option use_clang
    opt.add_option('--use_clang', dest='use_clang', action="store_true", default=False,
                   help='use clang and compile in cbuild (replace --out --check_cxx_compiler)' )

    # option debug
    opt.add_option('--debug', dest='debug', action="store_true", default=False,
                   help='compile with debugging symbols' )

    # clang compilation database
    opt.add_option('--compil_db', dest='compil_db', action="store_true", default=False,
                   help='use clang compilation database' )

    # define some macro for C++
    # (equivalent to #define LABEL or -DLABEL to compilator
    opt.add_option('-D', '--define', action="append", dest="defined_macro",
                   help='define preprocessing macro' )

    # specific to this project ************************************************
    # option anttweakbar
    opt.add_option('--atb', dest='atb', action="store_true", default=False,
                   help='configure and build with AntTwekBar' )
    # option old file
    opt.add_option('--old', dest='old', action="store_true", default=False,
                   help='configure and build old files' )

# **************************************************************** CMD configure
def configure( conf ):
    print( "__CONFIGURE" )
    print( '→ config from ' + conf.path.abspath())


    #memorise appname as it is not easily recoverable later
    conf.env.appname = APPNAME
    conf.env.version = VERSION

    if conf.options.use_clang:
        conf.options.check_cxx_compiler = "clang++"
        # 'out' cannot be without option --out as it is checked BEFORE reading wscript

    ## set env and options according to the possible compilers
    ## better to set options 'out' and 'check_cxx_compiler' before loading
    conf.load( 'compiler_cxx' )

    # sys.exit()
    if conf.options.compil_db:
        ## To generate 'compile_commands.json' at the root of buildpath
        # to be linked/copied in order to user 'cquery' in emacs through lsp-mode
        # see https://github.com/cquery-project/cquery/wiki/Emacs
        conf.load('clang_compilation_database', tooldir="ressources")
        print( "CXX=",conf.env.CXX)
    
    conf.env['CXXFLAGS'] = ['-D_REENTRANT','-Wall','-fPIC','-std=c++14']
    
    ## Require FTGL, using wraper around pkg-config
    conf.check_cfg(package='ftgl',
                   uselib_store='FTGL',
                   args=['--cflags', '--libs']
    )
    ## Require OpenGL, using wraper around pkg-onfig
    conf.check_cfg(package='gl',
                   uselib_store='GL',
                   args=['--cflags', '--libs']
    )
    ## Require OpenGL >1.1 (glew), using wraper around pkg-onfig
    conf.check_cfg(package='glew',
                   uselib_store='GLEW',
                   args=['--cflags', '--libs']
    )
    ## Require GLFW3, using wraper around pkg-config
    conf.check_cfg(package='glfw3',
                   uselib_store='GLFW3',
                   args=['--cflags', '--libs']
    )
    ## Require libpng, using wrapper around pkg-config
    conf.check_cfg(package='libpng',
                   uselib_store='PNG',
                   args=['--cflags', '--libs']
    )
    ## and libpng++ using code fragment
    conf.check_cxx( fragment='''
    #include <png++/png.hpp>
    int main() {} ''',
                   execute     = True,
                    msg         = "Checking for 'png++'",
                    uselib_store='PNG++'
    )
    
    ## Require libglm, using wrapper around pkg-config
    conf.check_cfg(package='glm',
                   uselib_store='GLM',
                   args=['--cflags', '--libs']
    )

    if conf.options.atb:
        ## Require AntTweakBar, using wrapper around pkg-config
        conf.check_cfg(package='AntTweakBar',
                       uselib_store='ANTTWEAKBAR',
                       args=['--cflags', '--libs']
                       )
        ## store in env
        conf.env.ATB = conf.options.atb

    # if conf.options.build_demos:
    #     conf.recurse('demos')

    # if conf.options.build_tests:
    #     conf.recurse('tests')

# ********************************************************************
# CMD build
def build( bld ):
    print('→ build from ' + bld.path.abspath())

    # check debug option
    if bld.options.debug:
        bld.env['CXXFLAGS'] += debug_flags.split(' ')
    else:
        bld.env['CXXFLAGS'] += opt_flags.split(' ')
    print( bld.env['CXXFLAGS'] )

    # add macro defines as -D options
    # print( "MACRO=", bld.options.defined_macro )
    if bld.options.defined_macro:
        # print( "ENV=", bld.env.DEFINES )
        bld.env.DEFINES = bld.env.DEFINES + bld.options.defined_macro
        # print( "new ENV=", bld.env.DEFINES )

    bld.recurse( 'src'  )
    # if bld.options.build_tests:
    bld.recurse( 'test' )

    bld.recurse( 'doc' )

    def generate_pkgconfig(task):
        """ Parse src file template, fill in the variables
        """
        src = task.inputs[0].abspath()
        tgt = task.outputs[0].abspath()
        # dict for replacing in template file
        pk_dict = {
            'version' : bld.env.version,
            'includedir' : bld.env.PREFIX+'/include/visugl',
            'libdir' : '',
            'libname' : '',
            'requirements' : 'ftgl glew gl glfw3 libpng'
        }
        if bld.env.ATB:
            pk_dict['requirements'] += ' AntTweakBar'

        # now parse template
        from string import Template
        with open(src) as fsrc:
            template = Template(fsrc.read())
            result = template.substitute(pk_dict)
            print(result)
            with open(tgt, 'w') as ftgt:
                ftgt.write(result)

    # task to create a pkg-config file (.pc extension)
    bld(source='visugl.pc.in',
        target='%s.pc' % bld.env.appname,
        rule=generate_pkgconfig,
        install_path='${PREFIX}/lib/pkgconfig')
